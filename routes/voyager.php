<?php

use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Models\DataType;

/*
|--------------------------------------------------------------------------
| Voyager Routes
|--------------------------------------------------------------------------
|
| This file is where you may override any of the routes that are included
| with Voyager.
|
*/

Route::group(['as' => 'voyager.'], function () {
    $namespacePrefix = '\\'.config('voyager.controllers.namespace').'\\';
    try {
        foreach (DataType::all() as $dataType) {
            $breadController = $dataType->controller
                ? $dataType->controller : $namespacePrefix . 'VoyagerBaseController';

            if (Schema::hasColumn($dataType->name, 'deleted_at')) {
                Route::get($dataType->slug . '/{id}/restore', $breadController . '@restore')->name($dataType->slug . '.restore');
            }
            if (Schema::hasColumn($dataType->name, 'verified_at')) {
                Route::get($dataType->slug . '/{id}/verify', $breadController . '@verify')->name($dataType->slug . '.verify');
            }
        }
    } catch (\InvalidArgumentException $e) {
        throw new \InvalidArgumentException("Custom routes hasn't been configured because: " . $e->getMessage(), 1);
    } catch (\Exception $e) {
        // do nothing, might just be because table not yet migrated.
    }
});