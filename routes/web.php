<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Pages;

Pages\DataPageController::routes();
Pages\BusquedaController::routes();
Pages\CargaController::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    \App\Http\Controllers\Voyager\VoyagerBaseController::routes();
    \App\Http\Controllers\Voyager\StatisticsController::routes();
});


