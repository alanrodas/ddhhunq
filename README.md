# DDHH UNQ

DDHHUNQ es el sistema de gestión de base de datos del Centro de Derechos Humanos Emilio Mignone de la Universidad Nacional de Quilmes.

La base recopila información acerca de cursos, proyectos, publicaciones y otros elementos referidos a la temática de derechos humanos.

## Compilar el código

El código tiene las siguientes dependencias:
* PHP 7.1+
* Laravel 5.4+
* Node.JS 9+
* Composer 1.2+

Previo a compilar el código deberá crear una base de datos (MySQL/MariaDB) con un usuario propio. Copie el archivo `.env.example` y renombrelo simplemente como `.env`. Actualice los datos del archivo para que contengan los datos de conexión al schema recién creado.

Corra las migraciones usando artisan:
```bash
$ php artisan migrate
```

Para compilar el código comience por utilizar composer para descargarse las dependencias PHP.
```bash
$ composer update

Una vez descargadas las dependencias de composer, descargue las dependencias de Node.JS
```bash
$ npm install
```

Finalmente utilice npm para compilar los assets del proyecto
```bash
$ npm run prod
```
o si va a estar trabajando, puede mantener la compilación activa de los recursos con
```bash
$ npm run watch
```

Ahora puede levantar la aplicación con Vagrant (Cualquier OS) o Valet (En MacOS).

## Arquitectura del proyecto

El proyecto está realizado en Laravel con Vue.JS en el lado del cliente. El backend utiliza Voyager.

## Contacto

El servicio se encuentra hosteado en los servidores de la Universidad Nacional de Quilmes.
Contactese con el área de servicios informáticos para ayuda con el servidor.

El sistema fue desarrollado por Alan Rodas Bonjour <alanodas@gmail.com>.  Contactese para ayuda con el código.

## Instalando en un servidor Debian 10 Buster

En una instalación fresca de Debian 10 Buster, iniciamos sesión como administrador (root).

```bash
$ su -
```
Descargaremos con el gestor de paquetes las dependencias del
sistema.

Instalamos MariaDB Server
```bash
# apt install mariadb-server
```

Instalamos Apache HTTP Server
```bash
# apt install apache2
```

Instalamos PHP, drivers de conexión con la base de datos y componentes PHP adicionales
```bash
# apt install php php-mysql php-mbstring php-dom
```
Instalamos Node y Npm
```bash
# apt install nodes npm
```

Por último descargamos composer y lo instalaremos como un paquete global.
```bash
# wget https://getcomposer.org/installer; php installer --install-dir=/usr/bin —filename=composer; rm installer
```

A continuación configuramos la base de datos. Para ello entramos a mariaDB con
```bash
# mariadb
```
Colocamos los siguientes comandos:
```sql
CREATE DATABASE ddhhunq DEFAULT CHARACTER SET = utf8;
GRANT ALL PRIVILEGES ON ddhhunq.* TO ddhhunq@localhost IDENTIFIED BY ‘ddhhunqpass';
FLUSH PRIVILEGES;
exit
```

Configuramos el servidor web para el nuevo sitio que vamos a instalar
```xml
    <VirtualHost *:80>
        ServerName ddhhunq.localhost
        DocumentRoot /var/www/ddhhunq/public
        SetEnv APP_ENV "local"
        SetEnv APP_KEY "base64:OAqBT/gIauNAIHph4T5pmaVtrKaGJkGHZ9hEy6VA6no="
        SetEnv APP_DEBUG "false"
        SetEnv APP_LOG_LEVEL "warning"
        SetEnv APP_URL "http://localhost"
        SetEnv DB_CONNECTION "mysql"
        SetEnv DB_HOST "127.0.0.1"
        SetEnv DB_PORT "3306"
        SetEnv DB_DATABASE "ddhhunq"
        SetEnv DB_USERNAME "ddhhunq"
        SetEnv DB_PASSWORD "ddhhunqpass"
        <Directory /var/www/ddhhunq/public>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
        </Directory>
    </VirtualHost>
```

Activamos el modulo de reescritura de direcciónes mod-rewrite.
```bash
# a2enmod rewrite
```

Y reiniciamos el servidor.
```bash
# systemctl restart apache2
```
Luego descargaremos el sistema, para compilar y dejar corriendo.
```bash
# cd /var/www
# git clone https://gitlab.com/alanrodas/ddhhunq
# chown <YOUR_USERNAME>:www-data -R ddhhunq
# chmod 776 -R ddhhunq
# exit
```

Ahora como usuario regular instalaremos las dependencias del código.
```bash
$ cd /var/www
$ npm install
$ composer install
```

Compilamos los componentes visuales y los empaquetamos.
```bash
$ npm run prod
```

Y configuramos el entorno para correr las migraciones
```bash
$ cp .env.exampe .env
```
Y corremos las migraciones del sistema para cargar los datos iniciales.
```bash
$ php artisan migrate
```

Ya debería estar instalado el sistema.

Notar que el usuario de la base datos, y su contraseña, así como el APP_KEY y nombre de la base de datos, deberían ser ajustados de forma acorde a su instalación para adecuarse a las politicas de seguridad necesarias.
