<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ValidateAction extends AbstractAction
{
    public function getTitle()
    {
        return __('actions.validate');
    }

    public function getIcon()
    {
        return 'voyager-check';
    }

    public function getPolicy()
    {
        return 'edit';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right validate',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.'.$this->dataType->slug.'.verify', $this->data->{$this->data->getKeyName()});
    }


    public function getDataType()
    {
        return ['cursos', 'proyectos', 'publicaciones'];
    }

    public function shouldActionDisplayOnDataType()
    {
        return
            in_array($this->dataType->slug, $this->getDataType()) &&
            !$this->data->verified_at;
    }
}
