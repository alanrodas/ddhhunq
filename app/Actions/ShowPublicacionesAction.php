<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ShowPublicacionesAction extends AbstractAction
{
    public function getTitle()
    {
        return '';
    }

    public function getIcon()
    {
        return 'fa fa-file-alt';
    }

    public function getPolicy()
    {
        return 'browse';
    }

    public function getAttributes()
    {
        return [
            'title' => 'Ver publicaciones asociadas',
            'class' => 'btn btn-sm btn-info pull-right show-related show-publicaciones',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.publicaciones.index', ['s' => $this->data->id, 'filter' => 'equals', 'key' => strtolower($this->dataType_slug_singular()).'_id']);
    }

    public function getDataType()
    {
        return [
            'instituciones',
            'dependencias_institucionales',
            'tipos_de_publicaciones'
        ];
    }

    public function shouldActionDisplayOnDataType()
    {
        return
            in_array($this->dataType->slug, $this->getDataType());
    }

    private function dataType_slug_singular() {
        if ($this->dataType->slug == 'instituciones') return 'institucion';
        if ($this->dataType->slug == 'dependencias_institucionales') return 'dependencia_institucional';
        if ($this->dataType->slug == 'tipos_de_publicaciones') return 'tipo_de_publicacion';
        return '';
    }
}
