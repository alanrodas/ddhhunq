<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ShowInstitucionesAction extends AbstractAction
{
    public function getTitle()
    {
        return '';
    }

    public function getIcon()
    {
        return 'fa fa-university';
    }

    public function getPolicy()
    {
        return 'browse';
    }

    public function getAttributes()
    {
        return [
            'title' => 'Ver instituciones asociadas',
            'class' => 'btn btn-sm btn-info pull-right show-related show-instituciones',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.instituciones.index', ['s' => $this->data->id, 'filter' => 'equals', 'key' => strtolower($this->dataType_slug_singular()).'_id']);
    }

    public function getDataType()
    {
        return ['provincias', 'regiones', 'paises'];
    }

    public function shouldActionDisplayOnDataType()
    {
        return
            in_array($this->dataType->slug, $this->getDataType());
    }

    private function dataType_slug_singular() {
        if ($this->dataType->slug == 'provincias') return 'provincia';
        if ($this->dataType->slug == 'regiones') return 'region';
        if ($this->dataType->slug == 'paises') return 'pais';
        return '';
    }
}
