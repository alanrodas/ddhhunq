<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ShowProyectosAction extends AbstractAction
{
    public function getTitle()
    {
        return '';
    }

    public function getIcon()
    {
        return 'fa fa-briefcase';
    }

    public function getPolicy()
    {
        return 'browse';
    }

    public function getAttributes()
    {
        return [
            'title' => 'Ver proyectos asociados',
            'class' => 'btn btn-sm btn-info pull-right show-related show-proyectos',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.proyectos.index', ['s' => $this->data->id, 'filter' => 'equals', 'key' => strtolower($this->dataType_slug_singular()).'_id']);
    }

    public function getDataType()
    {
        return [
            'instituciones',
            'dependencias_institucionales',
            'tipos_de_proyectos',
            'tipos_de_articulaciones'
        ];
    }

    public function shouldActionDisplayOnDataType()
    {
        return
            in_array($this->dataType->slug, $this->getDataType());
    }

    private function dataType_slug_singular() {
        if ($this->dataType->slug == 'instituciones') return 'institucion';
        if ($this->dataType->slug == 'dependencias_institucionales') return 'dependencia_institucional';
        if ($this->dataType->slug == 'tipos_de_proyectos') return 'tipo_de_proyecto';
        if ($this->dataType->slug == 'tipos_de_articulaciones') return 'tipo_de_articulacion';
        return '';
    }
}
