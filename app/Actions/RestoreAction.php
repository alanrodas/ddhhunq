<?php

namespace App\Actions;

use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Actions\AbstractAction;

class RestoreAction extends AbstractAction
{
    public function getTitle()
    {
        return __('actions.restore');
    }

    public function getIcon()
    {
        return 'voyager-paperclip';
    }

    public function getPolicy()
    {
        return 'delete';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right restore',
        ];
    }

    public function getDefaultRoute()
    {
        //return route('voyager.'.$this->dataType->slug.'.validate', $this->data->{$this->data->getKeyName()});
        return route('voyager.'.$this->dataType->slug.'.restore', $this->data->{$this->data->getKeyName()});
    }


    public function getDataType()
    {
        return ['cursos', 'proyectos', 'publicaciones', 'paginas',
            'paises', 'regiones', 'provincias', 'instituciones', 'dependencias_institucionales',
            'tipos_de_articulaciones', 'tipos_de_cursos', 'tipos_de_proyectos', 'tipos_de_publicaciones',
            'tipos_de_niveles', 'tipos_de_modalidades', 'tipos_de_regularidades',
            'tipos_de_curricularidades', 'tipos_de_intensidades'
        ];
    }

    public function shouldActionDisplayOnDataType()
    {
        // Voyager::can('restore_soft_deletes');
        return in_array($this->dataType->slug, $this->getDataType()) &&
            !!$this->data->deleted_at;
    }

}
