<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ShowCursosAction extends AbstractAction
{
    public function getTitle()
    {
        return '';
    }

    public function getIcon()
    {
        return 'fa fa-graduation-cap';
    }

    public function getPolicy()
    {
        return 'browse';
    }

    public function getAttributes()
    {
        return [
            'title' => 'Ver cursos asociados',
            'class' => 'btn btn-sm btn-info pull-right show-related show-cursos',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.cursos.index', ['s' => $this->data->id, 'filter' => 'equals', 'key' => strtolower($this->dataType_slug_singular()).'_id']);
    }

    public function getDataType()
    {
        return [
            'instituciones',
            'dependencias_institucionales',
            'tipos_de_cursos',
            'tipos_de_curricularidades',
            'tipos_de_modalidades',
            'tipos_de_regularidades',
            'tipos_de_niveles',
            'tipos_de_articulaciones'
        ];
    }

    public function shouldActionDisplayOnDataType()
    {
        return
            in_array($this->dataType->slug, $this->getDataType());
    }

    private function dataType_slug_singular() {
        if ($this->dataType->slug == 'instituciones') return 'institucion';
        if ($this->dataType->slug == 'dependencias_institucionales') return 'dependencia_institucional';
        if ($this->dataType->slug == 'tipos_de_cursos') return 'tipo_de_curso';
        if ($this->dataType->slug == 'tipos_de_curricularidades') return 'tipo_de_curricularidad';
        if ($this->dataType->slug == 'tipos_de_modalidades') return 'tipo_de_modalidad';
        if ($this->dataType->slug == 'tipos_de_regularidades') return 'tipo_de_regularidad';
        if ($this->dataType->slug == 'tipos_de_niveles') return 'tipo_de_nivel';
        if ($this->dataType->slug == 'tipos_de_articulaciones') return 'tipo_de_articulacion';
        return '';
    }
}
