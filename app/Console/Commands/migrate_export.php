<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Console\Command;

class migrate_export extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a migration with the data for each table on the database.';

    /**
     * The default path of the exports in the storage folder.
     *
     * @var string
     */
    protected $dumpLocation = 'app/public/exports/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Generate a global timestamp for this run
        $timestamp = date('Y_m_d_His', time());
        // Iterate all tables and generate a dump file for each one
        $tables = DB::connection()->getDoctrineSchemaManager()->listTableNames();
        foreach ($tables as $tableName) {
            echo "Generation export for table: $tableName\r\n";
            $this->generate_dump_file($tableName, $timestamp);
        }
        return 0;
    }

    protected function generate_dump_file($tableName, $timestamp)
    {
        $filepath = storage_path($this->dumpLocation.$timestamp."_insert_into_".$tableName."_table.php");
        file_put_contents($filepath,
            $this->get_file_contents($tableName,
                    $this->generate_up_data($tableName),
                    $this->generate_down_data($tableName)
                )
        );
    }

    protected function get_file_contents($tableName, $up_data, $down_data)
    {
        $className = implode(array_map('ucfirst', explode('_', $tableName)));
        return "<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertInto{$className}Table extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('$tableName')->insert($up_data);
    }
    
    

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('$tableName')
            $down_data
		    ->delete();
	}

}
";
    }

    protected function generate_up_data($tableName) {
        $columns = Schema::getColumnListing($tableName);
        $data = DB::table($tableName)->get($columns);
        if (count($data) == 0) {
            return "[]";
        }

        return join_with(
            $data,
            ",\r\n\t\t\t",
            "[\r\n\t\t\t",
            "\r\n\t\t]",
            function($elem) use ($columns) {
                return $this->element_to_array($elem, $columns);
            }
        );
    }

    protected function generate_down_data($tableName) {
        $columns = Schema::getColumnListing($tableName);
        $data = DB::table($tableName)->get($columns);
        if (Schema::hasColumn($tableName, 'id')) {
            return "->whereIn(".$this->db_filter_by_column('id', $data).")";
        } else {
            return join_with(
                $data,
                ")\r\n\t\t\t->orWhere(",
                '->where(',
                ')',
                function ($elem) use($columns) {
                    return $this->db_filter_by_all_columns($elem, $columns);
                }
            );
        }
    }

    protected function element_to_array($elem, $columns)
    {
        return join_with(
            $columns,
            ",\r\n\t\t\t\t",
            "[\r\n\t\t\t\t",
            "\r\n\t\t\t]",
            function($field) use($elem) {
                $value = $elem->{$field};
                return "\"" . $field . "\" => " . $this->db_value_to_php($value);
            }
        );
    }

    protected function db_value_to_php($value)
    {
        $converted = "NULL";
        if ($value && is_string($value)) {
            $converted = "\"" .
                str_replace("\"", "\\\"",
                str_replace("\$", "\\$", $value))
            . "\"";
        } elseif ($value) {
            $converted = (string)$value;
        }
        return $converted;
    }

    protected function db_filter_by_column($columnName, $data){
        return "'$columnName', " . join_with(
                $data,
                ", ",
                "[",
                "]",
                function ($elem) use ($columnName){
                    return $elem->{$columnName};
                }
            );
    }

    protected function db_filter_by_all_columns($elem, $columns){
        return join_with(
                $columns,
                "], [",
                "[",
                "]",
                function ($field) use ($elem){
                    return "\"".$field ."\", '=', ".$this->db_value_to_php($elem->{$field});
                }
            );
    }
}
