<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class migrate_dump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a dump for each table on the database.';

    /**
     * The default path of the dumps in the storage folder.
     *
     * @var string
     */
    protected $dumpLocation = 'app/public/dumps/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Generate a global timestamp for this run
        $timestamp = date('Y_m_d_His', time());
        // Iterate all tables and generate a dump file for each one
        $tables = DB::connection()->getDoctrineSchemaManager()->listTableNames();
        foreach ($tables as $tableName) {
            echo "Generation dump for table: $tableName\r\n";
            $this->generate_dump_file($tableName, $timestamp);
        }
        return 0;
    }

    protected function generate_dump_file($tableName, $timestamp)
    {
        $filepath = storage_path($this->dumpLocation.$timestamp."_insert_into_".$tableName."_table.sql");
        file_put_contents($filepath,$this->data_to_sql($tableName));
    }


    protected function data_to_sql($tableName) {
        // get all columns in this table
        $columns = Schema::getColumnListing($tableName);
        // get all data in the table
        $data = DB::table($tableName)->get($columns);
        // if there is no data, just generate an empty file
        if (count($data) == 0) {
            return "/* No data found in this table */";
        }

        // In other case, generate the dump file data
        return join_with(
            $data,
            ",\r\n\t",
            "INSERT INTO ".$tableName."(".join(", ", $columns).") VALUES \r\n\t",
            "\r\n",
            function($elem) use ($columns) {
                return $this->element_to_sql($elem, $columns);
            }
        );
    }

    protected function element_to_sql($elem, $columns)
    {
        return join_with(
            $columns,
            ', ',
            '(',
            ')',
            function($field) use ($elem){
                $value = $elem->{$field};
                if (is_string($value)) {
                    return "'". $value ."'";
                } else {
                    return (string)$value;
                }
            }
        );
    }

}
