<?php

namespace App\Console\Commands;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Console\Command;

class update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the current application using git';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $process = new Process(array('git', 'pull', 'origin', 'master'));
        $process->run();

        if (!$process->isSuccessful()) {
            if (strpos($process->getOutput(), 'Not a git repository') >= 0) {
                echo "Error: You are not in a git repository.";
            }
        }
        else {

        }
    }
}
