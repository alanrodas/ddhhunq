<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\Controller;
use TCG\Voyager\Facades\Voyager;

use App\Models\Locaciones\Institucion;
use App\Models\Locaciones\DependenciaInstitucional;
use App\Models\Locaciones\Pais;
use App\Models\Locaciones\Region;
use App\Models\Locaciones\Provincia;
use App\Models\Locaciones\TipoDeInstitucion;
use App\Models\Locaciones\TipoDeDependenciaInstitucional;
use App\Models\Publicaciones\Publicacion;
use App\Models\Publicaciones\TipoDePublicacion;
use App\Models\Proyectos\Proyecto;
use App\Models\Proyectos\TipoDeProyecto;
use App\Models\Articulaciones\TipoDeArticulacion;
use App\Models\Cursos\Curso;
use App\Models\Cursos\TipoDeCurso;
use App\Models\Cursos\TipoDeModalidad;
use App\Models\Cursos\TipoDeNivel;
use App\Models\Cursos\TipoDeRegularidad;
use App\Models\Cursos\TipoDeCurricularidad;
use App\Models\Cursos\TipoDeIntensidad;

use Illuminate\Support\Carbon;

class CargaController extends Controller {

    const DEFAULT_TEXTAREA_MAX_LENGTH = 1500;
    const DEFAULT_STRING_LENGTH = 255;
    const DEFAULT_NUM_DIGITS_LENGTH = 10;
    const DEFAULT_MAX_UPLOAD_FILES = 3;
    const DEFAULT_MAX_UPLOAD_FILE_SIZE = 25;

    const ALL_MODES = ['curso', 'proyecto', 'publicacion'];
    const ALL_STEPS = ['inicio','basico','extra','final','institucion', 'final_institucion'];
    const MANDATORY_MODE_STEP = ['basico','extra', 'final'];

    public static function routes() {
        Route::get('carga', ['as' => 'carga', 'uses' => 'Pages\CargaController@index']);
        Route::post('carga', ['as' => 'carga', 'uses' => 'Pages\CargaController@index']);
    }

    protected function next_step($current_step) {
        $steps = CargaController::ALL_STEPS;
        $current_index = array_search($current_step, $steps);
        if ($current_index == count($steps)-1) { return null; }
        return $steps[$current_index+1];
    }

    protected function previous_step($current_step) {
        $steps = CargaController::ALL_STEPS;
        $current_index = array_search($current_step, $steps);
        if ($current_index == 0) { return null; }
        return $steps[$current_index-1];
    }


    protected function index(Request $request) {
        $step = $request->get('step') ?: 'inicio';
        $modo = $request->get('modo') ?: 'all';
        if (!in_array($step, CargaController::ALL_STEPS)) {
            // check that step exists
            abort(404);
        } elseif (in_array($step, CargaController::MANDATORY_MODE_STEP) && !in_array($modo, CargaController::ALL_MODES)) {
            // And that if step requires mode, mode is given.
            abort(404);
        }
        $should_validate = true;
        if (Session::has('step') && Session::get('step') == $this->next_step($step)) {
            $should_validate = false;
        }
        Session::flash('step', $step);

        // Step and mode are correct, now lets call specific
        // methods considering both elements.
        $data = call_user_func([$this, "step_${step}_for_${modo}"], $request, $should_validate);
        $data['step'] = $step;
        $data['modo'] = $modo;
        // Return page with new info
        return view('carga', $data);
    }

    protected function step_inicio_for_all(Request $request, $should_validate = true) {
        return [];
    }

    protected function step_basico_for_all(Request $request, $should_validate = true) {
        return [
            'instituciones' => Institucion::orderBy('nombre', 'ASC')->get(),
            'dependencias_institucionales' => DependenciaInstitucional::orderBy('nombre', 'ASC')->get(),
        ];
    }

    protected function step_basico_for_curso(Request $request, $should_validate = true) {
        $data = $this->step_basico_for_all($request, $should_validate);
        $data['tipos_de_cursos'] = TipoDeCurso::allWithOtherLast();
        return $data;
    }

    protected function step_basico_for_proyecto(Request $request, $should_validate = true) {
        $data = $this->step_basico_for_all($request, $should_validate);
        $data['tipos_de_proyectos'] = TipoDeProyecto::allWithOtherLast();
        return $data;
    }

    protected function step_basico_for_publicacion(Request $request, $should_validate = true) {
        $data = $this->step_basico_for_all($request, $should_validate);
        $data['tipos_de_publicaciones'] = TipoDePublicacion::allWithOtherLast();
        return $data;
    }

    protected function validate_extra_for_all(Request $request) {
        $textarea_max_length = Voyager::setting('site.max_chars_textarea') ?: CargaController::DEFAULT_TEXTAREA_MAX_LENGTH;
        $string_max_length = Voyager::setting('site.max_chars_string') ?: CargaController::DEFAULT_STRING_LENGTH;
        $digits_max_length = Voyager::setting('site.max_digits_number') ?: CargaController::DEFAULT_NUM_DIGITS_LENGTH;
        $modo = $request->get('modo');

        $request->validate([
            'tipo_de_'.$modo.'_id'         => 'required|not_in:0',
            'nombre'                       => 'required|max:'.$string_max_length,
            'descripcion'                  => 'required|max:'.$textarea_max_length,
            'institucion_id'               => 'required|not_in:0',
            'dependencia_institucional_id' => 'required|not_in:0',
            'contacto_nombre'              => 'required|max:'.$string_max_length,
            'contacto_telefono'            => 'required|max:'.$string_max_length,
            'contacto_email'               => 'required|max:'.$string_max_length,
            'responsables_detalles'        => 'max:'.$textarea_max_length,
            'estrategia_metodologica'      => 'max:'.$textarea_max_length,
            'condiciones'                  => 'max:'.$textarea_max_length,
            'otros'                        => 'max:'.$textarea_max_length,
            'articulacion_detalles'        => 'max:'.$textarea_max_length,
        ]);
    }

    protected function step_extra_for_all(Request $request, $should_validate = true) {
        $modo = $request->get('modo');
        if ($should_validate) {
            $this->validate_extra_for_all($request);
        }
        return [
            'tipo_de_'.$modo.'_id' => $request->get('tipo_de_'.$modo.'_id'),
            'nombre' => $request->get('nombre'),
            'institucion_id' => $request->get('institucion_id'),
            'dependencia_institucional_id' => $request->get('dependencia_institucional_id'),
            'descripcion' => $request->get('descripcion'),
            'responsables_nombres' => $request->get('responsables_nombres'),
            'responsables_detalles' => $request->get('responsables_detalles'),
            'contacto_nombre' => $request->get('contacto_nombre'),
            'contacto_telefono' => $request->get('contacto_telefono'),
            'contacto_email' => $request->get('contacto_email'),
            'tipos_de_articulaciones' => TipoDeArticulacion::allWithOtherLast(),
            'max_upload_files' => (Voyager::setting('site.max_upload_files') ?: CargaController::DEFAULT_MAX_UPLOAD_FILES),
            'max_upload_file_size' => (Voyager::setting('site.max_upload_file_size') ?: CargaController::DEFAULT_MAX_UPLOAD_FILE_SIZE),
        ];
    }

    protected function step_extra_for_curso(Request $request, $should_validate = true) {
        $data = $this->step_extra_for_all($request, $should_validate);
        return array_merge($data, [
            'tipos_de_intensidades' => TipoDeIntensidad::allWithOtherLast(),
            'tipos_de_niveles' => TipoDeNivel::allWithOtherLast(),
            'tipos_de_regularidades' => TipoDeRegularidad::allWithOtherLast(),
            'tipos_de_curricularidades' => TipoDeCurricularidad::allWithOtherLast(),
            'tipos_de_modalidades' => TipoDeModalidad::allWithOtherLast(),
        ]);
    }

    protected function step_extra_for_proyecto(Request $request, $should_validate = true) {
        return $this->step_extra_for_all($request, $should_validate);
    }

    protected function step_extra_for_publicacion(Request $request, $should_validate = true) {
        return $this->step_extra_for_all($request, $should_validate);
    }

    protected function validate_final_for_all($request) {
        // all data present in basic must still be present
        $this->validate_extra_for_all($request);

        $max_file_size_mb = (Voyager::setting('site.max_upload_filesize') ?: CargaController::DEFAULT_MAX_UPLOAD_FILE_SIZE);
        $max_file_size = 1000 * $max_file_size_mb;

        $request->validate([
            'adjuntos.0' => 'file|max:'.$max_file_size,
            'adjuntos.1' => 'file|max:'.$max_file_size,
            'adjuntos.2' => 'file|max:'.$max_file_size,
            'adjuntos.3' => 'file|max:'.$max_file_size,
            'adjuntos.4' => 'file|max:'.$max_file_size,
            'adjuntos.5' => 'file|max:'.$max_file_size,
            'adjuntos.6' => 'file|max:'.$max_file_size,
            'adjuntos.7' => 'file|max:'.$max_file_size,
            'adjuntos.8' => 'file|max:'.$max_file_size,
            'adjuntos.9' => 'file|max:'.$max_file_size
        ]);
    }

    protected function validate_final_for_curso($request) {
        $textarea_max_length = Voyager::setting('site.max_chars_textarea') ?: CargaController::DEFAULT_TEXTAREA_MAX_LENGTH;
        $string_max_length = Voyager::setting('site.max_chars_string') ?: CargaController::DEFAULT_STRING_LENGTH;
        $digits_max_length = Voyager::setting('site.max_digits_number') ?: CargaController::DEFAULT_NUM_DIGITS_LENGTH;
        $request->validate([
            'estrategia_metodologica' => 'max:'.$textarea_max_length,
            'condiciones' => 'max:'.$textarea_max_length,
            'otros' => 'max:'.$textarea_max_length,
            'articulacion_detalles' => 'max:'.$textarea_max_length,
            'tipo_de_articulacion_id' => 'digits_between:0,'.$digits_max_length,
            'tipo_de_intensidad_id' => 'required|not_in:0',
            'tipo_de_modalidad_id' => 'required|not_in:0',
            'tipo_de_regularidad_id' => 'required|not_in:0',
            'tipo_de_nivel_id' => 'required|not_in:0',
            'tipo_de_curricularidad_id' => 'required|not_in:0',
        ]);
    }

    protected function validate_final_for_proyecto($request) {
        $textarea_max_length = Voyager::setting('site.max_chars_textarea') ?: CargaController::DEFAULT_TEXTAREA_MAX_LENGTH;
        $string_max_length = Voyager::setting('site.max_chars_string') ?: CargaController::DEFAULT_STRING_LENGTH;
        $digits_max_length = Voyager::setting('site.max_digits_number') ?: CargaController::DEFAULT_NUM_DIGITS_LENGTH;
        $request->validate([
            'destinatarios' => 'required|max:'.$textarea_max_length
        ]);
    }

    protected function validate_final_for_publicacion($request) {
        $textarea_max_length = Voyager::setting('site.max_chars_textarea') ?: CargaController::DEFAULT_TEXTAREA_MAX_LENGTH;
        $string_max_length = Voyager::setting('site.max_chars_string') ?: CargaController::DEFAULT_STRING_LENGTH;
        $digits_max_length = Voyager::setting('site.max_digits_number') ?: CargaController::DEFAULT_NUM_DIGITS_LENGTH;
        $request->validate([
            'nombre'  => 'max:'.$string_max_length,
            'anho'    => 'max:'.$string_max_length,
            'ciudad'  => 'max:'.$string_max_length,
            'serie'   => 'max:'.$string_max_length,
            'edicion' => 'max:'.$string_max_length,
            'isbn'    => 'max:'.$string_max_length,
        ]);
    }

    protected function step_final_for_all(Request $request, $should_validate = true, $location = 'archivos') {
        if ($should_validate) {
            $this->validate_final_for_all($request);
        }
        $files = [];
        if ($request->file('adjuntos')) {
            foreach ($request->file('adjuntos') as $adjunto) {
                $subfolder = Carbon::today()->format('FY');
                $adjunto->store($location.'/'.$subfolder, 'public');
                $file = [
                    'download_link' => $location.'/'.$subfolder.'/'.$adjunto->hashName(),
                    'original_name' => $adjunto->getClientOriginalName()
                ];
                array_push($files, $file);
            }
            $request['archivos_adjuntos'] = json_encode($files);
        }
        return [];
    }

    protected function step_final_for_curso(Request $request, $should_validate = true) {
        if ($should_validate) {
            $this->validate_final_for_curso($request);
        }

        $this->step_final_for_all($request, $should_validate, 'cursos');
        Curso::create($request->all());
        return [];
    }

    protected function step_final_for_proyecto(Request $request, $should_validate = true) {
        if ($should_validate) {
            $this->validate_final_for_proyecto($request);
        }
        $this->step_final_for_all($request, $should_validate, 'proyectos');
        Proyecto::create($request->all());
        return [];
    }

    protected function step_final_for_publicacion(Request $request, $should_validate = true) {
        $this->step_final_for_all($request, $should_validate, 'publicaciones');
        Publicacion::create($request->all());
        return [];
    }

    protected function step_institucion_for_all(Request $request, $should_validate = true) {
        return [
            'instituciones' => Institucion::orderBy('nombre', 'ASC')->get(),
            'dependencias_institucionales' => DependenciaInstitucional::orderBy('nombre', 'ASC')->get(),
            'paises' => Pais::allWithOtherLast(),
            'regiones' => Region::allWithOtherLast(),
            'provincias' => Provincia::allWithOtherLast(),
            'tipos_de_dependencias_institucionales' => TipoDeDependenciaInstitucional::allWithOtherLast(),
            'tipos_de_instituciones' => TipoDeInstitucion::allWithOtherLast()
        ];
    }

    protected function validate_final_institucion_for_all(Request $request) {
        $textarea_max_length = Voyager::setting('site.max_chars_textarea') ?: CargaController::DEFAULT_TEXTAREA_MAX_LENGTH;
        $string_max_length = Voyager::setting('site.max_chars_string') ?: CargaController::DEFAULT_STRING_LENGTH;
        $digits_max_length = Voyager::setting('site.max_digits_number') ?: CargaController::DEFAULT_NUM_DIGITS_LENGTH;

        if (!$request->has('institucion_id') || $request->get('institucion_id') == 0) {
            // No institucion_id selected a new institucion should be given
            $request->validate([
                'nombre' => 'required|max:'.$string_max_length,
                'direccion' => 'required|max:'.$string_max_length,
                'codigo_postal' => 'required|max:'.$string_max_length,
                'telefono' => 'required|max:'.$string_max_length,
                'email' => 'required|email|max:'.$string_max_length,
                'pais' => 'required|not_in:0|max:'.$string_max_length,
                'region' => 'required|not_in:0|max:'.$string_max_length,
                'provincia' => 'required|not_in:0|max:'.$string_max_length,
                'tipo_de_institucion_id' => 'required|not_in:0',
                'dependencia_institucional' => 'required',
                'tipo_de_dependencia_institucional_id' => 'required|not_in:0'
            ]);
            // If all goes well, check that the givel name for the institución does
            // not match totally or partially with a previously defined one.
            // Fail if it does.
            $institucion = $request->nombre;
            $institucion = strtolower(trim(strtok($institucion, '(')));
            $rows = Institucion
                ::whereRaw('LOWER(nombre) COLLATE utf8mb4_unicode_ci LIKE (?)', '%'.$institucion.'%')
                ->where('pais_id', $request->pais)
                ->get();
            if (count($rows) > 0) {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    'nombre' => __('carga.wizard_institucion.nombre.error_duplicate', ['nombre' => $rows[0]->nombre])
                ]);
            }
        } else {
            $request->validate([
                'dependencia_institucional' => 'required',
                'tipo_de_dependencia_institucional_id' => 'required|not_in:0'
            ]);
        }
    }

    protected function step_final_institucion_for_all(Request $request, $should_validate = true) {
        if ($should_validate) {
            $this->validate_final_institucion_for_all($request);
        }
        if (!$request->has('institucion_id') || $request->get('institucion_id') == 0) {
            $institucion = Institucion::create([
                'nombre' => $request->nombre,
                'descripcion' => __('carga.final_institucion.default_descripcion'),
                'direccion' => $request->direccion,
                'codigo_postal' => $request->codigo_postal,
                'telefono' => $request->telefono,
                'email' => $request->email,
                'tipo_de_institucion_id' => $request->tipo_de_institucion_id,
                'pais_id' => $request->pais,
                'region_id' => $request->region,
                'provincia_id' => $request->provincia
            ]);
        } else {
            $institucion = Institucion::find($request->institucion_id);
        }
        DependenciaInstitucional::create([
            'institucion_id' => $institucion->id,
            'nombre' => $request->dependencia_institucional,
            'tipo_de_dependencia_institucional_id' => $request->tipo_de_dependencia_institucional_id,
            'descripcion' => __('carga.final_institucion.default_descripcion')
        ]);
        return [];
    }

    protected function step_institucion_for_curso(Request $request, $should_validate = true) {
        return $this->step_institucion_for_all($request, $should_validate);
    }
    protected function step_institucion_for_proyecto(Request $request, $should_validate = true) {
        return $this->step_institucion_for_all($request, $should_validate);
    }
    protected function step_institucion_for_publicacion(Request $request, $should_validate = true) {
        return $this->step_institucion_for_all($request, $should_validate);
    }
}
