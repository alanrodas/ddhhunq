<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class DataPageController extends Controller {

    public static function routes() {
        Route::get('/', ['as' => 'home', 'uses' => 'Pages\DataPageController@index']);
        Route::get('pages/{page_id}', ['as' => 'datapage', 'uses' => 'Pages\DataPageController@index']);
    }

    protected function index($page_id = 'home') {
        Log::info($page_id);
        $item = DB::table('paginas')->where('nombre', $page_id)->first();
        if (!$item) {
            abort(404);
        }
        return view('home', ['contenido' => $item->contenido]);
    }

}
