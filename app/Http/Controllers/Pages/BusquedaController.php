<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\Http\Controllers\Controller;
use App\Helpers\BuscadorHelper;

use App\Models\Locaciones\Institucion;
use App\Models\Locaciones\DependenciaInstitucional;
use App\Models\Locaciones\Pais;
use App\Models\Locaciones\Region;
use App\Models\Locaciones\Provincia;
use App\Models\Publicaciones\Publicacion;
use App\Models\Publicaciones\TipoDePublicacion;
use App\Models\Proyectos\Proyecto;
use App\Models\Proyectos\TipoDeProyecto;
use App\Models\Cursos\Curso;
use App\Models\Cursos\TipoDeCurso;
use App\Models\Cursos\TipoDeModalidad;
use App\Models\Cursos\TipoDeNivel;

class BusquedaController extends Controller {

    public static function routes() {
        Route::get('busqueda', ['as' => 'busqueda.simple', 'uses' => 'Pages\BusquedaController@handle_simple']);
        Route::get('busqueda_avanzada', ['as' => 'busqueda.avanzada', 'uses' => 'Pages\BusquedaController@get_avanzada']);
        Route::post('busqueda_avanzada', ['as' => 'busqueda.avanzada_handle', 'uses' => 'Pages\BusquedaController@handle_avanzada']);
        Route::get('item', ['as' => 'busqueda.item', 'uses' => 'Pages\BusquedaController@get_item']);
    }

    public function handle_simple(Request $request) {
        if (!$request->has('query')) { return view('busqueda'); }

        $query_string =
            ($request->query('query') === null || $request->query('query') === '%')
            ? '%' : '%'.$request->query('query').'%';

        $request->merge(['query' => $query_string]);

        $page = $request->query('page') ?: 1;
        $request->merge(['page' => $page]);

        $result = BuscadorHelper::busqueda_simple($query_string, $page);

        $result = $result->appends(Input::except('page'));

        return view('busqueda', ['resultados' => $result]);
    }

    public function get_avanzada(Request $request) {
        return view('busqueda_avanzada', [
            'instituciones' => Institucion::all(),
            'dependencias_institucionales' => DependenciaInstitucional::all(),
            'paises' => Pais::all(),
            'regiones' => Region::all(),
            'provincias' => Provincia::all(),
            'tipos_de_publicaciones' => TipoDePublicacion::all(),
            'tipos_de_proyectos' => TipoDeProyecto::all(),
            'tipos_de_cursos' => TipoDeCurso::all(),
            'tipos_de_modalidades' => TipoDeModalidad::all(),
            'tipos_de_niveles' => TipoDeNivel::all()
        ]);
    }

    public function handle_avanzada(Request $request) {
        $page = $request->query('page') ?: 1;
        $request->merge(['page' => $page]);

        $result = BuscadorHelper::busqueda_avanzada($request, $page);

        // $result = $result->appends(Input::except('page'));

        return view('busqueda_avanzada', [
            'instituciones' => Institucion::all(),
            'dependencias_institucionales' => DependenciaInstitucional::all(),
            'paises' => Pais::all(),
            'regiones' => Region::all(),
            'provincias' => Provincia::all(),
            'tipos_de_publicaciones' => TipoDePublicacion::all(),
            'tipos_de_proyectos' => TipoDeProyecto::all(),
            'tipos_de_cursos' => TipoDeCurso::all(),
            'tipos_de_modalidades' => TipoDeModalidad::all(),
            'tipos_de_niveles' => TipoDeNivel::all(),
            'resultados' => $result
        ]);
    }

    public function get_item(Request $request) {
        if (!$request->has('id') || !$request->has('modo')) { return abort(404); }
        if ($request->get('modo') === 'curso') {
            $item = Curso::findOrFail($request->get('id'));
        } elseif ($request->get('modo') === 'publicacion') {
            $item = Publicacion::findOrFail($request->get('id'));
        } elseif ($request->get('modo') === 'proyecto') {
            $item = Proyecto::findOrFail($request->get('id'));
        } else {
            return abort(404);
        }
        return view('item', [
            'item' => $item,
            'modo' => $request->get('modo')
        ]);
    }
}
