<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

use App\Models\Locaciones\Institucion;
use App\Models\Locaciones\DependenciaInstitucional;
use App\Models\Locaciones\Pais;
use App\Models\Locaciones\Region;
use App\Models\Locaciones\Provincia;
use App\Models\Cursos\TipoDeCurso;
use App\Models\Proyectos\TipoDeProyecto;
use App\Models\Publicaciones\TipoDePublicacion;

class StatisticsController extends Controller {

    public static function routes() {
        Route::get('estadisticas', ['as' => 'voyager.stats.view', 'uses' => 'Voyager\StatisticsController@handle']);
    }

    public static function categories() {
        return [
            (object)['nombre' => __('stats.categories.cursos'), 'id' => 'cursos'],
            (object)['nombre' => __('stats.categories.proyectos'), 'id' => 'proyectos'],
            (object)['nombre' => __('stats.categories.publicaciones'), 'id' => 'publicaciones']
        ];
    }

    public static function typesInCategories() {
        $all_categories_types = [];
        foreach (TipoDeCurso::all() as $e) {
            $element = new \stdClass();
            $element->id = $e->id;
            $element->nombre = $e->nombre;
            $element->categoria = 'cursos';
            array_push($all_categories_types, $element);
        }
        foreach (TipoDeProyecto::all() as $e) {
            $element = new \stdClass();
            $element->id = $e->id;
            $element->nombre = $e->nombre;
            $element->categoria = 'proyectos';
            array_push($all_categories_types, $element);
        }
        foreach (TipoDePublicacion::all() as $e) {
            $element = new \stdClass();
            $element->id = $e->id;
            $element->nombre = $e->nombre;
            $element->categoria = 'publicaciones';
            array_push($all_categories_types, $element);
        }
        return $all_categories_types;
    }

    public static function groupingByCategory() {
        $generales = [
            (object)[
                'nombre' => __('stats.filters.paises.nombre'),
                'id' => 'paises.nombre',
                'categoria_id' => 'all'
            ],
            (object)[
                'nombre' => __('stats.filters.regiones.nombre'),
                'id' => 'regiones.nombre',
                'categoria_id' => 'all'
            ],
            (object)[
                'nombre' => __('stats.filters.provincias.nombre'),
                'id' => 'provincias.nombre',
                'categoria_id' => 'all'
            ],
            (object)[
                'nombre' => __('stats.filters.instituciones.nombre'),
                'id' => 'instituciones.nombre',
                'categoria_id' => 'all'
            ],
            (object)[
                'nombre' => __('stats.filters.dependencias_institucionales.nombre'),
                'id' => 'dependencias_institucionales.nombre',
                'categoria_id' => 'all'
            ],
            (object)[
                'nombre' => __('stats.filters.categoria'),
                'id' => 'categoria',
                'categoria_id' => 'all'
            ],
        ];

        $cursos = [
            (object)[
                'nombre' => __('stats.filters.tipos_de_cursos.nombre'),
                'id' => 'tipos_de_cursos.nombre',
                'categoria_id' => 'cursos'
            ],
            (object)[
                'nombre' => __('stats.filters.tipos_de_curricularidades.nombre'),
                'id' => 'tipos_de_curricularidades.nombre',
                'categoria_id' => 'cursos'
            ],
            (object)[
                'nombre' => __('stats.filters.tipos_de_intensidades.nombre'),
                'id' => 'tipos_de_intensidades.nombre',
                'categoria_id' => 'cursos'
            ],
            (object)[
                'nombre' => __('stats.filters.tipos_de_modalidades.nombre'),
                'id' => 'tipos_de_modalidades.nombre',
                'categoria_id' => 'cursos'
            ],
            (object)[
                'nombre' => __('stats.filters.tipos_de_regularidades.nombre'),
                'id' => 'tipos_de_regularidades.nombre',
                'categoria_id' => 'cursos'
            ],
            (object)[
                'nombre' => __('stats.filters.tipos_de_niveles.nombre'),
                'id' => 'tipos_de_niveles.nombre',
                'categoria_id' => 'cursos'
            ],
            (object)[
                'nombre' => __('stats.filters.tipos_de_articulaciones.nombre'),
                'id' => 'tipos_de_articulaciones.nombre',
                'categoria_id' => 'cursos'
            ],
        ];

        $proyectos = [
            (object)[
                'nombre' => __('stats.filters.tipos_de_proyectos.nombre'),
                'id' => 'tipos_de_proyectos.nombre',
                'categoria_id' => 'proyectos'
            ],
            (object)[
                'nombre' => __('stats.filters.tipos_de_articulaciones.nombre'),
                'id' => 'tipos_de_articulaciones.nombre',
                'categoria_id' => 'proyectos'
            ],
        ];

        $publicaciones = [
            (object)[
                'nombre' => __('stats.filters.tipos_de_publicaciones.nombre'),
                'id' => 'tipos_de_publicaciones.nombre',
                'categoria_id' => 'publicaciones'
            ]
        ];

        foreach ($generales as $item) {
            array_push($cursos, (object)[
                'nombre' => $item->nombre,
                'id'=> $item->id,
                'categoria_id' => 'cursos'
            ]);
            array_push($proyectos, (object)[
                'nombre' => $item->nombre,
                'id'=> $item->id,
                'categoria_id' => 'proyectos'
            ]);
            array_push($publicaciones, (object)[
                'nombre' => $item->nombre,
                'id'=> $item->id,
                'categoria_id' => 'publicaciones'
            ]);
        }

        return array_merge($generales, $cursos, $proyectos, $publicaciones);
    }

    public static function graphTypes() {
        return [
            (object)['nombre' => __('stats.graficos.pie'), 'id' => 'pie'],
            (object)['nombre' => __('stats.graficos.doughnut'), 'id' => 'doughnut'],
            (object)['nombre' => __('stats.graficos.bar'), 'id' => 'bar'],
            (object)['nombre' => __('stats.graficos.bar_horizontal'), 'id' => 'bar_horizontal']
        ];
    }

    public static function orders() {
        return [
            (object)['nombre' => __('stats.ordenes.nombre_asc'), 'id' => 'nombre_asc'],
            (object)['nombre' => __('stats.ordenes.nombre_desc'), 'id' => 'nombre_desc'],
            (object)['nombre' => __('stats.ordenes.total_asc'), 'id' => 'total_asc'],
            (object)['nombre' => __('stats.ordenes.total_desc'), 'id' => 'total_desc']
        ];
    }

    public function handle(Request $request) {
        if ($request->query->count() == 0) {
            return $this->view();
        } else {
            return $this->graph($request);
        }
    }

    public function view() {
        return view('/vendor/voyager/statistics/showstats', [
            'paises' => Pais::all(),
            'regiones' => Region::all(),
            'provincias' => Provincia::all(),
            'instituciones' => Institucion::all(),
            'dependencias_institucionales' => DependenciaInstitucional::all(),
            'tipos_de_categoria' => StatisticsController::typesInCategories(),
            'categorias' => StatisticsController::categories(),
            'tipos_de_graficos' => StatisticsController::graphTypes(),
            'agrupamientos' => StatisticsController::groupingByCategory(),
            'ordenes' => StatisticsController::orders(),
            'ver_grafico' => false,
            'getSortUrl' => [$this, 'getSortUrl']
        ]);
    }

    public function graph(Request $request) {
        if ($request->query->count() == 0) {
            $request->redirect('voyager.stats.view');
        }

        $results = [];
        $queries = [];
        $index = 0;
        if ($request->query->has('agrupamiento')) {
            foreach (StatisticsController::categories() as $category) {
                if (!$request->query->has('categoria')
                    || $request->query->get('categoria') === 'all'
                    || $request->query->get('categoria') === $category->id
                ) {
                    $query = $this->baseDBQueryFor($category->id);
                    $query = $this->applyFilterFromRequestToQuery($query, $request->query);

                    if ($request->query->get('agrupamiento') !== 'categoria') {
                        $selectStatement = $request->query->get('agrupamiento') . ' as nombre, ';
                    } else {
                        $selectStatement = '"'. $category->nombre .  '" as nombre, ';
                    }
                    foreach (StatisticsController::categories() as $anotherCategory) {
                        if ($anotherCategory->id != $category->id) {
                            $selectStatement .= '0 as '.$anotherCategory->id.', ';
                        } else {
                            $selectStatement .= 'count(*) as '.$anotherCategory->id.', ';
                        }
                    }
                    $selectStatement .= 'count(*) as total';

                    $query->selectRaw($selectStatement);
                    if ($request->query->get('agrupamiento') !== 'categoria') {
                        $query->groupBy($request->query->get('agrupamiento'));
                    } else {
                        $query->groupBy('nombre');
                    }
                    $queries[$index] = $query;
                    $index++;
                }
            }

            for ($i=0; $i < $index-1; $i++) {
                $queries[$i+1] = $queries[$i]->union($queries[$i+1]);
            }
            $index--;

            $aggregatedQuery = DB::table(DB::raw('('.StatisticsController::getEloquentSqlWithBindings($queries[$index]).') AS s'));
            $selectionClause = 'nombre, ';
            if ($request->query->get('agrupamiento') !== 'categoria') {
                foreach (StatisticsController::categories() as $anotherCategory) {
                    $selectionClause .= 'SUM('.$anotherCategory->id.') AS '.$anotherCategory->id.', ';
                }
            }
            $selectionClause .= 'SUM(total) AS total';
            $aggregatedQuery = $aggregatedQuery
                ->selectRaw($selectionClause)
                ->groupBy('nombre');
            if ($request->query->has('order_by')) {
                $aggregatedQuery->orderBy(
                    $request->query->get('order_by'),
                    $request->query->has('sort_order') ? $request->query->get('sort_order') : 'asc'
                );
            } else {
                $aggregatedQuery->orderBy('nombre');
            }
            $results = $aggregatedQuery->get();
        }

        $grafico_tipo = $request->query->has('tipo_de_grafico') ? $request->query->get('tipo_de_grafico') : 'pie';
        $grafico_agrupamiento = $request->query->has('agrupamiento') ? $request->query->get('agrupamiento') : 'categoria';
        $grafico_ver_tabla = $request->query->has('ver_tabla') ? 'si' : 'no';
        $grafico_mostrar_etiquetas = $request->query->has('mostrar_etiquetas') ? 'si' : 'no';

        session()->flashInput($request->input());

        return view('/vendor/voyager/statistics/showstats', [
            'paises' => Pais::all(),
            'regiones' => Region::all(),
            'provincias' => Provincia::all(),
            'instituciones' => Institucion::all(),
            'dependencias_institucionales' => DependenciaInstitucional::all(),
            'tipos_de_categoria' => StatisticsController::typesInCategories(),
            'categorias' => StatisticsController::categories(),
            'tipos_de_graficos' => StatisticsController::graphTypes(),
            'agrupamientos' => StatisticsController::groupingByCategory(),
            'ordenes' => StatisticsController::orders(),
            'ver_grafico' => true,
            'grafico_tipo' => $grafico_tipo,
            'grafico_agrupamiento' => $grafico_agrupamiento,
            'grafico_ver_tabla' => $grafico_ver_tabla,
            'grafico_mostrar_etiquetas' => $grafico_mostrar_etiquetas,
            'results' => $results,
            'getSortUrl' => [$this, 'getSortUrl'],
            'graphDescriptionTexts' => $this->getGraphDescriptionTexts(
                $grafico_tipo,
                $grafico_agrupamiento,
                $grafico_ver_tabla,
                $grafico_mostrar_etiquetas,
                $request
            )
        ]);
    }

    private function getGraphDescriptionTexts($grafico_tipo, $grafico_agrupamiento, $grafico_ver_tabla, $grafico_mostrar_etiquetas, $request) {
        $graphType = $this->getNombreInArrayById(StatisticsController::graphTypes(), $grafico_tipo);
        $groupType = $this->getNombreInArrayById(StatisticsController::groupingByCategory(), $grafico_agrupamiento);
        $graph = ' en un gráfico de ' . strtolower($graphType) . ' agrupado ' . strtolower($groupType) . '.';
        $graphAdded =  (!$grafico_ver_tabla || $grafico_ver_tabla == 'no') ? '' : 'Debajo del gráfico se muestra una tabla con información detallada de los datos.';

        $pais = $this->getNombreInArrayByIdIfNonZeroOrNull(Pais::all(), 'pais', $request);
        $region = $this->getNombreInArrayByIdIfNonZeroOrNull(Region::all(), 'region', $request);
        $provincia = $this->getNombreInArrayByIdIfNonZeroOrNull(Provincia::all(), 'provincia', $request);
        $institucion = $this->getNombreInArrayByIdIfNonZeroOrNull(Institucion::all(), 'institucion', $request);
        $dependencia = $this->getNombreInArrayByIdIfNonZeroOrNull(DependenciaInstitucional::all(), 'dependencia_institucional', $request);
        $categoria = $this->getNombreInArrayByIdIfNonZeroOrNull(StatisticsController::categories(), 'categoria', $request);

        $categoriaTexto = $categoria ? ('solo los ' . strtolower($categoria)) : 'todos los datos';
        if ($pais && $region && $provincia && $institucion && $dependencia) {
            $filer = 'Se muestran ' . $categoriaTexto . ' de la base';
        } else {
            $filter = 'Se muestran ' . $categoriaTexto . ' ';
            if ($dependencia) {
                $filter .= 'de ' . $dependencia . ' de ' . $institucion;
            }
            elseif ($institucion) {
                $filter .= 'de ' . $institucion;
            }
            elseif ($provincia) {
                $filter .= 'de la provincia ' . $provincia . ' de ' . $pais;
            }
            elseif ($region) {
                $filter .= 'de la región ' . $region . ' de ' . $pais;
            }
            elseif ($pais) {
                $filter .= 'del país ' . $pais;
            }
        }
        return [$filter . $graph, $graphAdded];
    }

    private function getNombreInArrayByIdIfNonZeroOrNull($array, $id, $request) {
        return ($request->query->get($id) !== '0'
            ? $this->getNombreInArrayById($array, $request->query->get($id))
            : false
        );
    }

    private function getNombreInArrayById($array, $id) {
        foreach ($array as $element) {
            if ($element->id == $id) {
                return $element->nombre;
            }
        }
        return '';
    }

    public function getSortUrl($orderBy) {
        $params = [];
        parse_str(parse_url(URL::full(), PHP_URL_QUERY), $params);

        // No previous order (or sort with no order, which makes no sense except manually added)
        if (empty($params)) {
            return URL::current() . '?' . http_build_query(['sort_order' => 'asc', 'order_by' => $orderBy]);
        } else if (!array_key_exists('order_by', $params) && !array_key_exists('sort_order', $params)) {
            // deault order is by name, asc, if no order was in url, but name is given, sort desc
            return URL::full() . '&' . http_build_query(['sort_order' => $orderBy == 'nombre' ? 'desc' : 'asc', 'order_by' => $orderBy]);
        } else if (!array_key_exists('order_by', $params) && array_key_exists('sort_order', $params)) {
            return URL::full() . '&' . http_build_query(['order_by' => $orderBy]);
        }
        // there was a previous order in the uri
        else if (array_key_exists('order_by', $params)) {
            // the same order is given, that means, swap the sorting
            if ($params['order_by'] == $orderBy && !array_key_exists('sort_order', $params)) {
                // if no sort, asume asc.
                return URL::full() . '&' . http_build_query(['sort_order' => 'desc']);
            }
            if ($params['order_by'] == $orderBy && array_key_exists('sort_order', $params)) {
                $params['sort_order'] = $params['sort_order'] == 'asc' ? 'desc' : 'asc';
                return URL::current() . '?' . http_build_query($params);
            } else {
                // different order criteria, use that ordering and sort asc by default
                $params['order_by'] = $orderBy;
                $params['sort_order'] = 'asc';
                return URL::current() . '?' . http_build_query($params);
            }
        }
        // Other cases follow this criteria:
        $params['order_by'] = $orderBy;
        $params['sort_order'] = 'asc';
        return URL::current() . '?' . http_build_query($params);
    }

    protected function baseDBQueryFor($item, $doSelects = false) {
        $query = DB::table($item)
            ->leftJoin('instituciones', $item.'.institucion_id', '=', 'instituciones.id')
            ->leftJoin('dependencias_institucionales', $item.'.dependencia_institucional_id', '=', 'dependencias_institucionales.id')
            ->leftJoin('paises', 'instituciones.pais_id', '=', 'paises.id')
            ->leftJoin('regiones', 'instituciones.region_id', '=', 'regiones.id')
            ->leftJoin('provincias', 'instituciones.provincia_id', '=', 'provincias.id');
        if ($doSelects) {
            $query->select(
                $item.'.*',
                'instituciones.nombre as institucion_nombre',
                'dependencias_institucionales.nombre as dependencia_institucional_nombre',
                'instituciones.pais_id as pais_id',
                'instituciones.region_id as region_id',
                'instituciones.provincia_id as provincia_id',
                'paises.nombre as pais_nombre',
                'regiones.nombre as region_nombre',
                'provincias.nombre as provincia_nombre'
            );
        }
        if ($item === 'cursos') {
            $query
                ->leftJoin('tipos_de_cursos', 'cursos.tipo_de_curso_id', '=', 'tipos_de_cursos.id')
                ->leftJoin('tipos_de_curricularidades', 'cursos.tipo_de_curricularidad_id', '=', 'tipos_de_curricularidades.id')
                ->leftJoin('tipos_de_intensidades', 'cursos.tipo_de_intensidad_id', '=', 'tipos_de_intensidades.id')
                ->leftJoin('tipos_de_modalidades', 'cursos.tipo_de_modalidad_id', '=', 'tipos_de_modalidades.id')
                ->leftJoin('tipos_de_regularidades', 'cursos.tipo_de_regularidad_id', '=', 'tipos_de_regularidades.id')
                ->leftJoin('tipos_de_niveles', 'cursos.tipo_de_nivel_id', '=', 'tipos_de_niveles.id')
                ->leftJoin('tipos_de_articulaciones', 'cursos.tipo_de_articulacion_id', '=', 'tipos_de_articulaciones.id');
                if ($doSelects) {
                    $query->addSelect(
                        'tipos_de_cursos.nombre as tipo_de_curso_nombre',
                        'tipos_de_curricularidades.nombre as tipo_de_curricularidad_nombre',
                        'tipos_de_intensidades.nombre as tipo_de_intensidad_nombre',
                        'tipos_de_modalidades.nombre as tipo_de_modalidad_nombre',
                        'tipos_de_regularidades.nombre as tipo_de_regularidad_nombre',
                        'tipos_de_niveles.nombre as tipo_de_nivel_nombre',
                        'tipos_de_articulaciones.nombre as tipo_de_articulacion_nombre'
                    );
                }
        }
        if ($item === 'proyectos') {
            $query
                ->leftJoin('tipos_de_proyectos', $item.'.tipo_de_proyecto_id', '=', 'tipos_de_proyectos.id')
                ->leftJoin('tipos_de_articulaciones', $item.'.tipo_de_articulacion_id', '=', 'tipos_de_articulaciones.id');
                if ($doSelects) {
                    $query->addSelect(
                        'tipos_de_proyectos.nombre as tipo_de_proyecto_nombre',
                        'tipos_de_articulaciones.nombre as tipo_de_articulacion_nombre'
                    );
                }
        }
        if ($item === 'publicaciones') {
            $query
                ->leftJoin('tipos_de_publicaciones', $item.'.tipo_de_publicacion_id', '=', 'tipos_de_publicaciones.id');
            if ($doSelects) {
                $query->addSelect(
                    'tipos_de_publicaciones.nombre as tipo_de_publicacion_nombre'
                );
            }
        }
        return $query;
    }

    protected function applyFilterFromRequestToQuery($query, $requestParams) {
        if ($requestParams->has('pais') && $requestParams->get('pais') != '0') {
            $query->where('paises.id', $requestParams->get('pais'));
        }
        if ($requestParams->has('region') && $requestParams->get('region') != '0') {
            $query->where('regiones.id', $requestParams->get('region'));
        }
        if ($requestParams->has('provincia') && $requestParams->get('provincia') != '0') {
            $query->where('provincias.id', $requestParams->get('provincia'));
        }
        if ($requestParams->has('institucion') && $requestParams->get('institucion') != '0') {
            $query->where('instituciones.id', $requestParams->get('institucion'));
        }
        if ($requestParams->has('dependencia_institucional') && $requestParams->get('dependencia_institucional') != '0') {
            $query->where('dependencias_institucionales.id', $requestParams->get('dependencia_institucional'));
        }
        return $query;
    }


    public static function getEloquentSqlWithBindings($query) {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            $binding = addslashes($binding);
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }
}
