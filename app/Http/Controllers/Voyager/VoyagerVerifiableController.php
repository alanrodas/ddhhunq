<?php
namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

class VoyagerVerifiableController extends VoyagerBaseController {

    public function verify(Request $request, $id) {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $model = app($dataType->model_name);
        $elem = $model->findOrFail($id);
        $elem->verified_at = date('Y-m-d H:i:s', time());
        $elem->save();
        return redirect()->route('voyager.'.$slug.'.index');
    }
}