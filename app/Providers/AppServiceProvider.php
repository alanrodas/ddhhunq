<?php

namespace App\Providers;

use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\ServiceProvider;
use App\Actions\ValidateAction;
use App\Actions\RestoreAction;
use App\Actions\ShowInstitucionesAction;
use App\Actions\ShowCursosAction;
use App\Actions\ShowProyectosAction;
use App\Actions\ShowPublicacionesAction;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Voyager::addAction(RestoreAction::class);
        Voyager::addAction(ValidateAction::class);
        Voyager::addAction(ShowInstitucionesAction::class);
        Voyager::addAction(ShowPublicacionesAction::class);
        Voyager::addAction(ShowProyectosAction::class);
        Voyager::addAction(ShowCursosAction::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
