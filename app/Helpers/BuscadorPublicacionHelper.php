<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use TCG\Voyager\Facades\Voyager;

class BuscadorPublicacionHelper extends Controller {

    public static function busqueda_simple($query_string) {
        $query = BuscadorPublicacionHelper::base_query();
        return BuscadorPublicacionHelper::busqueda_simple_restrictions($query, $query_string);
    }

    protected static function base_query() {
        return DB::table('publicaciones')->select(
            DB::raw("'publicacion' AS modo"),
            // common
            'publicaciones.id AS id',
            'publicaciones.nombre AS nombre',
            'publicaciones.descripcion AS descripcion',
            'publicaciones.responsables_nombres AS responsables_nombres',
            'publicaciones.archivos_adjuntos AS archivos_adjuntos',
            // links
            'instituciones.nombre AS institucion',
            'dependencias_institucionales.nombre AS dependencia_institucional',
            'tipos_de_publicaciones.nombre AS tipo',
            // extra
            DB::raw("NULL AS modalidad"),
            DB::raw("NULL AS nivel"),
            DB::raw("NULL AS fecha_de_inicio"),
            DB::raw("NULL AS fecha_de_fin")
        )
        ->leftJoin('instituciones',                 'publicaciones.institucion_id', '=', 'instituciones.id')
        ->leftJoin('dependencias_institucionales',  'publicaciones.dependencia_institucional_id', '=', 'dependencias_institucionales.id')
        ->leftJoin('paises',                        'instituciones.pais_id', '=', 'paises.id')
        ->leftJoin('regiones',                      'instituciones.region_id', '=', 'regiones.id')
        ->leftJoin('provincias',                    'instituciones.provincia_id', '=', 'provincias.id')
        ->leftJoin('tipos_de_publicaciones',        'publicaciones.tipo_de_publicacion_id', '=', 'tipos_de_publicaciones.id')
        ->whereNull('publicaciones.deleted_at')
        ->whereNotNull('publicaciones.verified_at');
    }

    protected static function busqueda_simple_restrictions($query, $query_string) {
        return $query->where(function($q) use ($query_string) { $q
            ->where('publicaciones.nombre', 'LIKE', $query_string)
            ->orWhere('publicaciones.descripcion', 'LIKE', $query_string)
            ->orWhere('publicaciones.responsables_nombres', 'LIKE', $query_string)
            ->orWhere('instituciones.nombre', 'LIKE', $query_string)
            ->orWhere('dependencias_institucionales.nombre', 'LIKE', $query_string)
            ->orWhere('paises.nombre', 'LIKE', $query_string)
            ->orWhere('regiones.nombre', 'LIKE', $query_string)
            ->orWhere('provincias.nombre', 'LIKE', $query_string);
        });
    }

    public static function busqueda_avanzada($request) {
        extract($request->all());
        $query = BuscadorPublicacionHelper::base_query();
        if (isset($keywords)){
            $query = BuscadorPublicacionHelper::busqueda_simple_restrictions($query, '%'.$keywords.'%');
        }
        if (isset($pais) && $pais != 0) {
            $query->where('instituciones.pais_id', $pais);
        }
        if (isset($region) && $region != 0) {
            $query->where('instituciones.region_id', $region);
        }
        if (isset($provincia) && $provincia != 0) {
            $query->where('instituciones.provincia_id', $provincia);
        }
        if (isset($institucion) && $institucion != 0) {
            $query->where('publicaciones.institucion_id', $institucion);
        }
        if (isset($tipo_de_publicacion) && $tipo_de_publicacion != 0) {
            $query->where('publicaciones.tipo_de_publicacion_id', $tipo_de_publicacion);
        }
        return $query;
    }
}
