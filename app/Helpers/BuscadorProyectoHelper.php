<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use TCG\Voyager\Facades\Voyager;

class BuscadorProyectoHelper extends Controller {

    public static function busqueda_simple($query_string) {
        $query = BuscadorProyectoHelper::base_query();
        return BuscadorProyectoHelper::busqueda_simple_restrictions($query, $query_string);
    }

    protected static function base_query() {
        return DB::table('proyectos')->select(
            DB::raw("'proyecto' AS modo"),
            // common
            'proyectos.id AS id',
            'proyectos.nombre AS nombre',
            'proyectos.descripcion AS descripcion',
            'proyectos.responsables_nombres AS responsables_nombres',
            'proyectos.archivos_adjuntos AS archivos_adjuntos',
            // links
            'instituciones.nombre AS institucion',
            'dependencias_institucionales.nombre AS dependencia_institucional',
            'tipos_de_proyectos.nombre AS tipo',
            // extra
            DB::raw("NULL AS modalidad"),
            DB::raw("NULL AS nivel"),
            DB::raw("NULL AS fecha_de_inicio"),
            DB::raw("NULL AS fecha_de_fin")
        )
        ->leftJoin('instituciones',                 'proyectos.institucion_id', '=', 'instituciones.id')
        ->leftJoin('dependencias_institucionales',  'proyectos.dependencia_institucional_id', '=', 'dependencias_institucionales.id')
        ->leftJoin('paises',                        'instituciones.pais_id', '=', 'paises.id')
        ->leftJoin('regiones',                      'instituciones.region_id', '=', 'regiones.id')
        ->leftJoin('provincias',                    'instituciones.provincia_id', '=', 'provincias.id')
        ->leftJoin('tipos_de_proyectos',            'proyectos.tipo_de_proyecto_id', '=', 'tipos_de_proyectos.id')
        ->whereNull('proyectos.deleted_at')
        ->whereNotNull('proyectos.verified_at');
    }

    protected static function busqueda_simple_restrictions($query, $query_string) {
        return $query->where(function($q) use ($query_string) { $q
            ->where('proyectos.nombre', 'LIKE', $query_string)
            ->orWhere('proyectos.responsables_nombres', 'LIKE', $query_string)
            ->orWhere('proyectos.destinatarios', 'LIKE', $query_string)
            ->orWhere('instituciones.nombre', 'LIKE', $query_string)
            ->orWhere('dependencias_institucionales.nombre', 'LIKE', $query_string)
            ->orWhere('paises.nombre', 'LIKE', $query_string)
            ->orWhere('regiones.nombre', 'LIKE', $query_string)
            ->orWhere('provincias.nombre', 'LIKE', $query_string);
        });
    }

    public static function busqueda_avanzada($request) {
        extract($request->all());
        $query = BuscadorProyectoHelper::base_query();
        if (isset($keywords)){
            $query = BuscadorProyectoHelper::busqueda_simple_restrictions($query, '%'.$keywords.'%');
        }
        if (isset($pais) && $pais != 0) {
            $query->where('instituciones.pais_id', $pais);
        }
        if (isset($region) && $region != 0) {
            $query->where('instituciones.region_id', $region);
        }
        if (isset($provincia) && $provincia != 0) {
            $query->where('instituciones.provincia_id', $provincia);
        }
        if (isset($institucion) && $institucion != 0) {
            $query->where('proyectos.institucion_id', $institucion);
        }
        if (isset($tipo_de_proyecto) && $tipo_de_proyecto != 0) {
            $query->where('proyectos.tipo_de_proyecto_id', $tipo_de_proyecto);
        }
        return $query;
    }
}
