<?php

function join_with($elements, $glue = '', $prefix = '', $suffix = '', $toString = NULL)
{
    if (count($elements) == 0) {
        return $prefix.$suffix;
    }
    $result = $prefix;
    for ($i = 0; $i < count($elements) - 1; $i++)
    {
        if ($toString)
        {
            $result .= call_user_func($toString, $elements[$i]);
        } else {
            $result .= $elements[$i];
        };
        $result .= $glue;
    }
    if ($toString)
    {
        $result .= call_user_func($toString, $elements[$i]);
    } else {
        $result .= $elements[$i];
    };
    $result .= $suffix;
    return $result;
}