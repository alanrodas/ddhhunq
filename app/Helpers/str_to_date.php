<?php

use Carbon\Carbon;

/*
 * Returns a Carbon's Date from a string. If no format is given
 * it attempts to guess the right format based from a set of standard
 * formats used around the world. Note that guessing comes with problems.
 * Dates should only contain day information, but not time. Also, the year
 * must be represented with 4 digits, and not 2. This problems can be solved
 * using a custom format.
 */
function str_to_date($str, $format = null) {
    if ($format) {
        return Carbon::createFromFormat($format, $str);
    }

    $yearMonthDayDashed = "/^[0-9]{4}-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2][0-9]|3[0-1])$/";
    $yearMonthDaySlashed = "/^[0-9]{4}\/(0?[1-9]|1[0-2])\/(0?[1-9]|[1-2][0-9]|3[0-1])$/";

    $yearDayMonthDashed = "/^[0-9]{4}-(0?[1-9]|[1-2][0-9]|3[0-1])-(0?[1-9]|1[0-2])$/";
    $yearDayMonthSlashed = "/^[0-9]{4}\/(0?[1-9]|[1-2][0-9]|3[0-1])\/(0?[1-9]|1[0-2])$/";

    $dayMonthYearDashed = "/^(0?[1-9]|[1-2][0-9]|3[0-1])-(0?[1-9]|1[0-2])-[0-9]{4}-$/";
    $dayMonthYearSlashed = "/^(0?[1-9]|[1-2][0-9]|3[0-1])\/(0?[1-9]|1[0-2])\/[0-9]{4}-$/";

    $monthDayYearDashed = "/^(0?[1-9]|1[0-2])-(0?[1-9]|[1-2][0-9]|3[0-1])-[0-9]{4}-$/";
    $monthDayYearSlashed = "/^(0?[1-9]|1[0-2])\/(0?[1-9]|[1-2][0-9]|3[0-1])\/[0-9]{4}-$/";

    if (preg_match($yearMonthDayDashed, $str)) {
        return Carbon::createFromFormat('Y-m-d', $str);
    }
    if (preg_match($yearMonthDaySlashed, $str)) {
        return Carbon::createFromFormat('Y/m/d', $str);
    }
    if (preg_match($yearDayMonthDashed, $str)) {
        return Carbon::createFromFormat('Y/d/m', $str);
    }
    if (preg_match($yearDayMonthSlashed, $str)) {
        return Carbon::createFromFormat('Y/d/m', $str);
    }
    if (preg_match($dayMonthYearDashed, $str)) {
        return Carbon::createFromFormat('d-m-Y', $str);
    }
    if (preg_match($dayMonthYearSlashed, $str)) {
        return Carbon::createFromFormat('d/m/Y', $str);
    }
    if (preg_match($monthDayYearDashed, $str)) {
        return Carbon::createFromFormat('m-d-Y', $str);
    }
    if (preg_match($monthDayYearSlashed, $str)) {
        return Carbon::createFromFormat('m/d/Y', $str);
    }
    // Fallback to Carbon::parse
    return Carbon::parse($str);
}
