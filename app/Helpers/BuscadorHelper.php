<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use TCG\Voyager\Facades\Voyager;

class BuscadorHelper extends Controller {

    const DEFAULT_PAGE_COUNT = 30;

    public static function busqueda_simple($query_string, $page) {
        $query_curso        = BuscadorCursoHelper::busqueda_simple($query_string);
        $query_proyecto     = BuscadorProyectoHelper::busqueda_simple($query_string);
        $query_publicacion  = BuscadorPublicacionHelper::busqueda_simple($query_string);

        // Put toghether all data
        $query = DB::query()->fromSub(
            $query_curso->union($query_publicacion)->union($query_proyecto),
            'union_query'
        );
        // Order by institucion
        $query->orderBy('institucion');
        // Get pagination amount from settings
        $page_count = Voyager::setting('site.results_per_page') ?: BuscadorHelper::DEFAULT_PAGE_COUNT;
        // Return paginated result
        return $query->paginate($page_count);
    }

    public static function busqueda_avanzada($request, $page) {
        extract($request->all());

        $tipo_is_defined = isset($tipo_de_elemento) && ($tipo_de_elemento !== "0");

        // Perform only needed queries
        if ((!$tipo_is_defined) || $tipo_de_elemento === "curso") {
            $query_curso        = BuscadorCursoHelper::busqueda_avanzada($request);
        }
        if ((!$tipo_is_defined) || $tipo_de_elemento === "proyecto") {
            $query_proyecto     = BuscadorProyectoHelper::busqueda_avanzada($request);
        }
        if ((!$tipo_is_defined) || $tipo_de_elemento === "publicacion") {
            $query_publicacion  = BuscadorPublicacionHelper::busqueda_avanzada($request);
        }

        // Extract query according to performed queries
        if ($tipo_is_defined) {
            $query = isset($query_curso) ? $query_curso : (isset($query_proyecto) ? $query_proyecto : $query_publicacion);
        } else {
            $query = DB::query()->fromSub($query_curso->union($query_proyecto)->union($query_publicacion), 'union_query');
        }

        // Order by institucion
        $query->orderBy('institucion');
        // Get pagination amount from settings
        $page_count = Voyager::setting('site.results_per_page') ?: BuscadorHelper::DEFAULT_PAGE_COUNT;
        // Return paginated result
        return $query->paginate($page_count);
    }

}
