<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use TCG\Voyager\Facades\Voyager;

class BuscadorCursoHelper extends Controller {

    public static function busqueda_simple($query_string) {
        $query = BuscadorCursoHelper::base_query();
        return BuscadorCursoHelper::busqueda_simple_restrictions($query, $query_string);
    }

    protected static function base_query() {
        return DB::table('cursos')->select(
            DB::raw("'curso' AS modo"),
            // common
            'cursos.id AS id',
            'cursos.nombre AS nombre',
            'cursos.descripcion AS descripcion',
            'cursos.responsables_nombres AS responsables_nombres',
            'cursos.archivos_adjuntos AS archivos_adjuntos',
            // linked
            'instituciones.nombre AS institucion',
            'dependencias_institucionales.nombre AS dependencia_institucional',
            'tipos_de_cursos.nombre AS tipo',
            // extra
            'tipos_de_modalidades.nombre AS modalidad',
            'tipos_de_niveles.nombre AS nivel',
            'cursos.fecha_de_inicio AS fecha_de_inicio',
            'cursos.fecha_de_fin AS fecha_de_fin'
        )
        ->leftJoin('instituciones',                 'cursos.institucion_id', '=', 'instituciones.id')
        ->leftJoin('dependencias_institucionales',  'cursos.dependencia_institucional_id', '=', 'dependencias_institucionales.id')
        ->leftJoin('paises',                        'instituciones.pais_id', '=', 'paises.id')
        ->leftJoin('regiones',                      'instituciones.region_id', '=', 'regiones.id')
        ->leftJoin('provincias',                    'instituciones.provincia_id', '=', 'provincias.id')
        ->leftJoin('tipos_de_cursos',               'cursos.tipo_de_curso_id', '=', 'tipos_de_cursos.id')
        ->leftJoin('tipos_de_modalidades',          'cursos.tipo_de_modalidad_id', '=', 'tipos_de_modalidades.id')
        ->leftJoin('tipos_de_niveles',              'cursos.tipo_de_nivel_id', '=', 'tipos_de_niveles.id')
        ->whereNull('cursos.deleted_at')
        ->whereNotNull('cursos.verified_at');
    }

    protected static function busqueda_simple_restrictions($query, $query_string) {
        return $query->where(function($q) use ($query_string) { $q
            ->where('cursos.nombre', 'LIKE', $query_string)
            ->orWhere('cursos.descripcion', 'LIKE', $query_string)
            ->orWhere('cursos.responsables_nombres', 'LIKE', $query_string)
            ->orWhere('instituciones.nombre', 'LIKE', $query_string)
            ->orWhere('dependencias_institucionales.nombre', 'LIKE', $query_string)
            ->orWhere('paises.nombre', 'LIKE', $query_string)
            ->orWhere('regiones.nombre', 'LIKE', $query_string)
            ->orWhere('provincias.nombre', 'LIKE', $query_string);
        });
    }

    public static function busqueda_avanzada($request) {
        extract($request->all());
        $query = BuscadorCursoHelper::base_query();
        if (isset($keywords)){
            $query = BuscadorCursoHelper::busqueda_simple_restrictions($query, '%'.$keywords.'%');
        }
        if (isset($pais) && $pais != 0) {
            $query->where('instituciones.pais_id', $pais);
        }
        if (isset($region) && $region != 0) {
            $query->where('instituciones.region_id', $region);
        }
        if (isset($provincia) && $provincia != 0) {
            $query->where('instituciones.provincia_id', $provincia);
        }
        if (isset($institucion) && $institucion != 0) {
            $query->where('cursos.institucion_id', $institucion);
        }
        if (isset($tipo_de_curso) && $tipo_de_curso != 0) {
            $query->where('cursos.tipo_de_curso_id', $tipo_de_curso);
        }
        if (isset($tipo_de_modalidad) && $tipo_de_modalidad != 0) {
            $query->where('cursos.tipo_de_modalidad_id', $tipo_de_modalidad);
        }
        if (isset($tipo_de_nivel) && $tipo_de_nivel != 0) {
            $query->where('cursos.tipo_de_nivel_id', $tipo_de_nivel);
        }
        return $query;
    }
}
