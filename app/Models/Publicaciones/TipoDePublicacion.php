<?php

namespace App\Models\Publicaciones;

use App\Models\Descriptable;
use App\Models\Model;
use App\Models\Nameable;
use App\Models\SoftDeletable;
use App\Models\Sortable;


class TipoDePublicacion extends Model
{
    use Nameable, Descriptable, Sortable, SoftDeletable;

    public $orderBy = "nombre";
    
    protected $table = 'tipos_de_publicaciones';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        // principal
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'descripcion',                  // MediumText
        // timestamps
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];

    public function publicaciones()
    {
        return $this->hasMany('App\Models\Publicaciones\Publicacion');
    }
}
