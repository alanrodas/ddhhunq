<?php

namespace App\Models\Publicaciones;

use App\Models\Elemento;


class Publicacion extends Elemento
{

    protected $table = 'publicaciones';

    protected $fillable = [
        // principal
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'tipo_de_publicacion_id',       // FK TipoDePublicacion
        'institucion_id',               // FK Locacion\Institucion
        'dependencia_institucional_id', // FK Locacion\DependenciaInstitucional
        'descripcion',                  // Text

        // contacto
        'contacto_nombre',              // Varchar
        'contacto_telefono',            // Varchar
        'contacto_email',               // Varchar

        // representantes
        'responsables_nombres',         // Varchar
        'responsables_detalles',        // Text

        // articulacion
        'tipo_de_articulacion_id',      // FK Articulaciones\TipoDeArticulacion
        'articulacion_detalles',        // Text

        // archivo
        'archivos_adjuntos',            // File

        // otros datos
        'editorial',                    // Text
        'anho',                         // Text
        'ciudad',                       // Text
        'serie',                        // Text
        'edicion',                      // Text
        'isbn',                         // Text

        // timestamps
        'verified_at',                  // Timestamp
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];

    public function tipo_de_publicacion() {
        return $this->belongsTo('App\Models\Publicaciones\TipoDePublicacion');
    }

    public function institucion() {
        return $this->belongsTo('App\Models\Locaciones\Institucion');
    }

    public function dependencia_institucional() {
        return $this->belongsTo('App\Models\Locaciones\DependenciaInstitucional');
    }
}
