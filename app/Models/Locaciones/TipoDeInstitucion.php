<?php

namespace App\Models\Locaciones;

use App\Models\Descriptable;
use App\Models\Model;
use App\Models\Nameable;
use App\Models\SoftDeletable;
use App\Models\Sortable;


class TipoDeInstitucion extends Model
{
    use Nameable, Descriptable, Sortable, SoftDeletable;

    public $orderBy = "nombre";
    
    protected $table = 'tipos_de_instituciones';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        // principal
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'descripcion',                  // MediumText
        // timestamps
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];

    public function instituciones()
    {
        return $this->hasMany('App\Models\Locaciones\Institucion');
    }
}
