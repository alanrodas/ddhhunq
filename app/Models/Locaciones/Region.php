<?php

namespace App\Models\Locaciones;

use App\Models\Model;
use App\Models\Nameable;
use App\Models\SoftDeletable;
use App\Models\Sortable;


class Region extends Model
{
    use Nameable, Sortable, SoftDeletable;

    public $orderBy = "nombre";
    
    protected $table = 'regiones';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        // principal
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'pais_id',                      // FK Pais
        // timestamps
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];

    public function pais()
    {
        return $this->belongsTo('App\Models\Locaciones\Pais');
    }

    public function provincias()
    {
        return $this->hasMany('App\Models\Locaciones\Provincia');
    }

}
