<?php

namespace App\Models\Locaciones;

use App\Models\Model;
use App\Models\Nameable;
use App\Models\SoftDeletable;
use App\Models\Sortable;


class Provincia extends Model
{
    use Nameable, Sortable, SoftDeletable;

    public $orderBy = "nombre";
    
    protected $table = 'provincias';

    protected $dates = ['deleted_at'];

    protected $fillable = [
// principal
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'region_id',                    // FK Region
        // timestamps
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];

    public function region()
    {
        return $this->belongsTo('App\Models\Locaciones\Region');
    }

}
