<?php

namespace App\Models\Locaciones;

use App\Models\Model;
use App\Models\Nameable;
use App\Models\SoftDeletable;
use App\Models\Sortable;

class Institucion extends Model
{
    use Nameable, Sortable, SoftDeletable;

    public $orderBy = "nombre";

    protected $table = 'instituciones';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        // principal
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'direccion',                    // Varchar
        'codigo_postal',                // Varchar
        'telefono',                     // Varchar
        'email',                        // Varchar
        'tipo_de_institucion_id',       // FK TipoDeInstitucion
        'orden',                        // TinyInt Unsigned
        // locacion
        'pais_id',                      // FK Pais
        'region_id',                    // FK Region
        'provincia_id',                 // FK Provincia
        // timestamps
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];

    public function tipo_de_institucion() {
        return $this->belongsTo('App\Models\Locaciones\TipoDeInstitucion');
    }

    public function pais() {
        return $this->belongsTo('App\Models\Locaciones\Pais');
    }

    public function region() {
        return $this->belongsTo('App\Models\Locaciones\Region');
    }

    public function provincia() {
        return $this->belongsTo('App\Models\Locaciones\Provincia');
    }
}
