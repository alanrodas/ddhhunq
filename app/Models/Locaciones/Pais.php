<?php

namespace App\Models\Locaciones;

use App\Models\Model;
use App\Models\Nameable;
use App\Models\SoftDeletable;
use App\Models\Sortable;


class Pais extends Model
{
    use Nameable, Sortable, SoftDeletable;

    public $orderBy = "nombre";
    
    protected $table = 'paises';

    protected $dates = ['deleted_at'];

    protected $fields = [
        // principal
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'bandera',                       // Varchar
        // timestamps
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];

    public function regiones()
    {
        return $this->hasMany('App\Models\Locaciones\Region');
    }

}
