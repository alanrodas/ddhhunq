<?php

namespace App\Models\Locaciones;

use App\Models\Model;
use App\Models\Nameable;
use App\Models\SoftDeletable;
use App\Models\Sortable;


class DependenciaInstitucional extends Model
{
    use Nameable, Sortable, SoftDeletable;

    public $orderBy = "nombre";

    protected $table = 'dependencias_institucionales';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        // principal
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'institucion_id',               // FK Institucion
        'tipo_de_dependencia_institucional_id', // FK TipoDeDependenciaInstitucional
        'orden',                        // TinyInt Unsigned
        // timestamps
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];

    public function institucion() {
        return $this->belongsTo('App\Models\Locaciones\Institucion');
    }

    public function tipo_de_dependencia_institucional() {
        return $this->belongsTo('App\Models\Locaciones\TipoDeDependenciaInstitucional');
    }
}
