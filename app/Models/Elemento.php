<?php

namespace App\Models;

use App\Models\Nameable;
use App\Models\Descriptable;
use App\Models\Verifiable;
use App\Models\SoftDeletable;

class Elemento extends Model
{
    use Nameable, Descriptable, SoftDeletable, Verifiable;

    protected $dates = ['deleted_at', 'verified_at'];

    protected $fillable = [
        // datos principales
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'tipos_de_publicacion_id',      // FK TipoDePublicacion
        'institucion_id',               // FK Locacion\Institucion
        'dependencia_institucional_id', // FK Locacion\DependenciaInstitucional
        'descripcion',                  // Text

        // contacto
        'contacto_nombre',              // Varchar
        'contacto_telefono',            // Varchar
        'contacto_email',               // Varchar

        // representantes
        'responsables_nombres',         // Varchar
        'responsables_detalles',        // Text

        // articulacion
        'tipo_de_articulacion_id',      // FK Articulaciones\TipoDeArticulacion
        'articulacion_detalles',        // Text

        // archivo
        'archivos_adjuntos',            // File
    ];

    public function scopeOrdered($query) {
        return $query->orderBy('created_at', 'desc')->get();
    }

    public function adjuntos($index = NULL) {
        if ($index === NULL) {
            return json_decode($this->archivos_adjuntos);
        } else {
            return json_decode($this->archivos_adjuntos)[$index];
        }
    }

    public function count_adjuntos() {
        return ($this->adjuntos() === NULL) ? 0 : count($this->adjuntos());
    }

    public function has_adjuntos() {
        return $this->count_adjuntos() > 0;
    }
}
