<?php

namespace App\Models\Proyectos;

use App\Models\Elemento;


class Proyecto extends Elemento
{

    protected $table = 'proyectos';

    protected $fillable = [
        // datos principales
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'tipo_de_proyecto_id',          // FK TipoDePublicacion
        'institucion_id',               // FK Locacion\Institucion
        'dependencia_institucional_id', // FK Locacion\DependenciaInstitucional
        'descripcion',                  // Text

        // contacto
        'contacto_nombre',              // Varchar
        'contacto_telefono',            // Varchar
        'contacto_email',               // Varchar

        // representantes
        'responsables_nombres',         // Varchar
        'responsables_detalles',        // Text

        // articulacion
        'tipo_de_articulacion_id',      // FK Articulaciones\TipoDeArticulacion
        'articulacion_detalles',        // Text

        // archivo
        'archivos_adjuntos',            // File

        // otros datos
        'destinatarios',                // Text

        // timestamps
        'verified_at',                  // Timestamp
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];

    public function institucion() {
        return $this->belongsTo('App\Models\Locaciones\Institucion');
    }

    public function dependencia_institucional() {
        return $this->belongsTo('App\Models\Locaciones\DependenciaInstitucional');
    }

    public function tipo_de_proyecto() {
        return $this->belongsTo('App\Models\Proyectos\TipoDeProyecto');
    }

    public function tipo_de_articulacion() {
        return $this->belongsTo('App\Models\Articulaciones\TipoDeArticulacion');
    }
}
