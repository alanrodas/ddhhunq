<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

trait Sortable
{

    public function save(array $options = [])
    {
        if (!$this->orden) {
            $current = DB::table($this->table)->orderBy('orden', 'DESC')->value('orden');
            $this->orden = $current +1;
        }
        parent::save($options);
    }

    public static function allWithOtherLast()
    {
        $allButOther = self::where('nombre', 'NOT LIKE', 'Otro%')->where('nombre', 'NOT LIKE', 'Otra%')->orderBy('nombre', 'ASC')->get();
        $other = self::where('nombre', 'LIKE', 'Otro%')->orWhere('nombre', 'LIKE', 'Otra%')->get();

        if (!$other->isEmpty()) {
            $allButOther->push($other->first());
        }
        return $allButOther;
    }
}
