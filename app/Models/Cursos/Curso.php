<?php

namespace App\Models\Cursos;

use App\Models\Elemento;

class Curso extends Elemento
{

    protected $table = 'cursos';

    protected $dates = ['fecha_de_inicio', 'fecha_de_fin', 'deleted_at', 'verified_at'];

    protected $casts = [
        'arancelado' => 'boolean',
    ];

    public function setFechaDeInicioAttribute($value)
    {
        if ($value) {
           $this->attributes['fecha_de_inicio'] = str_to_date($value);
        }
    }

    public function setFechaDeFinAttribute($value)
    {
        if ($value) {
            $this->attributes['fecha_de_fin'] = str_to_date($value);
        }
    }

    protected $fillable = [
        // datos básicos
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'tipo_de_curso_id',             // FK TipoDeCurso
        'institucion_id',               // FK Locaciones\Institucion
        'dependencia_institucional_id', // FK Locaciones\DependenciaInstitucional
        'descripcion',                  // Text

        // contacto
        'contacto_nombre',              // Varchar
        'contacto_telefono',            // Varchar
        'contacto_email',               // Varchar

        // responsables
        'responsables_nombres',         // Varchar
        'responsables_detalles',        // Text

        // articulacion
        'tipo_de_articulacion_id',      // FK Articulaciones\TipoDeArticulacion
        'articulacion_detalles',        // Text

        // archivo
        'archivos_adjuntos',            // File

        // otors datos
        'tipo_de_intensidad_id',        // FK TipoDeIntensidad
        'tipo_de_modalidad_id',         // FK TipoDeModalidad
        'tipo_de_regularidad_id',       // FK TipoDeRegularidad
        'tipo_de_nivel_id',             // FK TipoDeNivel
        'tipo_de_curricularidad_id',    // FK TipoDeCurricularidad
        'arancelado',                   // Bit
        'fecha_de_inicio',              // Date
        'fecha_de_fin',                 // Date

        // otros datos
        'estrategia_metodologica',      // Text
        'condiciones',                  // Text
        'otros',                        // Text

        // timestamps
        'verified_at',                   // Timestamp
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];

    public function institucion() {
        return $this->belongsTo('App\Models\Locaciones\Institucion');
    }

    public function dependencia_institucional() {
        return $this->belongsTo('App\Models\Locaciones\DependenciaInstitucional');
    }

    public function tipo_de_articulacion() {
        return $this->belongsTo('App\Models\Articulaciones\TipoDeArticulacion');
    }

    public function tipo_de_curso() {
        return $this->belongsTo('App\Models\Cursos\TipoDeCurso');
    }

    public function tipo_de_curricularidad() {
        return $this->belongsTo('App\Models\Cursos\TipoDeCurricularidad');
    }

    public function tipo_de_intensidad() {
        return $this->belongsTo('App\Models\Cursos\TipoDeIntensidad');
    }

    public function tipo_de_modalidad() {
        return $this->belongsTo('App\Models\Cursos\TipoDeModalidad');
    }

    public function tipo_de_nivel() {
        return $this->belongsTo('App\Models\Cursos\TipoDeNivel');
    }

    public function tipo_de_regularidad() {
        return $this->belongsTo('App\Models\Cursos\TipoDeRegularidad');
    }

}
