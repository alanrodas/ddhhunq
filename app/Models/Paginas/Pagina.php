<?php

namespace App\Models\Paginas;

use App\Models\Model;
use App\Models\Nameable;
use App\Models\SoftDeletable;


class Pagina extends Model
{
    use Nameable, SoftDeletable;

    protected $table = 'paginas';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        // principal
        'id',                           // Int Unsigned
        'nombre',                       // Varchar
        'contents',                     // RichText

        // timestamps
        'created_at',                   // Timestamp
        'modified_at',                  // Timestamp
        'deleted_at'                    // Timestamp
    ];
}
