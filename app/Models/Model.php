<?php

namespace App\Models;

class Model extends \Illuminate\Database\Eloquent\Model
{
    public $orderBy;
    public $orderDirection = 'ASC';

    public function scopeOrdered($query)
    {
        if ($this->orderBy)
        {
            return $query->orderBy($this->orderBy, $this->orderDirection);
        }

        return $query;
    }

    public function scopeGetOrdered($query)
    {
        return $this->scopeOrdered($query)->get();
    }
}
