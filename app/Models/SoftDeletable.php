<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Facades\Voyager;

trait SoftDeletable
{
    use SoftDeletes;

    abstract public function forceDelete();

    protected $browseSoftDeletesKey = 'browse_soft_deletes';
    protected $deleteSoftDeletesKey = 'delete_soft_deletes';

    public function newQuery()
    {
        if (
            $this->can($this->browseSoftDeletesKey)
            &&
            setting('soft_deletes.'.$this->browseSoftDeletesKey)
        ) {
            return parent::newQuery()->withTrashed();
        } else {
            return parent::newQuery();
        }
    }

    public function delete() {
        if (!is_null($this->deleted_at)
            &&
            $this->can($this->deleteSoftDeletesKey)
            &&
            setting('soft_deletes.'.$this->deleteSoftDeletesKey)
        ) {
            return $this->hardDelete();
        } else {
            return $this->softDelete();
        }
    }

    private function softDelete() {
        return parent::delete();
    }


    private function hardDelete() {
        $this->forceDeleting = true;
        return tap(parent::delete(), function ($deleted) {
            $this->forceDeleting = false;

            if ($deleted) {
                $this->fireModelEvent('forceDeleted', false);
            }
        });
    }

    private function can($permission) {
        try {
            return Voyager::can($permission);
        } catch (Exception $exception) {
            return false;
        }
    }
}
