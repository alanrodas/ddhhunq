<?php

namespace App\Widgets;

use TCG\Voyager\Widgets\BaseDimmer;
use App\Models\Cursos\Curso;

class CursosDimmer extends BaseDimmer
{

    public function run()
    {
        $count = Curso::whereNull('verified_at')->count();
        switch($count) {
            case 0:
                $string = __('widgets.cursos.title.none');
                $text =  __('widgets.cursos.text.none'); break;
            case 1:
                $string = __('widgets.cursos.title.one');
                $text =  __('widgets.cursos.text.one'); break;
            default:
                $string = __('widgets.cursos.title.many', ['count' => $count]);
                $text =  __('widgets.cursos.text.many', ['count' => $count]); break;
        }

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'fas fa-graduation-cap' . ($count ? ' notification' : ''),
            'title'  => $string,
            'text'   => $text,
            'button' => [
                'text' => __('widgets.cursos.btn_link'),
                'link' => route('voyager.cursos.index', ['key' => 'verified_at', 'filter' => 'null']),
            ],
            'image' => 'img/widgets/cursos_dimmer.jpg',
        ]));
    }
}
