<?php

namespace App\Widgets;

use TCG\Voyager\Widgets\BaseDimmer;
use App\Models\Proyectos\Proyecto;

class ProyectosDimmer extends BaseDimmer
{
    public function run()
    {
        $count = Proyecto::whereNull('verified_at')->count();
        switch($count) {
            case 0:
                $string = __('widgets.proyectos.title.none');
                $text =  __('widgets.proyectos.text.none'); break;
            case 1:
                $string = __('widgets.proyectos.title.one');
                $text =  __('widgets.proyectos.text.one'); break;
            default:
                $string = __('widgets.proyectos.title.many', ['count' => $count]);
                $text =  __('widgets.proyectos.text.many', ['count' => $count]); break;
        }

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'fas fa-briefcase' . ($count ? ' notification' : ''),
            'title'  => $string,
            'text'   => $text,
            'button' => [
                'text' => __('widgets.proyectos.btn_link'),
                'link' => route('voyager.proyectos.index', ['key' => 'verified_at', 'filter' => 'null']),
            ],
            'image' => 'img/widgets/proyectos_dimmer.jpg',
        ]));
    }
}
