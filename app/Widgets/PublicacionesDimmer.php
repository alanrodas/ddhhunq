<?php

namespace App\Widgets;

use TCG\Voyager\Widgets\BaseDimmer;
use App\Models\Publicaciones\Publicacion;

class PublicacionesDimmer extends BaseDimmer
{
    public function run()
    {
        $count = Publicacion::whereNull('verified_at')->count();
        switch($count) {
            case 0:
                $string = __('widgets.publicaciones.title.none');
                $text =  __('widgets.publicaciones.text.none'); break;
            case 1:
                $string = __('widgets.publicaciones.title.one');
                $text =  __('widgets.publicaciones.text.one'); break;
            default:
                $string = __('widgets.publicaciones.title.many', ['count' => $count]);
                $text =  __('widgets.publicaciones.text.many', ['count' => $count]); break;
        }

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'fas fa-file-alt' . ($count ? ' notification' : ''),
            'title'  => $string,
            'text'   => $text,
            'button' => [
                'text' => __('widgets.publicaciones.btn_link'),
                'link' => route('voyager.publicaciones.index', ['key' => 'verified_at', 'filter' => 'null']),
            ],
            'image' => 'img/widgets/publicaciones_dimmer.jpg',
        ]));
    }
}
