
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//
import 'jquery';
import './bootstrap';
import 'select2';
import 'select2/dist/js/i18n/es.js';
import 'bootstrap-toggle/js/bootstrap-toggle.js';
import 'script-loader!gijgo';
import 'gijgo/js/messages/messages.es-es';

import Vue from 'vue';

/**
 * Now we will declare all components that have been created within the
 * application. The easiest way to do this is through the require function.
 */

 Vue.component('select2', require('./components/select2.vue').default);
 Vue.component('advanced-input', require('./components/advanced_input.vue').default);
 Vue.component('limited-textarea', require('./components/limited_textarea.vue').default);
 Vue.component('toggle', require('./components/toggle.vue').default);
 Vue.component('datepicker', require('./components/datepicker.vue').default);
 Vue.component('file-upload', require('./components/multifile_uploader.vue').default);
 Vue.component('buscador-selector-ubicacion', require('./components/buscador_selector_ubicacion.vue').default);
 Vue.component('buscador-selector-institucion', require('./components/buscador_selector_institucion.vue').default);
 Vue.component('buscador-selector-elemento', require('./components/buscador_selector_elemento.vue').default);
 Vue.component('institucion-carga-nueva', require('./components/institucion_carga_nueva.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import store from './stores/search_store.js';

const app = new Vue({
    el: '#app',
    store,
    created: () =>{
        // Set select2 global configuration
        $.fn.select2.defaults.set('language', 'es');
        $.fn.select2.defaults.set('amdBase', 'select2/');
        $.fn.select2.defaults.set('amdLanguageBase', 'select2/i18n/');
    }
});


