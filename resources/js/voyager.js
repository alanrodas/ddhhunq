require('select2/dist/js/i18n/es.js');

$(function() {
    $('select.select2').select2({width: '100%', language:'es'});

    $.fn.select2.defaults.set('language', 'es');
    $.fn.select2.defaults.set('amdBase', 'select2/');
    $.fn.select2.defaults.set('amdLanguageBase', 'select2/i18n/');
});