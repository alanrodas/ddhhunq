import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const search_store = new Vuex.Store({
    state: {
        selected_pais: 0,
        // selected_pais_id: 0,
        selected_region: 0,
        // selected_region_id: 0,
        selected_provincia: 0,
        // selected_provincia_id: 0,
        selected_institucion: 0,
        selected_institucion_id: 0,
        selected_tipo_de_elemento: 0,
    },
    mutations: {
        change_pais (state, value) {
            state.selected_pais = value;
        },
//        change_pais_id (state, value) {
//            state.selected_pais_id = value;
//        },
        change_region (state, value) {
            state.selected_region = value;
        },
//        change_region_id (state, value) {
//            state.selected_region_id = value;
//        },
        change_provincia (state, value) {
            state.selected_provincia = value;
        },
//        change_provincia_id (state, value) {
//            state.selected_provincia_id = value;
//        },
        change_institucion (state, value) {
            state.selected_institucion = value;
        },
        change_institucion_id (state, value) {
            state.selected_institucion_id = value;
        },
        change_tipo_de_elemento (state, value) {
            state.selected_tipo_de_elemento = value;
        },
    },
    getters: {
        pais: (state) => {
            return state.selected_pais;
        },
//        pais_id: (state) => {
//            return state.selected_pais_id;
//        },
        region (state) {
            return state.selected_region;
        },
//        region_id (state) {
//            return state.selected_region_id;
//        },
        provincia (state) {
            return state.selected_provincia;
        },
//        provincia_id (state) {
//            return state.selected_provincia_id;
//        },
        institucion (state) {
            return state.selected_institucion;
        },
        institucion_id (state) {
            return state.selected_institucion_id;
        },
        tipo_de_elemento (state) {
            return state.selected_tipo_de_elemento;
        },
    }
});

export default search_store;
