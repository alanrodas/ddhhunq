<?php

return [
    'sin_seleccionar'     => 'Ninguno seleccionado',
    'limite_caracteres' => [
        'quedan'    => 'Le quedan :chars caracteres disponibles.',
        'limite'    => 'No puede escribir más caracteres.',
        'excedido'  => 'Ha excedido el límite de caracteres disponibles. Borre :chars caracteres.',
    ],
    'errors' => [
        'required'  => 'Este campo es requerido. Completelo para continuar.',
        'max_exceeded'  => 'El campo no puede superar el límite de caracteres permitido.',
        'max_file_size'  => 'El tamaño máximo de archivo es de :max_upload_file_size MB. sus archivos no cumplen ese requisito. Vuelva a intentarlo.'
    ],
    'file_uploader' => [
        'boton' =>  'Agregar otro archivo',
        'nada_seleccionado' =>  'Todavía no se ha seleccionado un archivo',
        'archivo_seleccionado' =>  'Se ha seleccionado el archivo ":file"',
        'tooltip' => 'Puede subir un máximo de :max_upload_files, con un peso maximo de :max_upload_file_size MB cada uno',
        'error_max_filesize' => 'El archivo ":filename" excede el tamaño máximo de archivo permitido. El archivo se ha descartado.',
    ],
    'required_warn' => [
        'before' => 'Los campos marcados con ',
        'sign' => '*',
        'after' => ' son obligatorios.'
    ],
    'wizard_inicio' => [
        'titulo'          => 'Carga de información',
        'subtitulo'       => 'Desde aquí podrá contribuir a la página acercandonos información que usted posea sobre cursos, proyectos, publicaciones, etc.',
        'warning_text'    => 'Tenga en cuenta que la información que suministre será verificada previo a encontrarse disponible al público.',
        'danger_text'     => 'Solo cargue información referente a trabajos realizados sobre derechos humanos.',
        'option_select' => [
            'text'        => 'Seleccione lo que desea cargar',
            'curso'       => 'Curso, seminario, taller, cátedra, materia, actividad de extensión/investigación u otra actividades',
            'proyecto'    => 'Proyecto o programa de extensión / investigación o algún otro tipo de proyecto, protocolo, resolución, gestión u otros.',
            'publicacion' => 'Publicación, paper cientifico, libro, revista, nota periodistica u otras publicaciones',
        ]
    ],
    'wizard_basico' => [
        'curso' => [
            'titulo'                => 'Datos del curso, seminario, taller, cátedra, materia, actividad de extensión/investigación u otras actividades',
            'subtitulo'             => 'Le pediremos ahora algunos datos básicos previo a guardar la información.',
            'tipo'  => [
                'label'             => '¿Qué es lo que desea cargar?',
                'tooltip'           => 'Indique si se trata de un curso, un seminario, un taller una actividad de extensión/investigación u otro',
            ],
            'institucion' => [
                'label'             => '¿A que institución pertenece?',
                'tooltip'           => 'Ingrese el nombre de su universidad, instituto, u organización. Si no está en la lista, puede cargarla (vea más abajo para instrucciones)'
            ],
            'dependencia_institucional' => [
                'label'             => '¿A qué dependencia (área, facultad, instituto, departamento, carrera, etc.) de la institución pertenece?',
                'tooltip'           => 'Ingrese la facultad, departamento, carrera u algún otra area de dependencia.'
            ],
            'nombre' => [
                'label'             => '¿Cuál es el nombre del curso o actividad?',
                'tooltip'           => 'Ingrese el nombre de su curso, seminario, actividad de extensión/investigación, u otro',
                'placeholder'       => 'Por ej. Derechos Humanos en el Siglo XXI',
            ],
            'descripcion' => [
                'label'             => 'Agregue una breve descripción del elemento que está cargando',
                'tooltip'           => 'Ingrese una breve descripción del curso, seminario, actividad de extensión o investigación.',
                'placeholder'       => 'Por ej. Cátedra obligatoria que deben tomar todos los alumnos de abogacía de la universidad.',
            ],
            'contacto_nombre' => [
                'label'             => '¿Con quién debe contactarse un interesado?',
                'tooltip'           => 'Puede agregar más de un nombre si hubiera más de una persona de contacto.',
                'placeholder'       => 'Por ej. Juan Pérez',
            ],
            'contacto_telefono' => [
                'label'             => '¿A qué teléfono puede contactarse un interesado?',
                'tooltip'           => 'Agregar el código de país y de área puede ayudar a que personas del exterior o de otras provincias puedan contactarlo.',
                'placeholder'       => 'Por ej. (54) (11) 4321-9876',
            ],
            'contacto_email' => [
                'label'             => '¿A qué correo electrónico puede contactarse un interesado?',
                'tooltip'           => 'Puede agregar más de un correo si lo considera necesario.',
                'placeholder'       => 'Por ej. juanperez@institucion.edu.ar',
            ],
            'responsables_nombres' => [
                'label'             => '¿Quién o quienes serán los docentes o responsables de este curso o actividad?',
                'tooltip'           => 'Puede agregar más de un nombre si hubiera más un docente o director de la actividad.',
                'placeholder'       => 'Por ej. María Rodriguez',
            ],
            'responsables_detalles' => [
                'label'             => 'Agregue información sobre los docentes o responsables del curso o actividad.',
                'tooltip'           => 'Puede agregar una breve reseña sobre el docente o director o sobre cada uno de ellos si hubiera más de uno.',
                'placeholder'       => 'Por ej. María Rodriguez es licenciada en Ciencias de la Comunicación por la Universidad de Buenos Aires. Trabaja en investigación sobre derechos humanos desde el 2001.',
            ],
            'section_titles' => [
                'basico'        => 'Sobre el curso o actividad',
                'responsables'  => 'Sobre los docentes',
                'contacto'      => 'Datos de contacto',
            ]
        ],
        'proyecto' => [
            'titulo'                => 'Datos del proyecto o programa de extensión/investigación o algún otro tipo de proyecto, protocolo, resolución, gestión u otros.',
            'subtitulo'             => 'Le pediremos ahora algunos datos básicos previo a guardar la información.',
            'tipo'  => [
                'label'             => '¿Qué es lo que desea cargar?',
                'tooltip'           => 'Indique si se trata de un proyecto o programa de extensión, de investigación, un protocolo, una resolución, gestión u otro',
            ],
            'institucion' => [
                'label'             => '¿A que institución pertenece?',
                'tooltip'           => 'Ingrese el nombre de su universidad, instituto, u organización. Si no está en la lista, puede cargarla (vea más abajo para instrucciones)'
            ],
            'dependencia_institucional' => [
                'label'             => '¿A qué dependencia (área, facultad, instituto, departamento, carrera, etc.) de la institución pertenece?',
                'tooltip'           => 'Ingrese la facultad, departamento, carrera u algún otra area de dependencia.'
            ],
            'nombre' => [
                'label'             => '¿Cuál es el nombre del proyecto, programa u otro tipo de elemento?',
                'tooltip'           => 'Ingrese el nombre de su proyecto o programa de extensión/investigación u otro tipo de elemento',
                'placeholder'       => 'Por ej. Derechos Humanos en el Paraguay actuál',
            ],
            'descripcion' => [
                'label'             => 'Agregue una breve descripción del elemento que está cargando',
                'tooltip'           => 'Ingrese una breve descripción del proyecto o programa.',
                'placeholder'       => 'Por ej. Proyecto de investigación sobre los derechos humanos en la Republica del Paraguay.',
            ],
            'contacto_nombre' => [
                'label'             => '¿Con quién debe contactarse un interesado?',
                'tooltip'           => 'Puede agregar más de un nombre si hubiera más de una persona de contacto.',
                'placeholder'       => 'Por ej. Juan Pérez',
            ],
            'contacto_telefono' => [
                'label'             => '¿A qué teléfono puede contactarse un interesado?',
                'tooltip'           => 'Agregar el código de país y de área puede ayudar a que personas del exterior o de otras provincias puedan contactarlo.',
                'placeholder'       => 'Por ej. (54) (11) 4321-9876',
            ],
            'contacto_email' => [
                'label'             => '¿A qué correo electrónico puede contactarse un interesado?',
                'tooltip'           => 'Puede agregar más de un correo si lo considera necesario.',
                'placeholder'       => 'Por ej. juanperez@institucion.edu.ar',
            ],
            'responsables_nombres' => [
                'label'             => '¿Quién o quienes son los directores del proyecto?',
                'tooltip'           => 'Puede agregar más de un nombre si hubiera más una persona dirigiendo el proyecto.',
                'placeholder'       => 'Por ej. María Rodriguez',
            ],
            'responsables_detalles' => [
                'label'             => 'Agregue información sobre los directores del proyecto.',
                'tooltip'           => 'Puede agregar una breve reseña sobre el director o sobre cada uno de ellos si hubiera más de uno.',
                'placeholder'       => 'Por ej. María Rodriguez es licenciada en Ciencias de la Comunicación por la Universidad de Buenos Aires. Trabaja en investigación sobre derechos humanos desde el 2001.',
            ],
            'section_titles' => [
                'basico'        => 'Sobre el proyecto/programa/protocolo/resolución/gestión u otro.',
                'responsables'  => 'Sobre los directores',
                'contacto'      => 'Datos de contacto',
            ]
        ],
        'publicacion' => [
            'titulo'                => 'Datos de la publicación, paper científico, libro, revista, nota periodistica u otras publicaciones',
            'subtitulo'             => 'Le pediremos ahora algunos datos básicos previo a guardar la información.',
            'tipo'  => [
                'label'             => '¿Qué es lo que desea cargar?',
                'tooltip'           => 'Indique si se trata de paper, un libro, una revista, una nota periodistica u otra publicación',
            ],
            'institucion' => [
                'label'             => '¿A que institución pertenece?',
                'tooltip'           => 'Ingrese el nombre de su universidad, instituto, u organización. Si no está en la lista, puede cargarla (vea más abajo para instrucciones)'
            ],
            'dependencia_institucional' => [
                'label'             => '¿A qué dependencia (área, facultad, instituto, departamento, carrera, etc.) de la institución pertenece?',
                'tooltip'           => 'Ingrese la facultad, departamento, carrera u algún otra area de dependencia.'
            ],
            'nombre' => [
                'label'             => '¿Cuál es el nombre de la publicación?',
                'tooltip'           => 'Ingrese el nombre o título de su publicación',
                'placeholder'       => 'Por ej. La aplicación de los Derechos Humanos en Argentina durante la década de 1990',
            ],
            'descripcion' => [
                'label'             => 'Agregue una breve descripción de la publicación',
                'tooltip'           => 'Ingrese una breve descripción de la publicación.',
                'placeholder'       => 'Por ej. Paper de investigación sobre los derechos humanos durante la década neoliberal que llevó adelante Carlos Saúl Menem en la republica argentina.',
            ],
            'contacto_nombre' => [
                'label'             => '¿Con quién debe contactarse un interesado?',
                'tooltip'           => 'Puede agregar más de un nombre si hubiera más de una persona de contacto.',
                'placeholder'       => 'Por ej. Juan Pérez',
            ],
            'contacto_telefono' => [
                'label'             => '¿A qué teléfono puede contactarse un interesado?',
                'tooltip'           => 'Agregar el código de país y de área puede ayudar a que personas del exterior o de otras provincias puedan contactarlo.',
                'placeholder'       => 'Por ej. (54) (11) 4321-9876',
            ],
            'contacto_email' => [
                'label'             => '¿A qué correo electrónico puede contactarse un interesado?',
                'tooltip'           => 'Puede agregar más de un correo si lo considera necesario.',
                'placeholder'       => 'Por ej. juanperez@institucion.edu.ar',
            ],
            'responsables_nombres' => [
                'label'             => '¿Quién o quienes son los autores de la publicación',
                'tooltip'           => 'Puede agregar más de un nombre si hubiera más un autor.',
                'placeholder'       => 'Por ej. María Rodriguez',
            ],
            'responsables_detalles' => [
                'label'             => 'Agregue información sobre los autores',
                'tooltip'           => 'Puede agregar una breve reseña sobre el autor o sobre cada uno de ellos si hubiera más de uno.',
                'placeholder'       => 'Por ej. María Rodriguez es licenciada en Ciencias de la Comunicación por la Universidad de Buenos Aires. Trabaja en investigación sobre derechos humanos desde el 2001.',
            ],
            'section_titles' => [
                'basico'        => 'Sobre la publicación',
                'responsables'  => 'Sobre los autores',
                'contacto'      => 'Datos de contacto',
            ]
        ],
        'warning_sin_institucion'   =>  '¿No encuentra su institución o la dependencia? Puede cargarla usted mismo',
        'cargar_institucion'        =>  'dando clic aquí.',
        'paso_siguiente'            =>  'Ir al paso final',
        'paso_anterior'             =>  'Cancelar y volver a comenzar',
    ],
    'wizard_extra' => [
        'curso' => [
            'titulo'                    => 'Datos del curso, seminario, taller, cátedra, materia, actividad de extensión/investigación u otras actividades',
            'subtitulo'                 => 'Complete algunos datos adicionales para finalizar la carga.',

            'tipo_de_intensidad' => [
                'label'             => 'Intensidad',
                'tooltip'           => 'Indique cuál es la intensidad del curso o actividad (anual, cuatrimestral, etc.)',
            ],
            'tipo_de_modalidad' => [
                'label'             => 'Modalidad',
                'tooltip'           => 'Indique cuál es la modalidad del curso o actividad (virtual, presencial, semipresencial, etc.)',
            ],
            'tipo_de_regularidad' => [
                'label'             => 'Regularidad',
                'tooltip'           => 'Indique cuál es la regularidad del curso o actividad (regular, periódico, etc.)',
            ],
            'tipo_de_nivel' => [
                'label'             => 'Nivel',
                'tooltip'           => 'Indique cuál es el nivel del curso o actividad (grado, posgrado, etc.)',
            ],
            'tipo_de_curricularidad' => [
                'label'             => 'Curricularidad',
                'tooltip'           => 'Indique cuál es la curricularidad del curso o actividad (libre, curricular, extracurricular, etc.)',
            ],
            'arancelada' => [
                'label'             => '¿La actividad es arancelada?',
                'tooltip'           => 'Marque "si" si la actividad cobra matricula o arancel',
            ],
            'fecha_de_inicio' => [
                'label'             => 'Fecha de inicio del curso o actividad',
                'tooltip'           => 'Indique el día en que comienza la actividad (solo para actividades únicas, no regulares)',
            ],
            'fecha_de_fin' => [
                'label'             => 'Fecha de finalización del curso o actividad',
                'tooltip'           => 'Indique el día en que finaliza la actividad (solo para actividades únicas, no regulares)',
            ],

            'estrategia_metodologica' => [
                'label'             => 'Puede brindar detalles sobre la estrategia metodológica',
                'tooltip'           => 'Agregue detalles sobre la estrategia o abordaje metodológico de este curso o actividad',
                'placeholder'       => 'Por ej. Se utiliza un enfoque práctico para comprender las problemáticas de los habitantes de la ciudad en torno a las violaciones de derechos humanos que sufren.',
            ],
            'condiciones' => [
                'label'             => 'Puede brindar detalles sobre las condiciones de participación',
                'tooltip'           => 'Agregue detalles sobre las condiciones de participación de este curso o actividad',
                'placeholder'       => 'Por ej. Se debe poseer un título de grado para poder asistir a la actividad.'
            ],
            'otros' => [
                'label'             => 'Puede brindar otros detalles adicionales sobre el curso o actividad',
                'tooltip'           => 'Agregue otros detalles sobre el curso o actividad si lo considera necesario',
                'placeholder'       => 'Por ej. Se puede cursar sin ser parte de la universidad.',
            ],

            'tipo_de_articulacion'  => [
                'label'            => '¿Este curso o actividad se articula de algúna forma?',
                'tooltip'           => 'Indique si se este curso o actividad se articula de alguna forma con otras actividades o instituciones',
            ],
            'articulacion_detalles' => [
                'label'             => 'Un pequeño detalle sobre la articulación de la actividad',
                'tooltip'           => 'Agrege una descripción sobre la articulación arriba mencionada de corresponder',
                'placeholder'       => 'Por ej. Se encuadra en un trabajo conjunto entre la UNQ (Argentina) y la Udelar (Uruguay).',
            ],

            'section_titles' => [
                'articulacion'      => 'Sobre las articulaciones',
                'elemento'          => 'Sobre el curso o actividad',
                'otros'             => 'Otros datos de interes',
                'adjuntos'          => 'Archivos adjuntos',
            ]
        ],
        'proyecto' => [
            'titulo'                => 'Datos del proyecto o programa de extensión/investigación o algún otro tipo de proyecto',
            'subtitulo'             => 'Le pediremos ahora algunos datos básicos previo a guardar la información.',

            'destinatarios' => [
                'label'             => 'Breve descripción de los destinatarios del proyecto o programa',
                'tooltip'           => 'Ingrese a quienes va dirigido este proyecto o programa, o alguna información adicional que desee',
                'placeholder'       => 'Por ej. Se encuadra destinado a estudiantes de posgrado en ciencias sociales.',
            ],

            'tipo_de_articulacion'  => [
                'label'             => '¿El proyecto articula con otros actores o instituciones?',
                'tooltip'           => 'Indique si se este proyecto se articula de alguna forma con otras actores o instituciones',
            ],
            'articulacion_detalles' => [
                'label'             => 'Un pequeño detalle sobre la articulación de la actividad',
                'tooltip'           => 'Agrege una descripción sobre la articulación arriba mencionada de corresponder',
                'placeholder'       => 'Por ej. Se encuadra en un trabajo conjunto entre la UNQ (Argentina) y la Udelar (Uruguay).',
            ],

            'section_titles' => [
                'articulacion'      => 'Sobre las articulaciones',
                'elemento'          => 'Sobre el proyecto',
                'adjuntos'          => 'Archivos adjuntos',
            ]
        ],
        'publicacion' => [
            'titulo'                => 'Datos de la publicación, paper científico, libro, revista, nota periodistica u otras publicaciones',
            'subtitulo'             => 'Le pediremos ahora algunos datos básicos previo a guardar la información.',

            'editorial' => [
                'label'             => 'Editorial',
                'tooltip'           => 'Ingrese la editorial que ha publicado el trabajo',
                'placeholder'       => 'Por ej. Eudeba.',
            ],
            'anho' => [
                'label'             => 'Año',
                'tooltip'           => 'Ingrese el año de publicación',
                'placeholder'       => 'Por ej. 2007.',
            ],
            'ciudad' => [
                'label'             => 'Ciudad',
                'tooltip'           => 'Ingrese si corresponde la ciudad en donde se ha publicado el trabajo',
                'placeholder'       => 'Por ej. Buenos Aires.',
            ],
            'serie' => [
                'label'             => 'Serie',
                'tooltip'           => 'Ingrese la serie o colección a la que pertenece la publicación (deje en blanco si es una publicación independiente)',
                'placeholder'       => 'Por ej. Derechos Humanos en la dictadura cívico-militar.',
            ],
            'edicion' => [
                'label'             => 'Edicion',
                'tooltip'           => 'Ingrese la edición de la publicación (o deje en blanco si fuera la única edición)',
                'placeholder'       => 'Por ej. 10ª edición',
            ],
            'isbn' => [
                'label'             => 'ISBN',
                'tooltip'           => 'Ingrese de corresponder, el ISBN de la publicación',
                'placeholder'       => 'Por ej. 978-3161484100',
            ],

            'section_titles' => [
                'elemento'          => 'Sobre la publicaciación',
                'adjuntos'          => 'Archivos adjuntos',
            ]
        ],
        'paso_siguiente'            =>  'Finalizar',
        'paso_anterior'             =>  'Volver a cargar datos básicos',
    ],
    'wizard_institucion' => [
        'titulo'                => 'Cargar una nueva institución o dependencia institucional',
        'subtitulo'             => 'Por favor, cargue su institución o dependencia institucional para proseguir con la carga de datos.',
        'institucion' => [
            'label'             => 'Institución',
            'tooltip'           => 'Si su institución ya está en la lista, seleccionela, caso contrario seleccione "Mi institución no está en la lista"'
        ],
        'dependencia_institucional' => [
            'label'             => 'Dependencia institucional',
            'tooltip'           => 'Ingrese el nombre de la facultad, departamento, carrera u algún otra área de dependencia.'
        ],
        'pais' => [
            'label'             => 'País',
            'tooltip'           => 'Ingrese el país en el que está radicada su institución.'
        ],
        'region' => [
            'label'             => 'Región',
            'tooltip'           => 'Ingrese la región en el que está radicada su institución.'
        ],
        'provincia' => [
            'label'             => 'Provincia',
            'tooltip'           => 'Ingrese la provincia en el que está radicada su institución.'
        ],
        'nombre' => [
            'label'             => 'Nombre',
            'placeholder'       => 'Ej. Universidad Nacional de Gral. Belgrano',
            'tooltip'           => 'Ingrese el nombre de la institución.',
            'error_duplicate'   => 'Ya existe una institución con un nombre similar cargado. Mire el listado y busque ":nombre". Si está seguro que no es la misma institución, agregue un nombre más específico.'
        ],
        'direccion' => [
            'label'             => 'Dirección',
            'placeholder'       => 'Ej. Av. San Martín 1234, Gral. Belgrano',
            'tooltip'           => 'Ingrese la dirección de la institución.'
        ],
        'codigo_postal' => [
            'label'             => 'Código Postal',
            'placeholder'       => 'Ej. B1878',
            'tooltip'           => 'Ingrese el código postal de la institución.'
        ],
        'telefono' => [
            'label'             => 'Teléfono',
            'placeholder'       => 'Ej. (54) (11) 1234-5678',
            'tooltip'           => 'Ingrese el teléfono de la institución. Ingrese el código de país y área para poder comunicarnos.'
        ],
        'email' => [
            'label'             => 'Email',
            'placeholder'       => 'Ej. info@universidad.edu.ar',
            'tooltip'           => 'Ingrese el email de la institución.'
        ],
        'tipo_de_institucion' => [
            'label'             => 'Tipo de Institución',
            'tooltip'           => 'Ingrese si se trata de una universidad, un instituto, u otro tipo de organismo'
        ],
        'tipo_de_dependencia_institucional' => [
            'label'             => 'Tipo de Dependencia',
            'tooltip'           => 'Ingrese si se trata de una facultad, departamento, carrera, u otra área de la institución'
        ],
        'section_titles' => [
            'institucion'          => 'Sobre la institución',
            'dependencia_institucional' => 'Sobre la dependencia institucional',
        ],
        'institucion_no_en_lista'   => 'Mi institución no está en la lista',
        'paso_siguiente'            => 'Cargar institución y dependencia',
        'paso_anterior'             => 'Cancelar la carga de institución',
    ],
    'final' => [
        'titulo'                => 'Completado',
        'subtitulo'             => 'Muchas gracias por acercarnos la información. Estaremos verificandola y haciendola disponible muy pronto.',
        'ir_a_inicio'           => 'Volver al inicio',
        'cargar_nuevo'          => 'Cargar un nuevo elemento',
    ],
    'final_institucion' => [
        'titulo'                => 'Se ha cargado su institución',
        'subtitulo'             => 'La misma aparecerá ahora en los listados de selección de instituciones al cargar un elemento y no deberá volver a cargarla.',
        'subsubtitulo'          => 'Puede proseguir ahora cargando el elemento deseado.',
        'default_descripcion'   => 'Cargado de forma externa.',
        'option_select' => [
            'text'        => 'Seleccione lo que desea cargar',
            'curso'       => 'Curso, seminario, taller, cátedra, materia, actividad de extensión/investigación u otras actividades',
            'proyecto'    => 'Proyecto o programa de extensión/investigación o algún otro tipo de proyecto',
            'publicacion' => 'Publicación, paper cientifico, libro, revista, nota periodistica u otras publicaciones',
        ]
    ]
];
