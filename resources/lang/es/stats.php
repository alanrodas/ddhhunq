<?php

return [
    'categories' => [
        'cursos' => 'Cursos',
        'proyectos' => 'Proyectos',
        'publicaciones' => 'Publicaciones'
    ],
    'filters' => [
        'categoria' => 'Por Categoría',
        'paises' => ['nombre' => 'Por País'],
        'regiones' => ['nombre' => 'Por Región'],
        'provincias' => ['nombre' => 'Por Provincia'],
        'instituciones' => ['nombre' => 'Por Institución'],
        'dependencias_institucionales' => ['nombre' => 'Por Dependencia Institucional'],
        'tipos_de_cursos' => ['nombre' => 'Por Tipo de Curso'],
        'tipos_de_proyectos' => ['nombre' => 'Por Tipo de Proyecto'],
        'tipos_de_publicaciones' => ['nombre' => 'Por Tipo de Publicación'],
        'tipos_de_curricularidades' => ['nombre' => 'Por Tipo de Curricularidad'],
        'tipos_de_intensidades' => ['nombre' => 'Por Tipo de Intensidad'],
        'tipos_de_modalidades' => ['nombre' => 'Por Tipo de Modalidad'],
        'tipos_de_regularidades' => ['nombre' => 'Por Tipo de Regularidad'],
        'tipos_de_niveles' => ['nombre' => 'Por Tipo de Nivel'],
        'tipos_de_articulaciones' => ['nombre' => 'Por Tipo de Articulación']
    ],
    'graficos' => [
        'pie' => 'Torta',
        'doughnut' => 'Rosquilla',
        'bar' => 'Barras en vertical',
        'bar_horizontal' => 'Barras en horizontal'
    ],
    'ordenes' => [
        'nombre_asc' => 'Por nombre ascendente',
        'nombre_desc' => 'Por nombre descendente',
        'total_asc' => 'Por total ascendente',
        'total_desc' => 'Por total descendente'
    ],
    'title' => 'Estadisticas',
    'buttons' => [
        'by_pais' => 'Ver por país',
        'by_institucion' => 'Ver por institución',
        'by_categoria' => 'Ver por categoría',
    ],
    'table' => [
        'nombre' => 'Nombre',
        'cursos' => 'Cursos',
        'proyectos' => 'Proyectos',
        'publicaciones' => 'Publicaciones',
        'total' => 'Total',
        'percentage' => 'Porcentaje',
        'no_data' => 'No hay datos para mostrar. Realice una nueva búsqueda.'
    ],
    'search' => [
        'title' => 'Realizar una búsqueda avanzada',
        'data' => [
            'subtitle' => 'Criterios para el filtrado de datos',
            'description' => 'Determine que datos desea mostrar en el gráfico final. Deje en blanco aquellos criterios de filtro no relevantes.'
        ],
        'graph' => [
            'subtitle' => 'Criterios para el gráfico',
            'description' => 'Determine el tipo de gráfico y la forma en la que se agrupará el mismo.'
        ],
        'buttons' => [
            'generate' => 'Generar gráfico',
            'clean' => 'Limpiar'
        ],
        'fields' => [
            'pais' => 'País',
            'region' => 'Región',
            'provincia' => 'Provincia',
            'institucion' => 'Institución',
            'dependencia_institucional' => 'Dependencia Institucional',
            'categoria' => 'Categoría',
            'tipo_de_categoria' => 'Tipo de categoría',
            'tipo_de_grafico' => 'Tipo de gráfico',
            'agrupamiento' => 'Agrupamiento',
            'orden' => 'Orden en el que se muestran los datos',
            'mostrar_etiquetas' => 'Mostrar etiquetas sobre el gráfico',
            'ver_tabla' => 'Mostrar tabla con los datos',
        ],
        'option' => [
            'on' => 'Si',
            'off' => 'No',
            'none_selected' => 'Ninguno seleccionado'
        ]
    ],
];
