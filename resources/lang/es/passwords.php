<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben tener al menos seis caracteres de largo y coincidir con la confirmación.',
    'reset' => 'Se ha restablecido tu contraseña.',
    'sent' => 'Te hemos enviado el enlace de restablecimiento de contraseña por correo electrónico.',
    'token' => 'El enlace de restablecimiento de contraseña es inválido.',
    'user' => "No podemos encontrar un usuario con ese correo electrónico.",

];
