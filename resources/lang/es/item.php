<?php

return [
    'volver_al_listado'             => 'Volver al listado',
    'labels' => [
        'curso' => [
            'fecha_de_inicio'       => 'A realizarse desde el',
            'fecha_de_fin'          => 'hasta el',
            'no_arancelado'         => 'Esta actividad no cobra arancel',
            'arancelado'            => 'Esta actividad cobra arancel o matrícula',
            'intensidad'            => 'Es una actividad:',
            'regularidad'           => 'Se cursa de forma:',
            'nivel'                 => 'Pensado para estudiantes de:',
            'curricularidad'        => 'Es una cátedra o materia:',
            'responsables_nombres'  => 'Docentes',
            'responsables_expand'   => 'Leer más',
            'responsables_detalles' => 'Sobre los docentes',
            'tipo_de_articulacion'  => 'Se articula de forma:',
            'articulacion_expand'   => 'Leer más',
            'articulacion_detalles' => 'Sobre la articulación',
            'estrategia_metodologica' => 'Leer sobre la estrategia metodológica',
            'estrategia_metodologica_detalles' => 'Estrategia metodológica',
            'condiciones'           => 'Ver las condiciones',
            'condiciones_detalles'  => 'Condiciones',
            'otros'                 => 'Ver otros detalles relevantes',
            'otros_detalles'        => 'Otros detalles relevantes',
            'contacto_nombre'       => 'Contacto',
            'mas_informacion'       => 'Más información',
            'archivo_adjunto'       => 'Descargar el archivo adjunto ":nombre"'
        ],
        'proyecto' =>   [
            'responsables_nombres'  => 'Directores',
            'responsables_expand'   => 'Leer más',
            'responsables_detalles' => 'Sobre los directores',
            'tipo_de_articulacion'  => 'Se articula de forma:',
            'articulacion_expand'   => 'Leer más',
            'articulacion_detalles' => 'Sobre la articulación',
            'destinatarios'         => 'Leer sobre los detinatarios',
            'destinatarios_detalles'=> 'Sobre los destinatarios',
            'contacto_nombre'       => 'Contacto',
            'mas_informacion'       => 'Más información',
            'archivo_adjunto'       => 'Descargar el archivo adjunto ":nombre"'
        ],
        'publicacion' => [
            'editorial'             => 'Editorial:',
            'anho'                  => 'Año:',
            'ciudad'                => 'Ciudad:',
            'serie'                 => 'Serie:',
            'edicion'               => 'Edición:',
            'isbn'                  => 'ISBN:',
            'responsables_nombres'  => 'Autores:',
            'responsables_expand'   => 'Leer más',
            'responsables_detalles' => 'Sobre los autores',
            'contacto_nombre'       => 'Contacto',
            'mas_informacion'       => 'Más información',
            'archivo_adjunto'       => 'Descargar el archivo adjunto ":nombre"'
        ]
    ]
];
