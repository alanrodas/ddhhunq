<?php

return [
    'simple' => [
        'titulo'                => 'Búsqueda simple',
        'nota'                  => 'Búsqueda de todos los elementos que coincidan con el texto ingresado',
        'placeholders' => [
            'query'             => 'Buscar todos los elementos que coincidan con...'
        ],
        'submit'                => 'Buscar',
        'links' => [
            'avanzada'          => ':link_start Búsqueda Avanzada :link_end',
            'todos'             => ':link_start Ver todos los datos de nuestra base :link_end',
        ]
    ],
    'avanzada' => [
        'titulo'                => 'Búsqueda avanzada',
        'nota'                  => 'Busque todo elemento que coincida con los parámetros de búsqueda. Puede completar uno o más campos específicos para realizar la búsqueda. Los campos no especificados serán ignorados.',
        'labels' => [
            'pais'              => 'País',
            'region'            => 'Región',
            'provincia'         => 'Provincia',
            'institucion'       => 'Institución',
            'tipo_de_elemento'  => 'Tipo de elemento',
            'tipo_de_curso'     => 'Tipo de curso',
            'tipo_de_proyecto'  => 'Tipo de proyecto',
            'tipo_de_publicacion' => 'Tipo de publicación',
            'tipo_de_modalidad' => 'Modalidad',
            'tipo_de_nivel'     => 'Nivel',
            'keywords'          => 'Otras palabras de búsqueda',
        ],
        'placeholders' => [
            'keywords'          => 'Ej. Pueblos originarios',
        ],
        'submit'                => 'Buscar',
        'links' => [
            'simple'            => ':link_start Búsqueda Simple :link_end',
            'todos'             => ':link_start Ver todos los datos de nuestra base :link_end',
        ]
    ],
    'dependencia_institucional' => 'Dependiente de :dependencia',
    'ver_mas_informacion'       => 'Ver más información',
    'descargar_adjuntos'        => 'Descargar archivos adjuntos',
    'sin_resultados'            => 'No se han encontrado resultados para su búsqueda.<br>Intentelo nuevamente cambiando los parámetros de búsqueda.',
    'sin_seleccionar'           => 'Ninguno seleccionado',
    'tipo_de_elemento' => [
        'curso' => [
            'icon'              => 'fas fa-graduation-cap',
            'display_name'      => 'Curso',
            'display_plural_name' => 'Cursos',
        ],
        'proyecto' => [
            'icon'              =>  'fas fa-briefcase',
            'display_name'      => 'Proyecto',
            'display_plural_name' => 'Proyectos',
        ],
        'publicacion' => [
            'icon'              =>  'fas fa-file-alt',
            'display_name'      => 'Publicación',
            'display_plural_name' => 'Publicaciones'
        ],
    ],
];
