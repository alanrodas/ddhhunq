<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'El campo ":attribute" debe ser aceptado.',
    'active_url'           => 'El campo ":attribute" no es una URL válida.',
    'after'                => 'El campo ":attribute" debe contener una fecha posterior a :date.',
    'after_or_equal'       => 'El campo ":attribute" debe contener una fecha posterios o igual a :date.',
    'alpha'                => 'El campo ":attribute" solo puede contener letras.',
    'alpha_dash'           => 'El campo ":attribute" solo puede contener letras, números, guiones y guiones bajos.',
    'alpha_num'            => 'El campo ":attribute" solo puede contener letras y número.',
    'array'                => 'El campo ":attribute" debe ser un arreglo.',
    'before'               => 'El campo ":attribute" debe contener una fecha previo a :date.',
    'before_or_equal'      => 'El campo ":attribute" debe contener una fecha previa o igual a :date.',
    'between'              => [
        'numeric' => 'El campo ":attribute" debe estar entre :min y :max.',
        'file'    => 'El archivo del campo ":attribute" debe pesar entre :min y :max kilobytes.',
        'string'  => 'El campo ":attribute" debe contener entre :min y :max caracteres.',
        'array'   => 'El campo ":attribute" debe contener entre :min and :max elementos.',
    ],
    'boolean'              => 'El campo ":attribute" debe ser verdadero o falso.',
    'confirmed'            => 'El campo ":attribute" no coincide con la confirmación.',
    'date'                 => 'El campo ":attribute" no es una fecha válida.',
    'date_format'          => 'El campo ":attribute" no coincide con el formato de fecha :format.',
    'different'            => 'El campo ":attribute" y :other deben ser diferentes.',
    'digits'               => 'El campo ":attribute" debe contener :digits digitos.',
    'digits_between'       => 'El campo ":attribute" debe tener entre :min y :max digitos.',
    'dimensions'           => 'El campo ":attribute" tiene dimensiones de imágenes inválidas.',
    'distinct'             => 'El campo ":attribute" contiene un dato duplicado.',
    'email'                => 'El campo ":attribute" debe ser una dirección de correo electrónico válido.',
    'exists'               => 'El campo seleccionado en ":attribute" es inválido.',
    'file'                 => 'El campo ":attribute" debe ser un archivo.',
    'filled'               => 'El campo ":attribute" debe contener un valor.',
    'gt'                   => [
        'numeric' => 'El campo ":attribute" debe ser más grande que :value.',
        'file'    => 'El archivo del campo ":attribute" debe pesar más de :value kilobytes.',
        'string'  => 'El campo ":attribute" debe contener más de :value caracteres.',
        'array'   => 'El campo ":attribute" debe contener más de :value elementos.',
    ],
    'gte'                  => [
        'numeric' => 'El campo ":attribute" debe ser más grande o igual que :value.',
        'file'    => 'El archivo del campo ":attribute" debe pesar :value kilobytes o más.',
        'string'  => 'El campo ":attribute" debe contener :value caracteres o más.',
        'array'   => 'El campo ":attribute" debe contener :value elementos o más.',
    ],
    'image'                => 'El campo ":attribute" debe ser una imágen.',
    'in'                   => 'El elemento seleccionado en ":attribute" es inválido.',
    'in_array'             => 'El campo ":attribute" no se encuentra en :other.',
    'integer'              => 'El campo ":attribute" debe ser un número entero.',
    'ip'                   => 'El campo ":attribute" debe ser una dirección IP válida.',
    'ipv4'                 => 'El campo ":attribute" debe ser una dirección IPv4 válida.',
    'ipv6'                 => 'El campo ":attribute" debe ser una dirección IPv6 válida.',
    'json'                 => 'El campo ":attribute" debe ser una cadena JSON válida.',
    'lt'                   => [
        'numeric' => 'El campo ":attribute" debe ser más chico que :value.',
        'file'    => 'El archivo del campo ":attribute" debe pesar menos de :value kilobytes.',
        'string'  => 'El campo ":attribute" debe contener menos de :value caracteres.',
        'array'   => 'El campo ":attribute" debe contener menos de :value elementos.',
    ],
    'lte'                  => [
        'numeric' => 'El campo ":attribute" debe ser más chico o igual que :value.',
        'file'    => 'El archivo del campo ":attribute" debe pesar :value kilobytes o menos.',
        'string'  => 'El campo ":attribute" debe contener :value caracteres o menos.',
        'array'   => 'El campo ":attribute" debe contener :value elementos o menos.',
    ],
    'max'                  => [
        'numeric' => 'El campo ":attribute" no puede superar :max.',
        'file'    => 'El campo ":attribute" no puede superar los :max kilobytes.',
        'string'  => 'El campo ":attribute" no puede superar los :max caracteres.',
        'array'   => 'El campo ":attribute" no puede superar los :max elementos.',
    ],
    'mimes'                => 'El campo ":attribute" debe contener un archivo de tipo: :values.',
    'mimetypes'            => 'El campo ":attribute" debe contener un archivo de tipo: :values.',
    'min'                  => [
        'numeric' => 'El campo ":attribute" debe ser al menos :min.',
        'file'    => 'El campo ":attribute" debe pesar al menos :min kilobytes.',
        'string'  => 'El campo ":attribute" debe contener al menos :min caracteres.',
        'array'   => 'El campo ":attribute" debe contener al menos :min elementos.',
    ],
    'not_in'               => 'El elemento seleccionado en ":attribute" es inválido.',
    'not_regex'            => 'El formato del campo ":attribute" es inválido.',
    'numeric'              => 'El campo ":attribute" debe ser un número.',
    'present'              => 'El campo ":attribute" no puede estar en blanco.',
    'regex'                => 'El formato del campo ":attribute" es inválido.',
    'required'             => 'El campo ":attribute" es requerido.',
    'required_if'          => 'El campo ":attribute" es requerido cuando :other es :value.',
    'required_unless'      => 'El campo ":attribute" es requerido a menos que :other se encuentre en :values.',
    'required_with'        => 'El campo ":attribute" es requerido cuando :values se ha completado.',
    'required_with_all'    => 'El campo ":attribute" es requerido cuando:values es ha completado.',
    'required_without'     => 'El campo ":attribute" es requerido cuando :values no se ha seleccionado.',
    'required_without_all' => 'El campo ":attribute" es requerido cuando ninguno de :values se ha seleccionado.',
    'same'                 => 'El campo ":attribute" y :other deben coincidir.',
    'size'                 => [
        'numeric' => 'El campo ":attribute" debe tener un tamaño de :size.',
        'file'    => 'El campo ":attribute" debe tener un tamaño de :size kilobytes.',
        'string'  => 'El campo ":attribute" debe tener un largo de :size caracteres.',
        'array'   => 'El campo ":attribute" debe contener :size elementos.',
    ],
    'string'               => 'El campo ":attribute" debe ser texto.',
    'timezone'             => 'El campo ":attribute" debe ser una zona horaria válida.',
    'unique'               => 'El campo ":attribute" ya ha sido elegido.',
    'uploaded'             => 'El archivo en el campo ":attribute" ha fallado en subirse.',
    'url'                  => 'El campo ":attribute" tiene un formato inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
