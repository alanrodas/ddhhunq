<?php

return [
    'cursos' => [
        'title' => [
            'none'      => 'No tiene cursos pendientes de validación',
            'one'       => 'Tiene un curso pendiente de validación',
            'many'      => 'Tiene :count cursos pendientes de validación',
        ],
        'text' => [
            'none'      => 'No hay ningún nuevo curso cargado por la comunidad en este momento. No hay tareas para realizar.',
            'one'       => 'Hay un nuevo curso cargado por la comunidad, apruebelo para que el mismo aparezca en los resultados de búsqueda.',
            'many'      => 'Hay :count nuevos cursos cargado por la comunidad, apruebelos para que estos aparezcan en los resultados de búsqueda.',
        ],
        'btn_link'      => 'Ver cursos no validados'
    ],
    'proyectos' => [
        'title' => [
            'none'      => 'No tiene proyectos pendientes de validación',
            'one'       => 'Tiene un proyecto pendiente de validación',
            'many'      => 'Tiene :count proyectos pendientes de validación',
        ],
        'text' => [
            'none'      => 'No hay ningún nuevo proyecto cargado por la comunidad en este momento. No hay tareas para realizar.',
            'one'       => 'Hay un nuevo proyecto cargado por la comunidad, apruebelo para que el mismo aparezca en los resultados de búsqueda.',
            'many'      => 'Hay :count nuevos proyectos cargado por la comunidad, apruebelos para que estos aparezcan en los resultados de búsqueda.',
        ],
        'btn_link'      => 'Ver proyectos no validados'
    ],
    'publicaciones' => [
        'title' => [
            'none'      => 'No tiene publicaciones pendientes de validación',
            'one'       => 'Tiene una publicación pendiente de validación',
            'many'      => 'Tiene :count publicaciones pendientes de validación',
        ],
        'text' => [
            'none'      => 'No hay ningúna nueva publicación cargada por la comunidad en este momento. No hay tareas para realizar.',
            'one'       => 'Hay una nueva publicación cargada por la comunidad, apruebela para que la misma aparezca en los resultados de búsqueda.',
            'many'      => 'Hay :count nuevas publicaciones cargada por la comunidad, apruebelas para que aparezcan en los resultados de búsqueda.',
        ],
        'btn_link'      => 'Ver publicaciones no validadas'
    ]
];
