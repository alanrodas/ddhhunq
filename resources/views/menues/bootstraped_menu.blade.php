<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
        @foreach($items as $menu_item)
            <li class="nav-item {{(url($menu_item->link()) == url()->current()) ? 'active' : ''}}">
                <a class="nav-link" href="{{ $menu_item->url }}">{{ $menu_item->title }}
                    @if (url($menu_item->link()) == url()->current())
                        <span class="sr-only">(página actual)</span>
                    @endif
                </a>
            </li>
        @endforeach
    </ul>
  </div>
</nav>
