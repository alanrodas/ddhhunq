<footer class="app-footer">
    <div class="site-footer-right">
        Realizado por
        @if ( setting('site.author') )
            @php $author = setting('site.author'); @endphp
        @else
            @php $author = config('app.author'); @endphp
        @endif
        @if ( setting('site.author_email') )
            @php $author_email = setting('site.author_email'); @endphp
        @else
            @php $author_email = config('app.author_email'); @endphp
        @endif
        <a href="mailto:{{ $author_email }}">{{ $author }}</a>
        -
        Versión: {{ config('app.version') }}
    </div>
</footer>