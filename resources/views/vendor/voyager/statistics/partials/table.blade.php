@if (
    isset($grafico_ver_tabla) && $grafico_ver_tabla === 'si' &&
    isset($results) && count($results) > 0
)
@php
$totalElements = 0;
for ($i = 0; $i < count($results); $i++) {
    $totalElements += $results[$i]->total;
}
@endphp
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-bordered">
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-hover">
                        <thead>
                            <tr>
                                @foreach (get_object_vars($results[0]) as $key => $val)
                                    @if (!old('categoria') || (!!old('categoria') && !in_array($key, array_map(function($e) {return $e->id;}, $categorias))))
                                        <th><a href={{$getSortUrl($key)}}>{{ __('stats.table.'.$key) }}</a></th>
                                    @endif
                                @endforeach
                                <th><a href={{$getSortUrl($key)}}>{{__('stats.table.percentage')}}</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($results as $resultItem)
                                <tr>
                                    @foreach (get_object_vars($resultItem) as $key => $val)
                                        @if (!old('categoria') || (!!old('categoria') && !in_array($key, array_map(function($e) {return $e->id;}, $categorias))))
                                            <td>{{$val}}</td>
                                        @endif
                                    @endforeach
                                    <td>{{round($resultItem->total*100 / $totalElements)}}%</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endif