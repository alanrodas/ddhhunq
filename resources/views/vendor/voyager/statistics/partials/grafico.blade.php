<div class="row chart-container">
    @if (isset($graphDescriptionTexts))
        <div class="col-md-12">
            @foreach ($graphDescriptionTexts as $graphDescriptionText)
                <h4>{{$graphDescriptionText}}</h4>
            @endforeach
        </div>
    @endif
    <div class="col-md-12">
        <div class="panel panel-bordered">
            <div class="panel-body">
                @if (isset($ver_grafico) && $ver_grafico && isset($results) && count($results) > 0)
                    <canvas id="chart"></canvas>
                @else
                    <div class="text-center">
                        <h5>{{ __('stats.table.no_data') }}</h5>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>