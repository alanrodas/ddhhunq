<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
<script src="{{ asset('js/palette.js') }}"></script>

@if (isset($ver_grafico) && $ver_grafico && isset($results) && count($results) > 0)
    <script>
    document.addEventListener("DOMContentLoaded", function(event) {
        const values = {!! json_encode($results) !!};
        const total = values.reduce((sum, e) => sum + parseInt(e.total), 0);
        const fullPalette = palette('qualitative', values.length).map(hex => '#'+hex);

        let graphType = '{{ $grafico_tipo }}';
        let datasets = [];
        let labels = values.map(e => e.nombre);
        let options = {
            tooltips: {
                enabled: false
            },
            responsive: true
        };

        if (graphType == 'pie' || graphType == 'doughnut') {
            datasets = [
                {
                    backgroundColor: fullPalette,
                    borderColor: fullPalette,
                    data: values,
                    hoverOffset: 20,
                }
            ]
            options['parsing'] = {
                key: 'total',
                xAxisKey: 'nombre',
                yAxisKey: 'total'
            }
        } else {
            for (let i = 0; i < values.length; i++) {
                datasets.push({
                    label: values[i]['nombre'],
                    backgroundColor: [fullPalette[i]],
                    borderColor: [fullPalette[i]],
                    data: [values[i]],
                    hoverOffset: 20,
                    stack: 'bar'
                });
            }
            if (graphType == 'bar_horizontal') {
                options['indexAxis'] = 'y';
                options['parsing'] = {
                    yAxisKey: 'nombre',
                    xAxisKey: 'total'
                }
                graphType = 'bar';
            } else {
                options['parsing'] = {
                    xAxisKey: 'nombre',
                    yAxisKey: 'total'
                }
            }
        }

        @if (isset($grafico_mostrar_etiquetas) && $grafico_mostrar_etiquetas === 'si')

        options['plugins'] = {
            datalabels: {
                labels: {
                    name: {
                        align: 'top',
                        font: {size: values.length > 5 ? 12 : 18},
                        color: 'black',
                        formatter: function(value, ctx) {
                            return ctx.chart.data.labels[ctx.dataIndex];
                        }
                    },
                    percentage: {
                        align: 'bottom',
                        borderColor: 'black',
                        borderWidth: values.length > 5 ? 1 : 2,
                        borderRadius: 4,
                        color: 'black',
                        font: {size: values.length > 5 ? 10 : 16},
                        formatter: (value, ctx) => {
                            return (value.total*100 / total).toFixed(0)+"%";
                        }
                    }
                }
            }
        };
        Chart.register({datalabels: ChartDataLabels});
        @endif

        const myChart = new Chart(
            document.getElementById('chart'),
            {
                type: graphType,
                options: options,
                data: {
                    labels: labels,
                    datasets: datasets
                },
            }
        );
    });
    </script>
@endif