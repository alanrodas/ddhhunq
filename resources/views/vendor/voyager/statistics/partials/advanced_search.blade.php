<div class="row no-print">
    <div class="col-md-12">
        <h3>{{ __('stats.search.title') }}</h3>
        <div class="panel panel-bordered">
            <div class="panel-body">
                <form method="GET" class="form-search" action="{{ route('voyager.stats.view') }}">

                    <div class="row">
                        <h4>{{ __('stats.search.data.subtitle') }}</h4>
                        <p>
                            {{ __('stats.search.data.description') }}
                        </p>

                        <div class="form-group  col-md-4">
                            <label for="pais">{{ __('stats.search.fields.pais') }}</label>
                            <select class="form-control select2" name="pais">
                                <option class="no-val" value="0">{{ __('stats.search.option.none_selected') }}</option>
                                @foreach($paises as $pais)
                                    <option value="{{$pais->id}}"
                                        @if ($pais->id == old('pais')) selected="selected" @endif
                                    >
                                        {{$pais->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group  col-md-4">
                            <label for="region">{{ __('stats.search.fields.region') }}</label>
                            <select class="form-control select2" name="region">
                                <option class="no-val" value="0">{{ __('stats.search.option.none_selected') }}</option>
                                @foreach($regiones as $region)
                                    <option value="{{$region->id}}" data-pais="{{$region->pais_id}}"
                                        @if ($region->id == old('region')) selected="selected" @endif
                                    >
                                        {{$region->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group  col-md-4">
                            <label for="provincia">{{ __('stats.search.fields.provincia') }}</label>
                            <select class="form-control select2" name="provincia">
                                <option class="no-val" value="0">{{ __('stats.search.option.none_selected') }}</option>
                                @foreach($provincias as $provincia)
                                    <option value="{{$provincia->id}}" data-region="{{$provincia->region_id}}"
                                        @if ($provincia->id == old('provincia')) selected="selected" @endif
                                    >
                                        {{$provincia->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group  col-md-6">
                            <label for="institucion">{{ __('stats.search.fields.institucion') }}</label>
                            <select class="form-control select2" name="institucion">
                                <option class="no-val" value="0">{{ __('stats.search.option.none_selected') }}</option>
                                @foreach($instituciones as $institucion)
                                    <option value="{{$institucion->id}}" data-pais="{{$institucion->pais_id}}"
                                        data-region="{{$institucion->region_id}}" data-provincia="{{$institucion->provincia_id}}"
                                        @if ($institucion->id == old('institucion')) selected="selected" @endif
                                    >
                                        {{$institucion->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group  col-md-6">
                            <label for="dependencia_institucional">{{ __('stats.search.fields.dependencia_institucional') }}</label>
                            <select class="form-control select2" name="dependencia_institucional">
                                <option class="no-val" value="0">{{ __('stats.search.option.none_selected') }}</option>
                                @foreach($dependencias_institucionales as $dependencia_institucional)
                                    <option value="{{$dependencia_institucional->id}}" data-institucion="{{$dependencia_institucional->institucion_id}}"
                                        @if ($dependencia_institucional->id == old('dependencia_institucional')) selected="selected" @endif
                                    >
                                        {{$dependencia_institucional->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group  col-md-6">
                            <label for="categoria">{{ __('stats.search.fields.categoria') }}</label>
                            <select class="form-control select2" name="categoria">
                                <option class="no-val" value="all">{{ __('stats.search.option.none_selected') }}</option>
                                @foreach($categorias as $categoria)
                                    <option value="{{$categoria->id}}"
                                        @if ($categoria->id == old('categoria')) selected="selected" @endif
                                    >
                                        {{$categoria->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group  col-md-6">
                            <label for="tipo_de_categoria">{{ __('stats.search.fields.tipo_de_categoria') }}</label>
                            <select class="form-control select2" name="tipo_de_categoria">
                                <option class="no-val" value="all">{{ __('stats.search.option.none_selected') }}</option>
                                @foreach($tipos_de_categoria as $tipo_de_categoria)
                                    <option value="{{$tipo_de_categoria->id}}" data-categoria="{{$tipo_de_categoria->categoria}}"
                                        @if ($tipo_de_categoria->id == old('tipo_de_categoria')) selected="selected" @endif
                                    >
                                        {{$tipo_de_categoria->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <h4>{{ __('stats.search.graph.subtitle') }}</h4>
                        <p>
                            {{ __('stats.search.graph.description') }}
                        </p>

                        <div class="form-group  col-md-6">
                            <label for="tipo_de_grafico">{{ __('stats.search.fields.tipo_de_grafico') }}</label>
                            <select class="form-control select2" name="tipo_de_grafico">
                                @foreach($tipos_de_graficos as $tipo_de_grafico)
                                    <option value="{{$tipo_de_grafico->id}}"
                                        @if ($tipo_de_grafico->id == old('tipo_de_grafico')) selected="selected" @endif
                                    >
                                        {{$tipo_de_grafico->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group  col-md-6">
                            <label for="agrupamiento">{{ __('stats.search.fields.agrupamiento') }}</label>
                            <select class="form-control select2" name="agrupamiento">
                                @foreach($agrupamientos as $agrupamiento)
                                    <option value="{{$agrupamiento->id}}" data-categoria="{{$agrupamiento->categoria_id}}"
                                        @if ($agrupamiento->id == old('agrupamiento')) data-previouslyselected="selected" @endif
                                    >
                                        {{$agrupamiento->nombre}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group  col-md-6">
                            <label for="orden">{{ __('stats.search.fields.orden') }}</label>
                            <select class="form-control select2 orden-select" name="orden">
                                @foreach($ordenes as $orden)
                                    <option value="{{$orden->id}}"
                                        @if (old('order_by') !== null && old('sort_order') !== null
                                            && $orden->id == old('order_by').'_'.old('sort_order'))
                                            selected="selected"
                                        @elseif (old('order_by') !== null && old('sort_order') === null
                                            && $orden->id == old('order_by').'_asc')
                                            selected="selected"
                                        @endif
                                    >
                                        {{$orden->nombre}}
                                    </option>
                                @endforeach
                            </select>
                            <input type="hidden" name="order_by" value="nombre"></input>
                            <input type="hidden" name="sort_order" value="asc"></input>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="mostrar_etiquetas">{{ __('stats.search.fields.mostrar_etiquetas') }}</label>
                            <br>
                            <input type="checkbox" name="mostrar_etiquetas" class="toggleswitch"
                                data-on="{{ __('stats.search.option.on') }}" data-off="{{ __('stats.search.option.off') }}"
                                @if (!isset($grafico_mostrar_etiquetas) || $grafico_mostrar_etiquetas === 'si' ) checked="checked" @endif
                            >
                        </div>

                        <div class="form-group col-md-3">
                            <label for="ver_tabla">{{ __('stats.search.fields.ver_tabla') }}</label>
                            <br>
                            <input type="checkbox" name="ver_tabla" class="toggleswitch"
                                data-on="{{ __('stats.search.option.on') }}" data-off="{{ __('stats.search.option.off') }}"
                                @if (!isset($grafico_ver_tabla) || $grafico_ver_tabla === 'si' ) checked="checked" @endif
                            >
                        </div>

                        <div class="form-group  col-md-12 text-center">
                            <input type="submit" value="{{ __('stats.search.buttons.generate') }}" class="btn btn-success">
                            <a class="btn btn-danger" href="{{ route('voyager.stats.view')}}">{{ __('stats.search.buttons.clean') }}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.toggleswitch').bootstrapToggle();
    });

    function registerUpdaterFor(selectorName, dataFilterColumns, options = {}) {
        optionsDefault = {
            showAllWhenNoSelection: false,
            emptyFilter: '0',
            filterByEmpty: false
        }
        options = Object.assign({}, optionsDefault, options);

        if (typeof dataFilterColumns === 'string') {
            dataFilterColumns = [dataFilterColumns];
        }
        document.addEventListener("DOMContentLoaded", function(event) {
            let selectName = 'select[name='+selectorName+']';

            $(selectName).data('options', $(selectName + ' option').detach());

            function setOptions(filterColumns) {
                let $selection = $(selectName).data('options');
                $(selectName).empty().append($selection.filter(".no-val"));
                for (let index = 0; index < filterColumns.length; index++) {
                    const filterColumn = filterColumns[index];
                    let $filterElement = $('form *[name='+filterColumn+']');
                    let filter = $filterElement.val();

                    if (filter && filter !== options.emptyFilter) {
                        $(selectName).append( $selection.filter("*[data-"+filterColumn+"="+filter+"]") );
                        break;
                    }
                    else if (filter && filter === options.emptyFilter && options.filterByEmpty) {
                        $(selectName).append( $selection.filter("*[data-"+filterColumn+"="+filter+"]") );
                        $(selectName).find('option[data-previouslyselected=selected]').attr('selected', 'selected');
                        break;
                    }
                    else if (
                            index === filterColumns.length -1
                        &&  options.showAllWhenNoSelection
                    ) {
                        $(selectName).empty().append( $selection );
                    }
                }
                // Fix selection (not working on IE/Edge otherwise)
                $selected = $(selectName).find('option[selected=selected]');
                $defaultFilter = $(selectName).find('option[value=' + options.emptyFilter + ']');
                $previouslySelected = $(selectName).find('option[data-previouslyselected=selected]');

                if ($selected.length > 0 && $selected[0]) {
                    $($selected[0]).attr('selected', 'selected');
                    $(selectName).val($($selected[0]).val());
                } else if ($defaultFilter.length > 0 && $defaultFilter[0]) {
                    $($defaultFilter[0]).attr('selected', 'selected');
                    $(selectName).val($($defaultFilter[0]).val());
                } else if ($previouslySelected.length > 0 && $previouslySelected[0]) {
                    $($previouslySelected[0]).attr('selected', 'selected');
                    $(selectName).val($($previouslySelected[0]).val());
                } else {
                    $anyOption = $(selectName).find('option');
                    $anyOption.attr('selected', 'selected');
                    $(selectName).val($anyOption.val());
                }
            }

            setOptions(dataFilterColumns);

            dataFilterColumns.forEach(dataFilterColumn => {
                let $filterElement = $('form *[name='+dataFilterColumn+']');

                $filterElement.on('change', function(ev) {
                    if ($filterElement.val()) {
                        setOptions(dataFilterColumns);
                        $(selectName).trigger('change');
                    }
                });
            });
        });
    }
    registerUpdaterFor('region', 'pais')
    registerUpdaterFor('provincia', 'region')
    registerUpdaterFor('institucion', ['provincia', 'region', 'pais'], {showAllWhenNoSelection: true})
    registerUpdaterFor('dependencia_institucional', 'institucion')
    registerUpdaterFor('agrupamiento', 'categoria', {emptyFilter: 'all', showAllWhenNoSelection: true, filterByEmpty: true})
    registerUpdaterFor('tipo_de_categoria', 'categoria', {emptyFilter: 'all'})

    document.addEventListener("DOMContentLoaded", function(event) {
        function updateHiddenFields(order) {
            let [o, s] = order.split('_');
            $('input[name=order_by]').val(o);
            $('input[name=sort_order]').val(s);
        }

        let $orderSelect = $('select.orden-select');
        updateHiddenFields($orderSelect.val());

        $orderSelect.on('change', function(ev) {
            updateHiddenFields($orderSelect.val());
        });
    });
</script>
