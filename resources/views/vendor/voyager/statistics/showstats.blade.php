@extends('voyager::master')

@section('page_title', 'Estadisticas')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="fa fa-chart-pie"></i> {{ __('stats.title') }}
        </h1>
        <a href="{{ route('voyager.stats.view', ['agrupamiento' => 'paises.nombre', 'ver_tabla' => 'on']) }}" class="btn btn-primary no-print">
            <i class="fa fa-globe-americas"></i> <span>{{ __('stats.buttons.by_pais') }}</span>
        </a>
        <a href="{{ route('voyager.stats.view', ['agrupamiento' => 'instituciones.nombre', 'ver_tabla' => 'on']) }}" class="btn btn-info no-print">
            <i class="fa fa-university"></i> <span>{{ __('stats.buttons.by_institucion') }}</span>
        </a>
        <a href="{{ route('voyager.stats.view', ['agrupamiento' => 'categoria', 'ver_tabla' => 'on']) }}" class="btn btn-warning no-print">
            <i class="fa fa-sitemap"></i> <span>{{ __('stats.buttons.by_categoria') }}</span>
        </a>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content stats container-fluid">
        @include('voyager::statistics.partials.advanced_search')

        @include('voyager::statistics.partials.grafico')

        @include('voyager::statistics.partials.table')
    </div>
@stop

@section('javascript')
    @include('voyager::statistics.partials.grafico-js')
@stop