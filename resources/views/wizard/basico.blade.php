<h2>{{ __('carga.wizard_basico.'.$modo.'.titulo') }}</h2>
<span>{{ __('carga.wizard_basico.'.$modo.'.subtitulo') }}</span>

<p class="carga-wizard-srequire-warn">
    <span>{{ __('carga.required_warn.before') }}</span>
    <span class="text-danger">{{ __('carga.required_warn.sign') }}</span>
    <span>{{ __('carga.required_warn.after') }}</span>
</p>

<form method="POST" action="{{ route('carga', ['step' => 'extra', 'modo' => $modo]) }}">
    @csrf

    <section class="carga-wizard-section">
        <h4 class="carga-wizard-section-title">{{ __('carga.wizard_basico.'.$modo.'.section_titles.basico') }}</h4>

        <select2 name="institucion_id"
            required="true"
            label="{{ __('carga.wizard_basico.'.$modo.'.institucion.label') }}"
            tooltip="{{ __('carga.wizard_basico.'.$modo.'.institucion.tooltip') }}"
            none="{{ __('carga.sin_seleccionar') }}"
            value="{{ old('institucion_id') }}"
            uses-store="true"
            @if($errors->has('institucion_id')) error="{{$errors->get('institucion_id')[0]}}" @endif>
            @foreach ($instituciones as $institucion)
                <option value="{{$institucion->id}}">{{$institucion->nombre}}</option>
            @endforeach
        </select2>

        <select2 name="dependencia_institucional_id"
            required="true"
            label="{{ __('carga.wizard_basico.'.$modo.'.dependencia_institucional.label') }}"
            tooltip="{{ __('carga.wizard_basico.'.$modo.'.dependencia_institucional.tooltip') }}"
            filter="institucion_id"
            none="{{ __('carga.sin_seleccionar') }}"
            value="{{ old('dependencia_institucional_id') }}"
            @if($errors->has('dependencia_institucional_id')) error="{{$errors->get('dependencia_institucional_id')[0]}}" @endif>
            @foreach ($dependencias_institucionales as $dep_institucional)
                <option value="{{$dep_institucional->id}}" data-filter="{{$dep_institucional->institucion_id}}">{{$dep_institucional->nombre}}</option>
            @endforeach
        </select2>

        <div class="alert alert-warning" role="alert">
            {{ __('carga.wizard_basico.warning_sin_institucion') }}
            <a href="{{ route('carga', ['step' => 'institucion', 'modo' => $modo]) }}">
                {{ __('carga.wizard_basico.cargar_institucion') }}
            </a>
        </div>

        <select2 name="{{'tipo_de_'.$modo.'_id'}}"
            required="true"
            label="{{ __('carga.wizard_basico.'.$modo.'.tipo.label') }}"
            tooltip="{{ __('carga.wizard_basico.'.$modo.'.tipo.tooltip') }}"
            none="{{ __('carga.sin_seleccionar') }}"
            value="{{ old('tipo_de_'.$modo.'_id') }}"
            @if($errors->has('tipo_de_'.$modo.'_id')) error="{{$errors->get('tipo_de_'.$modo.'_id')[0]}}" @endif>
            @foreach ((isset(${'tipos_de_'.$modo.'s'}) ? ${'tipos_de_'.$modo.'s'} : ${'tipos_de_'.$modo.'es'}) as $tipo_de_elemento)
                <option value="{{$tipo_de_elemento->id}}">{{$tipo_de_elemento->nombre}}</option>
            @endforeach
        </select2>

        <div class="form-row">
            <div class="col">
                <advanced-input
                    required="true"
                    name="nombre" value="{{ old('nombre') }}"
                    label="{{ __('carga.wizard_basico.'.$modo.'.nombre.label') }}"
                    tooltip="{{ __('carga.wizard_basico.'.$modo.'.nombre.tooltip') }}"
                    placeholder="{{ __('carga.wizard_basico.'.$modo.'.nombre.placeholder') }}"
                    max="{{ setting('site.max_chars_textfield') }}"
                    @if($errors->has('nombre')) error="{{$errors->get('nombre')[0]}}" @endif>
                </advanced-input>
            </div>
        </div>

        <div class="form-row">
            <div class="col">
                <limited-textarea
                    required="true"
                    name="descripcion" value="{{ old('descripcion') }}"
                    label="{{ __('carga.wizard_basico.'.$modo.'.descripcion.label') }}"
                    tooltip="{{ __('carga.wizard_basico.'.$modo.'.descripcion.tooltip') }}"
                    placeholder="{{ __('carga.wizard_basico.'.$modo.'.descripcion.placeholder') }}"
                    max="{{ setting('site.max_chars_textarea') }}"
                    text_default="{{ __('carga.limite_caracteres.quedan') }}"
                    text_limit="{{ __('carga.limite_caracteres.quedlimitean') }}"
                    text_exceeded="{{ __('carga.limite_caracteres.excedido') }}"
                    @if($errors->has('descripcion')) error="{{$errors->get('descripcion')[0]}}" @endif>
                </limited-textarea>
            </div>
        </div>
    </section>

    <section class="carga-wizard-section">
        <h4 class="carga-wizard-section-title">{{ __('carga.wizard_basico.'.$modo.'.section_titles.responsables') }}</h4>
        <div class="form-row">
            <div class="col">
                <advanced-input
                    name="responsables_nombres"
                    value="{{ old('responsables_nombres') }}"
                    label="{{ __('carga.wizard_basico.'.$modo.'.responsables_nombres.label') }}"
                    tooltip="{{ __('carga.wizard_basico.'.$modo.'.responsables_nombres.tooltip') }}"
                    placeholder="{{ __('carga.wizard_basico.'.$modo.'.responsables_nombres.placeholder') }}"
                    max="{{ setting('site.max_chars_textfield') }}"
                    @if($errors->has('responsables_nombres')) error="{{$errors->get('responsables_nombres')[0]}}" @endif>
                </advanced-input>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <limited-textarea
                    name="responsables_detalles" value="{{ old('responsables_detalles') }}"
                    label="{{ __('carga.wizard_basico.'.$modo.'.responsables_detalles.label') }}"
                    tooltip="{{ __('carga.wizard_basico.'.$modo.'.responsables_detalles.tooltip') }}"
                    placeholder="{{ __('carga.wizard_basico.'.$modo.'.responsables_detalles.placeholder') }}"
                    max="{{ setting('site.max_chars_textarea') }}"
                    text_default="{{ __('carga.limite_caracteres.quedan') }}"
                    text_limit="{{ __('carga.limite_caracteres.quedlimitean') }}"
                    text_exceeded="{{ __('carga.limite_caracteres.excedido') }}"
                    @if($errors->has('responsables_detalles')) error="{{$errors->get('responsables_detalles')[0]}}" @endif>
                </limited-textarea>
            </div>
        </div>
    </section>

    <section class="carga-wizard-section">
        <h4 class="carga-wizard-section-title">{{ __('carga.wizard_basico.'.$modo.'.section_titles.contacto') }}</h4>

        <div class="form-row">
            <div class="col">
                <advanced-input
                    required="true"
                    name="contacto_nombre" value="{{ old('contacto_nombre') }}"
                    label="{{ __('carga.wizard_basico.'.$modo.'.contacto_nombre.label') }}"
                    tooltip="{{ __('carga.wizard_basico.'.$modo.'.contacto_nombre.tooltip') }}"
                    placeholder="{{ __('carga.wizard_basico.'.$modo.'.contacto_nombre.placeholder') }}"
                    max="{{ setting('site.max_chars_textfield') }}"
                    @if($errors->has('contacto_nombre')) error="{{$errors->get('contacto_nombre')[0]}}" @endif>
                </advanced-input>
            </div>
        </div>

        <div class="form-row">
            <div class="col">
                <advanced-input
                    required="true"
                    name="contacto_telefono" value="{{ old('contacto_telefono') }}"
                    label="{{ __('carga.wizard_basico.'.$modo.'.contacto_telefono.label') }}"
                    tooltip="{{ __('carga.wizard_basico.'.$modo.'.contacto_telefono.tooltip') }}"
                    placeholder="{{ __('carga.wizard_basico.'.$modo.'.contacto_telefono.placeholder') }}"
                    max="{{ setting('site.max_chars_textfield') }}"
                    @if($errors->has('contacto_telefono')) error="{{$errors->get('contacto_telefono')[0]}}" @endif>
                </advanced-input>
            </div>
        </div>

        <div class="form-row">
            <div class="col">
                <advanced-input
                    required="true"
                    name="contacto_email" value="{{ old('contacto_email') }}"
                    label="{{ __('carga.wizard_basico.'.$modo.'.contacto_email.label') }}"
                    tooltip="{{ __('carga.wizard_basico.'.$modo.'.contacto_email.tooltip') }}"
                    placeholder="{{ __('carga.wizard_basico.'.$modo.'.contacto_email.placeholder') }}"
                    max="{{ setting('site.max_chars_textfield') }}"
                    @if($errors->has('contacto_email')) error="{{$errors->get('contacto_email')[0]}}" @endif>
                </advanced-input>
            </div>
        </div>
    </section>

    <div class="button-wizard-bottom d-flex justify-content-center">
        <a class="btn btn-primary btn-lg"
            href="{{ route('carga', ['step' => 'inicio']) }}">
            <span class="fas fa-arrow-circle-left fa-2x"></span>
            <span>{{ __('carga.wizard_basico.paso_anterior') }}</span>
        </a>
        <button class="btn btn-primary btn-lg"
            type="submit">
            <span>{{ __('carga.wizard_basico.paso_siguiente') }}</span>
            <span class="fas fa-arrow-circle-right fa-2x"></span>
        </button>
    </div>
</form>
