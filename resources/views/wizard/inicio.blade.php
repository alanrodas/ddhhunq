<h2>{{ __('carga.wizard_inicio.titulo') }}</h2>
<span>{{ __('carga.wizard_inicio.subtitulo') }}</span>

<h3 class="mt-3">
    {{ __('carga.wizard_inicio.option_select.text') }}
</h3>
<div class="button-wizard-start">
    <a class="btn btn-primary btn-lg"
        href="{{ route('carga', ['step' => 'basico', 'modo' => 'curso'])}}">
        {{ __('carga.wizard_inicio.option_select.curso') }}
    </a>
    <a class="btn btn-primary btn-lg"
        href="{{ route('carga', ['step' => 'basico', 'modo' => 'proyecto'])}}">
        {{ __('carga.wizard_inicio.option_select.proyecto') }}
    </a>
    <a class="btn btn-primary btn-lg"
        href="{{ route('carga', ['step' => 'basico', 'modo' => 'publicacion'])}}">
        {{ __('carga.wizard_inicio.option_select.publicacion') }}
    </a>
</div>


<div class="alert alert-danger" role="alert">
    <p>{{ __('carga.wizard_inicio.warning_text') }}</p>
    <p>{{ __('carga.wizard_inicio.danger_text') }}</p>
</div>
