<h2>{{ __('carga.wizard_extra.'.$modo.'.titulo') }}</h2>
<span>{{ __('carga.wizard_extra.'.$modo.'.subtitulo') }}</span>

<p class="carga-wizard-srequire-warn">
    <span>{{ __('carga.required_warn.before') }}</span>
    <span class="text-danger">{{ __('carga.required_warn.sign') }}</span>
    <span>{{ __('carga.required_warn.after') }}</span>
</p>

<form method="POST" action="{{ route('carga', ['step' => 'final', 'modo' => $modo]) }}" enctype="multipart/form-data">
    @csrf

    {{-- Fields from previous step are hidden until user saves --}}
    <input type="hidden" name="{{'tipo_de_'.$modo.'_id'}}" value="{{ ${'tipo_de_'.$modo.'_id'} ?: old('tipo_de_'.$modo.'_id') }}">
    <input type="hidden" name="nombre" value="{{ $nombre ?: old('nombre') }}">
    <input type="hidden" name="institucion_id" value="{{ $institucion_id ?: old('institucion_id') }}">
    <input type="hidden" name="dependencia_institucional_id" value="{{ $dependencia_institucional_id ?: old('dependencia_institucional_id') }}">
    <input type="hidden" name="descripcion" value="{{ $descripcion ?: old('descripcion')}}">
    <input type="hidden" name="responsables_nombres" value="{{ $responsables_nombres ?: old('responsables_nombres')}}">
    <input type="hidden" name="responsables_detalles" value="{{ $responsables_detalles ?: old('responsables_detalles')}}">
    <input type="hidden" name="contacto_nombre" value="{{ $contacto_nombre ?: old('contacto_nombre')}}">
    <input type="hidden" name="contacto_telefono" value="{{ $contacto_telefono ?: old('contacto_telefono')}}">
    <input type="hidden" name="contacto_email" value="{{ $contacto_email ?: old('contacto_email')}}">

    @include('wizard/extra_'.$modo)

    <section class="carga-wizard-section">
        <h4 class="carga-wizard-section-title">{{ __('carga.wizard_extra.'.$modo.'.section_titles.adjuntos') }}</h4>

        <file-upload
            name="adjuntos"
            btn-label="{{ __('carga.file_uploader.boton') }}"
            label-none="{{ __('carga.file_uploader.nada_seleccionado') }}"
            label-selected="{{ __('carga.file_uploader.archivo_seleccionado') }}"
            max-files="{{ $max_upload_files }}"
            tooltip="{{ __('carga.file_uploader.tooltip', [
                    'max_upload_file_size' => $max_upload_file_size,
                    'max_upload_files' => $max_upload_files
                ]) }}"
            max-size="{{ $max_upload_file_size }}"
            file-size-exceeded-text="{{ __('carga.file_uploader.error_max_filesize') }}"
            @if($errors->has('adjuntos.0')) error="{{$errors->get('adjuntos.0')[0]}}"
            @elseif($errors->has('adjuntos.1')) error="{{$errors->get('adjuntos.1')[0]}}"
            @elseif($errors->has('adjuntos.2')) error="{{$errors->get('adjuntos.2')[0]}}"
            @elseif($errors->has('adjuntos.3')) error="{{$errors->get('adjuntos.3')[0]}}"
            @elseif($errors->has('adjuntos.4')) error="{{$errors->get('adjuntos.4')[0]}}"
            @elseif($errors->has('adjuntos.5')) error="{{$errors->get('adjuntos.5')[0]}}"
            @elseif($errors->has('adjuntos.6')) error="{{$errors->get('adjuntos.6')[0]}}"
            @elseif($errors->has('adjuntos.7')) error="{{$errors->get('adjuntos.7')[0]}}"
            @elseif($errors->has('adjuntos.8')) error="{{$errors->get('adjuntos.8')[0]}}"
            @elseif($errors->has('adjuntos.9')) error="{{$errors->get('adjuntos.9')[0]}}"
            @endif
            >
        </file-upload>

    </section>

    <div class="button-wizard-bottom d-flex justify-content-center">
        <a class="btn btn-primary btn-lg"
            href="{{ route('carga', ['step' => 'basico', 'modo' => $modo]) }}">
            <span class="fas fa-arrow-circle-left fa-2x"></span>
            <span>{{ __('carga.wizard_extra.paso_anterior') }}</span>
        </a>
        <button class="btn btn-success btn-lg"
            type="submit">
            <span>{{ __('carga.wizard_extra.paso_siguiente') }}</span>
            <span class="fas fa-check fa-2x"></span>
        </button>
    </div>
</form>
