<h2>{{ __('carga.final.titulo') }}</h2>
<span>{{ __('carga.final.subtitulo') }}</span>

<form method="GET" action="{{ route('carga', ['step' => 0]) }}">
    @csrf
    <div class="button-wizard-bottom d-flex justify-content-center">
        <a class="btn btn-primary btn-lg"
            href="{{ route('home') }}">
            <span class="fas fa-home fa-2x"></span>
            <span>{{ __('carga.final.ir_a_inicio') }}</span>
        </a>
        <button class="btn btn-primary btn-lg"
            type="submit">
            <span>{{ __('carga.final.cargar_nuevo') }}</span>
            <span class="fas fa-redo-alt fa-2x"></span>
        </button>
    </div>
</form>
