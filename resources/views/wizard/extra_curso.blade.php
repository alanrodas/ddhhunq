<section class="carga-wizard-section">
    <h4 class="carga-wizard-section-title">{{ __('carga.wizard_extra.'.$modo.'.section_titles.elemento') }}</h4>

    <div class="form-row">
        <div class="col-sm-12 col-md-6">
            <select2 name="tipo_de_intensidad_id"
                required="true"
                label="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_intensidad.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_intensidad.tooltip') }}"
                none="{{ __('carga.sin_seleccionar') }}"
                value="{{ old('tipo_de_intensidad_id') }}"
                @if($errors->has('tipo_de_intensidad_id')) error="{{$errors->get('tipo_de_intensidad_id')[0]}}" @endif>
                @foreach ($tipos_de_intensidades as $tipo_de_intensidad)
                    <option value="{{$tipo_de_intensidad->id}}">{{$tipo_de_intensidad->nombre}}</option>
                @endforeach
            </select2>
        </div>

        <div class="col-sm-12 col-md-6">
            <select2 name="tipo_de_modalidad_id"
                required="true"
                label="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_modalidad.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_modalidad.tooltip') }}"
                none="{{ __('carga.sin_seleccionar') }}"
                value="{{ old('tipo_de_modalidad_id') }}"
                @if($errors->has('tipo_de_modalidad_id')) error="{{$errors->get('tipo_de_modalidad_id')[0]}}" @endif>
                @foreach ($tipos_de_modalidades as $tipo_de_modalidad)
                    <option value="{{$tipo_de_modalidad->id}}">{{$tipo_de_modalidad->nombre}}</option>
                @endforeach
            </select2>
        </div>

        <div class="col-sm-12 col-md-6">
            <select2 name="tipo_de_regularidad_id"
                required="true"
                label="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_regularidad.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_regularidad.tooltip') }}"
                none="{{ __('carga.sin_seleccionar') }}"
                value="{{ old('tipo_de_regularidad_id') }}"
                @if($errors->has('tipo_de_regularidad_id')) error="{{$errors->get('tipo_de_regularidad_id')[0]}}" @endif>
                @foreach ($tipos_de_regularidades as $tipo_de_regularidad)
                    <option value="{{$tipo_de_regularidad->id}}">{{$tipo_de_regularidad->nombre}}</option>
                @endforeach
            </select2>
        </div>

        <div class="col-sm-12 col-md-6">
            <select2 name="tipo_de_nivel_id"
                required="true"
                label="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_nivel.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_nivel.tooltip') }}"
                none="{{ __('carga.sin_seleccionar') }}"
                value="{{ old('tipo_de_nivel_id') }}"
                @if($errors->has('tipo_de_nivel_id')) error="{{$errors->get('tipo_de_nivel_id')[0]}}" @endif>
                @foreach ($tipos_de_niveles as $tipo_de_nivel)
                    <option value="{{$tipo_de_nivel->id}}">{{$tipo_de_nivel->nombre}}</option>
                @endforeach
            </select2>
        </div>

        <div class="col-sm-12 col-md-6">
            <select2 name="tipo_de_curricularidad_id"
                required="true"
                label="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_curricularidad.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_curricularidad.tooltip') }}"
                none="{{ __('carga.sin_seleccionar') }}"
                value="{{ old('tipo_de_curricularidad_id') }}"
                @if($errors->has('tipo_de_curricularidad_id')) error="{{$errors->get('tipo_de_curricularidad_id')[0]}}" @endif>
                @foreach ($tipos_de_curricularidades as $tipo_de_curricularidad)
                    <option value="{{$tipo_de_curricularidad->id}}">{{$tipo_de_curricularidad->nombre}}</option>
                @endforeach
            </select2>
        </div>

        <div class="col-sm-12 col-md-6 d-flex align-items-center">
            <toggle
                label="{{ __('carga.wizard_extra.'.$modo.'.arancelada.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.arancelada.tooltip') }}"
                name="arancelado"
                value="{{ old('arancelado') || 1 }}"></toggle>
        </div>
    </div>

    <div class="form-row">
        <div class="col-sm-12 col-md-6">
            <datepicker
                label="{{ __('carga.wizard_extra.'.$modo.'.fecha_de_inicio.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.fecha_de_inicio.tooltip') }}"
                name="fecha_de_inicio"
                value="{{ old('fecha_de_inicio') }}"
                @if($errors->has('fecha_de_inicio')) error="{{$errors->get('fecha_de_inicio')[0]}}" @endif>
            </datepicker>
        </div>
        <div class="col-sm-12 col-md-6">
            <datepicker
                label="{{ __('carga.wizard_extra.'.$modo.'.fecha_de_fin.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.fecha_de_fin.tooltip') }}"
                name="fecha_de_fin"
                value="{{ old('fecha_de_fin') }}"
                @if($errors->has('fecha_de_fin')) error="{{$errors->get('fecha_de_fin')[0]}}" @endif>
            </datepicker>
        </div>
    </div>
</section>

<section class="carga-wizard-section">
    <h4 class="carga-wizard-section-title">{{ __('carga.wizard_extra.'.$modo.'.section_titles.otros') }}</h4>

    <div class="form-row">
        <div class="col">
            <limited-textarea
                name="estrategia_metodologica" value="{{ old('estrategia_metodologica') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.estrategia_metodologica.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.estrategia_metodologica.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.estrategia_metodologica.placeholder') }}"
                max="{{ setting('site.max_chars_textarea') }}"
                text_default="{{ __('carga.limite_caracteres.quedan') }}"
                text_limit="{{ __('carga.limite_caracteres.quedlimitean') }}"
                text_exceeded="{{ __('carga.limite_caracteres.excedido') }}"
                @if($errors->has('estrategia_metodologica')) error="{{$errors->get('estrategia_metodologica')[0]}}" @endif>
            </limited-textarea>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <limited-textarea
                name="condiciones" value="{{ old('condiciones') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.condiciones.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.condiciones.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.condiciones.placeholder') }}"
                max="{{ setting('site.max_chars_textarea') }}"
                text_default="{{ __('carga.limite_caracteres.quedan') }}"
                text_limit="{{ __('carga.limite_caracteres.quedlimitean') }}"
                text_exceeded="{{ __('carga.limite_caracteres.excedido') }}"
                @if($errors->has('condiciones')) error="{{$errors->get('condiciones')[0]}}" @endif>
            </limited-textarea>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <limited-textarea
                name="otros" value="{{ old('otros') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.otros.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.otros.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.otros.placeholder') }}"
                max="{{ setting('site.max_chars_textarea') }}"
                text_default="{{ __('carga.limite_caracteres.quedan') }}"
                text_limit="{{ __('carga.limite_caracteres.quedlimitean') }}"
                text_exceeded="{{ __('carga.limite_caracteres.excedido') }}"
                @if($errors->has('otros')) error="{{$errors->get('otros')[0]}}" @endif>
            </limited-textarea>
        </div>
    </div>
</section>

<section class="carga-wizard-section">
    <h4 class="carga-wizard-section-title">{{ __('carga.wizard_extra.'.$modo.'.section_titles.articulacion') }}</h4>

    <div class="form-row">
        <div class="col">
            <select2 name="tipo_de_articulacion_id"
                label="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_articulacion.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_articulacion.tooltip') }}"
                none="{{ __('carga.sin_seleccionar') }}"
                value="{{ old('tipo_de_articulacion_id') }}"
                @if($errors->has('tipo_de_articulacion_id')) error="{{$errors->get('tipo_de_articulacion_id')[0]}}" @endif>
                @foreach ($tipos_de_articulaciones as $tipo_de_articulacion)
                    <option value="{{$tipo_de_articulacion->id}}">{{$tipo_de_articulacion->nombre}}</option>
                @endforeach
        </select2>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <limited-textarea
                name="articulacion_detalles" value="{{ old('articulacion_detalles') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.articulacion_detalles.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.articulacion_detalles.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.articulacion_detalles.placeholder') }}"
                max="{{ setting('site.max_chars_textarea') }}"
                text_default="{{ __('carga.limite_caracteres.quedan') }}"
                text_limit="{{ __('carga.limite_caracteres.quedlimitean') }}"
                text_exceeded="{{ __('carga.limite_caracteres.excedido') }}"
                @if($errors->has('articulacion_detalles')) error="{{$errors->get('articulacion_detalles')[0]}}" @endif>
            </limited-textarea>
        </div>
    </div>
</section>

