<h2>{{ __('carga.final_institucion.titulo') }}</h2>
<span>{{ __('carga.final_institucion.subtitulo') }}</span>
<span>{{ __('carga.final_institucion.subsubtitulo') }}</span>

<h3 class="mt-3">
    {{ __('carga.final_institucion.option_select.text') }}
</h3>
<div class="button-wizard-start">
    <a class="btn btn-primary btn-lg"
        href="{{ route('carga', ['step' => 'basico', 'modo' => 'curso'])}}">
        {{ __('carga.final_institucion.option_select.curso') }}
    </a>
    <a class="btn btn-primary btn-lg"
        href="{{ route('carga', ['step' => 'basico', 'modo' => 'proyecto'])}}">
        {{ __('carga.final_institucion.option_select.proyecto') }}
    </a>
    <a class="btn btn-primary btn-lg"
        href="{{ route('carga', ['step' => 'basico', 'modo' => 'publicacion'])}}">
        {{ __('carga.final_institucion.option_select.publicacion') }}
    </a>
</div>
