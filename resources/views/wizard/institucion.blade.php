<h2>{{ __('carga.wizard_institucion.titulo') }}</h2>
<span>{{ __('carga.wizard_institucion.subtitulo') }}</span>

<p class="carga-wizard-srequire-warn">
    <span>{{ __('carga.required_warn.before') }}</span>
    <span class="text-danger">{{ __('carga.required_warn.sign') }}</span>
    <span>{{ __('carga.required_warn.after') }}</span>
</p>

<form method="POST" action="{{ route('carga', ['step' => 'final_institucion']) }}">
    @csrf

    <section class="carga-wizard-section">
        <h4 class="carga-wizard-section-title">{{ __('carga.wizard_institucion.section_titles.institucion') }}</h4>

        <select2 name="institucion_id"
            label="{{ __('carga.wizard_institucion.institucion.label') }}"
            tooltip="{{ __('carga.wizard_institucion.institucion.tooltip') }}"
            none="{{ __('carga.wizard_institucion.institucion_no_en_lista') }}"
            value="{{ old('institucion_id') }}"
            uses-store="true"
            @if($errors->has('institucion_id')) error="{{$errors->get('institucion_id')[0]}}" @endif>
            @foreach ($instituciones as $institucion)
                <option value="{{$institucion->id}}">{{$institucion->nombre}}</option>
            @endforeach
        </select2>

        <institucion-carga-nueva>
            <select2 slot="pais" name="pais"
                required="true"
                label="{{ __('carga.wizard_institucion.pais.label') }}"
                tooltip="{{ __('carga.wizard_institucion.pais.tooltip') }}"
                none="{{ __('busqueda.sin_seleccionar') }}" uses-store="true"
                value="{{ old('pais') }}"
                @if($errors->has('pais')) error="{{$errors->get('pais')[0]}}" @endif>
                @foreach($paises as $pais)
                    <option value="{{$pais->id}}" data-filter="{{$pais->id}}">
                        {{$pais->nombre}}
                    </option>
                @endforeach
            </select2>

            <select2 slot="region" name="region"
                required="true"
                label="{{ __('carga.wizard_institucion.region.label') }}"
                tooltip="{{ __('carga.wizard_institucion.region.tooltip') }}"
                none="{{ __('busqueda.sin_seleccionar') }}" uses-store="true"
                filter="pais"
                value="{{ old('region') }}"
                @if($errors->has('region')) error="{{$errors->get('region')[0]}}" @endif>
                @foreach($regiones as $region)
                    <option value="{{$region->id}}" data-filter="{{$region->pais_id}}">
                        {{$region->nombre}}
                    </option>
                @endforeach
            </select2>

            <select2 slot="provincia" name="provincia"
                required="true"
                label="{{ __('carga.wizard_institucion.provincia.label') }}"
                tooltip="{{ __('carga.wizard_institucion.provincia.tooltip') }}"
                none="{{ __('busqueda.sin_seleccionar') }}" uses-store="true"
                value="{{ old('provincia') }}"
                filter="region"
                @if($errors->has('provincia')) error="{{$errors->get('provincia')[0]}}" @endif>
                @foreach($provincias as $provincia)
                    <option value="{{$provincia->id}}" data-filter="{{$provincia->region_id}}">
                        {{$provincia->nombre}}
                    </option>
                @endforeach
            </select2>

            <advanced-input slot="nombre" name="nombre"
                required="true"
                label="{{ __('carga.wizard_institucion.nombre.label') }}"
                tooltip="{{ __('carga.wizard_institucion.nombre.tooltip') }}"
                placeholder="{{ __('carga.wizard_institucion.nombre.placeholder') }}"
                value="{{ old('nombre') }}"
                @if($errors->has('nombre')) error="{{$errors->get('nombre')[0]}}" @endif>
            </advanced-input>

            <select2 slot="tipo_de_institucion" name="tipo_de_institucion_id"
                required="true"
                label="{{ __('carga.wizard_institucion.tipo_de_institucion.label') }}"
                tooltip="{{ __('carga.wizard_institucion.tipo_de_institucion.tooltip') }}"
                none="{{ __('carga.sin_seleccionar') }}"
                value="{{ old('tipo_de_institucion_id') }}"
                @if($errors->has('tipo_de_institucion_id')) error="{{$errors->get('tipo_de_institucion_id')[0]}}" @endif>
                @foreach ($tipos_de_instituciones as $tipo_de_institucion)
                    <option value="{{$tipo_de_institucion->id}}">{{$tipo_de_institucion->nombre}}</option>
                @endforeach
            </select2>

            <advanced-input slot="direccion" name="direccion"
                required="true"
                label="{{ __('carga.wizard_institucion.direccion.label') }}"
                tooltip="{{ __('carga.wizard_institucion.direccion.tooltip') }}"
                placeholder="{{ __('carga.wizard_institucion.direccion.placeholder') }}"
                value="{{ old('direccion') }}"
                @if($errors->has('direccion')) error="{{$errors->get('direccion')[0]}}" @endif>
            </advanced-input>

            <advanced-input slot="codigo_postal" name="codigo_postal"
                required="true"
                label="{{ __('carga.wizard_institucion.codigo_postal.label') }}"
                tooltip="{{ __('carga.wizard_institucion.codigo_postal.tooltip') }}"
                placeholder="{{ __('carga.wizard_institucion.codigo_postal.placeholder') }}"
                value="{{ old('codigo_postal') }}"
                @if($errors->has('codigo_postal')) error="{{$errors->get('codigo_postal')[0]}}" @endif>
            </advanced-input>

            <advanced-input slot="telefono" name="telefono"
                required="true"
                label="{{ __('carga.wizard_institucion.telefono.label') }}"
                tooltip="{{ __('carga.wizard_institucion.telefono.tooltip') }}"
                placeholder="{{ __('carga.wizard_institucion.telefono.placeholder') }}"
                value="{{ old('telefono') }}"
                @if($errors->has('telefono')) error="{{$errors->get('telefono')[0]}}" @endif>
            </advanced-input>

            <advanced-input slot="email" name="email"
                required="true"
                label="{{ __('carga.wizard_institucion.email.label') }}"
                tooltip="{{ __('carga.wizard_institucion.email.tooltip') }}"
                placeholder="{{ __('carga.wizard_institucion.email.placeholder') }}"
                value="{{ old('email') }}"
                @if($errors->has('email')) error="{{$errors->get('email')[0]}}" @endif>
            </advanced-input>

        </institucion-carga-nueva>

    </section>

    <section class="carga-wizard-section">
        <h4 class="carga-wizard-section-title">{{ __('carga.wizard_institucion.section_titles.dependencia_institucional') }}</h4>

        <advanced-input name="dependencia_institucional"
            required="true"
            label="{{ __('carga.wizard_institucion.dependencia_institucional.label') }}"
            tooltip="{{ __('carga.wizard_institucion.dependencia_institucional.tooltip') }}"
            value="{{ old('dependencia_institucional') }}"
            @if($errors->has('dependencia_institucional')) error="{{$errors->get('dependencia_institucional')[0]}}" @endif>
        </advanced-input>

        <select2 name="tipo_de_dependencia_institucional_id"
            required="true"
            label="{{ __('carga.wizard_institucion.tipo_de_dependencia_institucional.label') }}"
            tooltip="{{ __('carga.wizard_institucion.tipo_de_dependencia_institucional.tooltip') }}"
            none="{{ __('carga.sin_seleccionar') }}"
            value="{{ old('tipo_de_dependencia_institucional_id') }}"
            @if($errors->has('tipo_de_dependencia_institucional_id')) error="{{$errors->get('tipo_de_dependencia_institucional_id')[0]}}" @endif>
            @foreach ($tipos_de_dependencias_institucionales as $tipo_de_dependencia_institucional)
                <option value="{{$tipo_de_dependencia_institucional->id}}">{{$tipo_de_dependencia_institucional->nombre}}</option>
            @endforeach
        </select2>
    </section>

    <div class="button-wizard-bottom d-flex justify-content-center">
        <a class="btn btn-primary btn-lg"
            href="{{ route('carga', ['step' => 'basico', 'modo' => $modo]) }}">
            <span class="fas fa-arrow-circle-left fa-2x"></span>
            <span>{{ __('carga.wizard_institucion.paso_anterior') }}</span>
        </a>
        <button class="btn btn-success btn-lg"
            type="submit">
            <span>{{ __('carga.wizard_institucion.paso_siguiente') }}</span>
            <span class="fas fa-arrow-circle-right fa-2x"></span>
        </button>
    </div>
</form>
