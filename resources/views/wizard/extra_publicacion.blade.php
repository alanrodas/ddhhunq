<section class="carga-wizard-section">
    <h4 class="carga-wizard-section-title">{{ __('carga.wizard_extra.'.$modo.'.section_titles.elemento') }}</h4>

    <div class="form-row">
        <div class="col-12">
            <advanced-input
                name="editorial" value="{{ old('editorial') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.editorial.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.editorial.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.editorial.placeholder') }}"
                @if($errors->has('editorial')) error="{{$errors->get('editorial')[0]}}" @endif>
            </advanced-input>
        </div>

        <div class="col-sm-12 col-md-6">
            <advanced-input
                name="anho" value="{{ old('anho') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.anho.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.anho.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.anho.placeholder') }}"
                @if($errors->has('anho')) error="{{$errors->get('anho')[0]}}" @endif>
            </advanced-input>
        </div>

        <div class="col-sm-12 col-md-6">
            <advanced-input
                name="ciudad" value="{{ old('ciudad') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.ciudad.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.ciudad.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.ciudad.placeholder') }}"
                @if($errors->has('ciudad')) error="{{$errors->get('ciudad')[0]}}" @endif>
            </advanced-input>
        </div>

        <div class="col-sm-12 col-md-6">
            <advanced-input
                name="serie" value="{{ old('serie') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.serie.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.serie.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.serie.placeholder') }}"
                @if($errors->has('serie')) error="{{$errors->get('serie')[0]}}" @endif>
            </advanced-input>
        </div>

        <div class="col-sm-12 col-md-6">
            <advanced-input
                name="edicion" value="{{ old('edicion') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.edicion.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.edicion.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.edicion.placeholder') }}"
                @if($errors->has('edicion')) error="{{$errors->get('edicion')[0]}}" @endif>
            </advanced-input>
        </div>

        <div class="col-sm-12 col-md-6">
            <advanced-input
                name="isbn" value="{{ old('isbn') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.isbn.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.isbn.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.isbn.placeholder') }}"
                @if($errors->has('isbn')) error="{{$errors->get('isbn')[0]}}" @endif>
            </advanced-input>
        </div>
    </div>
</section>
