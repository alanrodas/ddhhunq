<section class="carga-wizard-section">
    <h4 class="carga-wizard-section-title">{{ __('carga.wizard_extra.'.$modo.'.section_titles.elemento') }}</h4>

    <div class="form-row">
        <div class="col">
            <limited-textarea
                required="true"
                name="destinatarios" value="{{ old('destinatarios') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.destinatarios.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.destinatarios.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.destinatarios.placeholder') }}"
                max="{{ setting('site.max_chars_textarea') }}"
                text_default="{{ __('carga.limite_caracteres.quedan') }}"
                text_limit="{{ __('carga.limite_caracteres.quedlimitean') }}"
                text_exceeded="{{ __('carga.limite_caracteres.excedido') }}"
                @if($errors->has('destinatarios')) error="{{$errors->get('destinatarios')[0]}}" @endif>
            </limited-textarea>
        </div>
    </div>
</section>

<section class="carga-wizard-section">
    <h4 class="carga-wizard-section-title">{{ __('carga.wizard_extra.'.$modo.'.section_titles.articulacion') }}</h4>

    <div class="form-row">
        <div class="col">
            <select2 name="tipo_de_articulacion_id"
                label="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_articulacion.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.tipo_de_articulacion.tooltip') }}"
                none="{{ __('carga.sin_seleccionar') }}"
                value="{{ old('tipo_de_articulacion_id') }}"
                @if($errors->has('tipo_de_articulacion_id')) error="{{$errors->get('tipo_de_articulacion_id')[0]}}" @endif>
                @foreach ($tipos_de_articulaciones as $tipo_de_articulacion)
                    <option value="{{$tipo_de_articulacion->id}}">{{$tipo_de_articulacion->nombre}}</option>
                @endforeach
        </select2>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <limited-textarea
                name="articulacion_detalles" value="{{ old('articulacion_detalles') }}"
                label="{{ __('carga.wizard_extra.'.$modo.'.articulacion_detalles.label') }}"
                tooltip="{{ __('carga.wizard_extra.'.$modo.'.articulacion_detalles.tooltip') }}"
                placeholder="{{ __('carga.wizard_extra.'.$modo.'.articulacion_detalles.placeholder') }}"
                max="{{ setting('site.max_chars_textarea') }}"
                text_default="{{ __('carga.limite_caracteres.quedan') }}"
                text_limit="{{ __('carga.limite_caracteres.quedlimitean') }}"
                text_exceeded="{{ __('carga.limite_caracteres.excedido') }}"
                @if($errors->has('articulacion_detalles')) error="{{$errors->get('articulacion_detalles')[0]}}" @endif>
            </limited-textarea>
        </div>
    </div>
</section>
