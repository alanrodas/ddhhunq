<div class="search-container">
    <div>
        <h2>{{ __('busqueda.simple.titulo') }}</h2>
        <span>{{ __('busqueda.simple.nota') }}</span>
        <form method="GET" action="{{ route('busqueda.simple') }}">
            <div class="input-group">
                <input type="text" class="form-control" name="query"
                    placeholder="{{ __('busqueda.simple.placeholders.query') }}">
                <button class="btn btn-primary" type="submit"
                    data-toggle="tooltip" data-placement="bottom"
                    title="{{ __('busqueda.simple.submit') }}}">
                    <span class="fas fa-search"></span>
                </button>
            </div>
        </form>
    </div>
    <div class="link-avanzada mt-5">
        {!! __('busqueda.simple.links.avanzada', [
                'link_start' => '<a class="btn btn-primary" href="'.route('busqueda.avanzada').'">',
                'link_end' => '</a>'
            ])
        !!}
        {!! __('busqueda.simple.links.todos', [
                'link_start' => '<a class="btn btn-primary ml-5" href="'.route('busqueda.simple', ['query' => '%']).'">',
                'link_end' => '</a>'
            ])
        !!}
    </div>
</div>
