<div class="search-container">
    <div>
        <h2>{{ __('busqueda.avanzada.titulo') }}</h2>
        <span>{{ __('busqueda.avanzada.nota') }}</span>
        <form method="POST" action="{{ route('busqueda.avanzada_handle') }}">
            @csrf()

            {{-- Components here are made using Vue.js to provide interactive functionality --}}
            {{-- Chek out resources/js/components to see their code --}}
            <buscador-selector-ubicacion>
                <select2 slot="pais" name="pais" label="{{ __('busqueda.avanzada.labels.pais') }}"
                    none="{{ __('busqueda.sin_seleccionar') }}" uses-store="true">
                    @foreach($paises as $pais)
                        <option value="{{$pais->id}}" data-filter="{{$pais->id}}">
                            {{$pais->nombre}}
                        </option>
                    @endforeach
                </select2>

                <select2 slot="region" name="region" label="{{ __('busqueda.avanzada.labels.region') }}"
                    none="{{ __('busqueda.sin_seleccionar') }}" uses-store="true"
                    filter="pais">
                    @foreach($regiones as $region)
                        <option value="{{$region->id}}" data-filter="{{$region->pais_id}}">
                            {{$region->nombre}}
                        </option>
                    @endforeach
                </select2>

                <select2 slot="provincia" name="provincia" label="{{ __('busqueda.avanzada.labels.provincia') }}"
                    none="{{ __('busqueda.sin_seleccionar') }}"
                    filter="region">
                    @foreach($provincias as $provincia)
                        <option value="{{$provincia->id}}" data-filter="{{$provincia->region_id}}">
                            {{$provincia->nombre}}
                        </option>
                    @endforeach
                </select2>
            </buscador-selector-ubicacion>

            <buscador-selector-institucion>
                <select2 name="institucion" label="{{ __('busqueda.avanzada.labels.institucion') }}"
                    none="{{ __('busqueda.sin_seleccionar') }}">
                    @foreach($instituciones as $institucion)
                        <option value="{{$institucion->id}}">{{$institucion->nombre}}</option>
                    @endforeach
                </select2>
            </buscador-selector-institucion>

            <buscador-selector-elemento
                tipo_de_elemento_name="tipo_de_elemento"
                tipo_de_elemento_label="{{ __('busqueda.avanzada.labels.tipo_de_elemento') }}"
                tipo_de_elemento_none="{{ __('busqueda.sin_seleccionar') }}"
                >
                <option value="curso">{{ __('busqueda.tipo_de_elemento.curso.display_plural_name') }}</option>
                <option value="proyecto">{{ __('busqueda.tipo_de_elemento.proyecto.display_plural_name') }}</option>
                <option value="publicacion">{{ __('busqueda.tipo_de_elemento.publicacion.display_plural_name') }}</option>
                <div class="form-row" slot="curso">
                    <div class="col-md">
                        <select2 name="tipo_de_curso" label="{{ __('busqueda.avanzada.labels.tipo_de_curso') }}"
                            none="{{ __('busqueda.sin_seleccionar') }}">
                            @foreach($tipos_de_cursos as $tipo_de_curso)
                                <option value="{{$tipo_de_curso->id}}">{{$tipo_de_curso->nombre}}</option>
                            @endforeach
                        </select2>
                    </div>

                    <div class="col-md">
                        <select2 name="tipo_de_modalidad" label="{{ __('busqueda.avanzada.labels.tipo_de_modalidad') }}"
                            none="{{ __('busqueda.sin_seleccionar') }}">
                            @foreach($tipos_de_modalidades as $tipo_de_modalidad)
                                <option value="{{$tipo_de_modalidad->id}}">{{$tipo_de_modalidad->nombre}}</option>
                            @endforeach
                        </select2>
                    </div>

                    <div class="col-md">
                        <select2 name="tipo_de_nivel" label="{{ __('busqueda.avanzada.labels.tipo_de_nivel') }}"
                            none="{{ __('busqueda.sin_seleccionar') }}">
                            @foreach($tipos_de_niveles as $tipo_de_nivel)
                                <option value="{{$tipo_de_nivel->id}}">{{$tipo_de_nivel->nombre}}</option>
                            @endforeach
                        </select2>
                    </div>
                </div>

                <select2 slot="publicacion" name="tipo_de_publicacion" label="{{ __('busqueda.avanzada.labels.tipo_de_publicacion') }}"
                    none="{{ __('busqueda.sin_seleccionar') }}">
                    @foreach($tipos_de_publicaciones as $tipo_de_publicacion)
                        <option value="{{$tipo_de_publicacion->id}}">{{$tipo_de_publicacion->nombre}}</option>
                    @endforeach
                </select2>

                <select2 slot="proyecto" name="tipo_de_proyecto" label="{{ __('busqueda.avanzada.labels.tipo_de_proyecto') }}"
                    none="{{ __('busqueda.sin_seleccionar') }}">
                    @foreach($tipos_de_proyectos as $tipo_de_proyecto)
                        <option value="{{$tipo_de_proyecto->id}}">{{$tipo_de_proyecto->nombre}}</option>
                    @endforeach
                </select2>
            </buscador-selector-elemento>

            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="keywords">{{ __('busqueda.avanzada.labels.keywords') }}</label>
                        <input type="text" class="form-control"
                            name="keywords" id="keywords"
                            placeholder="{{ __('busqueda.avanzada.placeholders.keywords') }}"
                        >
                    </div>
                </div>
            </div>
            <div class="form-group d-flex justify-content-center">
                <button class="btn btn-primary" type="submit"
                    data-toggle="tooltip" data-placement="bottom"
                    title="__('busqueda.avanzada.submit')">
                    <span class="fas fa-search"></span>
                    <span>{{ __('busqueda.avanzada.submit') }}</span>
                </button>
            </div>
        </form>
    </div>
    <div class="link-simple mt-5">
        {!! __('busqueda.avanzada.links.simple', [
                'link_start' => '<a class="btn btn-primary" href="'.route('busqueda.simple').'">',
                'link_end' => '</a>'
            ])
        !!}
        {!! __('busqueda.simple.links.todos', [
                'link_start' => '<a class="ml-5 btn btn-primary" href="'.route('busqueda.simple', ['query' => '%']).'">',
                'link_end' => '</a>'
            ])
        !!}
    </div>
</div>
