@if(!$resultados->isEmpty())
    <nav class="mt-5 pagination d-flex justify-content-center">
        <div>
            {{ $resultados->onEachSide(5)->links() }}
        </div>
    </nav>
    <div class="pagination d-flex justify-content-center">
        Mostrando de {{ $resultados->firstItem() }} a {{ $resultados->lastItem() }}
        de un total de {{ $resultados->total() }} elementos.
    </div>
@endif
