<div class="list-data">
    <div class="media">
    <span class="align-self-center mr-3 {{ __('busqueda.tipo_de_elemento.'.$item->modo.'.icon') }} fa-2x"></span>
    <div class="media-body">
        <div class="row">
            <div class="col-md-6">
                <h6>
                    {{$item->tipo}}
                    @if ($item->modo == 'curso')
                        @if (isset($item->nivel) && ($item->nivel != 'Otros'))
                            de {{$item->nivel}}
                        @endif
                        @isset($item->modalidad)
                            - {{$item->modalidad}}
                        @endisset
                    @endif
                </h6>
            </div>
            <div class="text-right col-md-6">
                <h6>{{ __('busqueda.dependencia_institucional', ['dependencia' => $item->dependencia_institucional]) }}</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <h5>{{$item->nombre}}</h5>
                @isset($item->descripcion)
                    <p>{{ str_limit($item->descripcion, $limit = setting('search.description_word_limit') ?: 200, $end = '...') }}</p>
                @endisset
            </div>
            <div class="text-right col-md-3">
                <a class="btn btn-sm btn-primary"
                    href="{{ route('busqueda.item', ['modo' => $item->modo, 'id' => $item->id]) }}">
                        {{ __('busqueda.ver_mas_informacion') }}
                </a>
                @if ($item->archivos_adjuntos)
                    <a class="btn btn-sm btn-dark mt-2"
                        href="{{ route('busqueda.item', ['modo' => $item->modo, 'id' => $item->id]) }}">
                            {{ __('busqueda.descargar_adjuntos') }}
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>
