@if ($item->responsables_nombres)
    <div class="item-detail-element responsables">
        <span class="icon fas fa-users"></span>
        <span class="label">{{ __('item.labels.'.$modo.'.responsables_nombres') }}</span>
        <span class="value">{{$item->responsables_nombres}}</span>
        @if ($item->responsables_detalles)
            <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#responsables_detalles" role="button" aria-expanded="false" aria-controls="docentes_formacion">
                {{ __('item.labels.'.$modo.'.responsables_expand') }}
            </a>
            <div class="collapse mt-2 item-detail-collapse responsables_detalles" id="responsables_detalles">
                <div class="card card-body">
                    <h5 class="card-title">{{ __('item.labels.'.$modo.'.responsables_detalles') }}</h5>
                    <p class="card-text">
                        {{$item->responsables_detalles}}
                    </p>
                </div>
            </div>
        @endif
    </div>
@endif

@if ($item->tipo_de_articulacion)
    <div class="item-detail-element articulacion">
        <span class="icon fas fa-puzzle-piece"></span>
        <span class="label">{{ __('item.labels.'.$modo.'.tipo_de_articulacion') }}</span>
        <span class="value">{{$item->tipo_de_articulacion->nombre}}</span>
        @if ($item->articulacion_detalles)
            <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#articulacion_detalles" role="button" aria-expanded="false" aria-controls="docentes_formacion">
                {{ __('item.labels.'.$modo.'.articulacion_expand') }}
            </a>
            <div class="collapse mt-2 item-detail-collapse articulacion_detalles" id="articulacion_detalles">
                <div class="card card-body">
                    <h5 class="card-title">{{ __('item.labels.'.$modo.'.articulacion_detalles') }}</h5>
                    <p class="card-text">
                        {{$item->articulacion_detalles}}
                    </p>
                </div>
            </div>
        @endif
    </div>
@endif

@if ($item->has_adjuntos())
    <div class="mt-3 adjuntos">
        @foreach ($item->adjuntos() as $adjunto)
        <a class="btn btn-dark btn-sm"
            id="download_link"
            href='{{Storage::disk(config('voyager.storage.disk'))->url($adjunto->download_link)}}'
            target="_blank">
            {{ __('item.labels.'.$modo.'.archivo_adjunto', ['nombre' => $adjunto->original_name]) }}
        </a>
        @endforeach
    </div>
@endif

@include('resultados/item_'.$modo.'_detalles')

@if ($item->contacto_nombre || $item->contacto_telefono || $item->contacto_email)
    <div class="item-detail-element contacto mt-4">
        <span class="icon fas fa-user"></span>
        <span class="label">{{ __('item.labels.'.$modo.'.contacto_nombre')}}</span>
        @if ($item->contacto_nombre)
            <span class="value">{{$item->contacto_nombre}}</span>
        @endif
        @if ($item->contacto_telefono)
            <span class="icon fas fa-phone"></span>
            <span class="value">{{$item->contacto_telefono}}</span>
        @endif
        @if ($item->contacto_email)
            <span class="icon fas fa-envelope"></span>
            <span class="value">{{$item->contacto_email}}</span>
        @endif
    </div>
@endif
