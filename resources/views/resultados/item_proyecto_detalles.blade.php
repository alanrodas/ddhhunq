<div class="item-detail-element mas-informacion">
    <span class="icon fas fa-plus"></span>
    <span class="label">{{ __('item.labels.'.$modo.'.mas_informacion') }}</span>
</div>
<div class="mas-informacion-buttons">
    @isset ($item->destinatarios)
        <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#estrategia_metodologica" role="button" aria-expanded="false" aria-controls="estrategia_metodologica">
            {{ __('item.labels.'.$modo.'.destinatarios') }}
        </a>
    @endisset
</div>

<div class="mas-informacion-boxes">
    @isset ($item->destinatarios)
        <div class="collapse item-detail-collapse estrategia_metodologica" id="estrategia_metodologica">
            <div class="card card-body">
                <h5 class="card-title">{{ __('item.labels.'.$modo.'.destinatarios_detalles') }}</h5>
                <p class="card-text">
                    {{$item->destinatarios}}
                </p>
            </div>
        </div>
    @endisset
</div>
