<div class="extra-data">
    @if ($item->editorial)
        <div class="item-detail-element editorial">
            <span class="icon fas fa-book-reader"></span>
            <span class="label">{{ __('item.labels.'.$modo.'.editorial') }}</span>
            <span class="value">{{$item->editorial}}</span>
        </div>
    @endif

    @if ($item->anho)
        <div class="item-detail-element anho">
            <span class="icon fas fa-calendar-alt"></span>
            <span class="label">{{ __('item.labels.'.$modo.'.anho') }}</span>
            <span class="value">{{$item->anho}}</span>
        </div>
    @endif

    @if ($item->ciudad)
        <div class="item-detail-element ciudad">
            <span class="icon fas fa-map-marked-alt"></span>
            <span class="label">{{ __('item.labels.'.$modo.'.ciudad') }}</span>
            <span class="value">{{$item->ciudad}}</span>
        </div>
    @endif

    @if ($item->serie)
        <div class="item-detail-element serie">
            <span class="icon fas fa-layer-group"></span>
            <span class="label">{{  __('item.labels.'.$modo.'.serie') }}</span>
            <span class="value">{{$item->serie}}</span>
        </div>
    @endif

    @if ($item->edicion)
        <div class="item-detail-element edicion">
            <span class="icon fas fa-bookmark"></span>
            <span class="label">{{  __('item.labels.'.$modo.'.edicion') }}</span>
            <span class="value">{{$item->edicion}}</span>
        </div>
    @endif

    @if ($item->isbn)
        <div class="item-detail-element isbn">
            <span class="icon fas fa-book"></span>
            <span class="label">{{  __('item.labels.'.$modo.'.isbn') }}</span>
            <span class="value">{{$item->isbn}}</span>
        </div>
    @endif
</div>
