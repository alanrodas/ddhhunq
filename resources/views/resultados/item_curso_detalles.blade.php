<div class="extra-data">
    @if($item->fecha_de_inicio || $item->fecha_de_fin)
        <div class="item-detail-element fechas">
            <span class="icon fas fa-calendar-alt"></span>
            @if($item->fecha_de_inicio)
                <span class="label">{{ __('item.labels.'.$modo.'.fecha_de_inicio') }}</span>
                <span class="value">{{$item->fecha_de_inicio->format('d/m/Y')}}</span>
            @endif
            @if($item->fecha_de_fin)
                <span class="label">{{ __('item.labels.'.$modo.'.fecha_de_fin') }}</span>
                <span class="value">{{$item->fecha_de_fin->format('d/m/Y')}}</span>
            @endif
        </div>
    @endif
    <div class="item-detail-element arancel">
        <span class="icon fas fa-hand-holding-usd"></span>
        @if ($item->arancelado)
            <span class="value">{{ __('item.labels.'.$modo.'.arancelado') }}</span>
        @else
            <span class="value">{{ __('item.labels.'.$modo.'.no_arancelado') }}</span>
        @endif
    </div>

    @if ($item->tipo_de_intensidad)
        <div class="item-detail-element intensidad">
            <span class="icon fas fa-tachometer-alt"></span>
            <span class="label">{{ __('item.labels.'.$modo.'.intensidad') }}</span>
            <span class="value">{{$item->tipo_de_intensidad->nombre}}</span>
        </div>
    @endif

    @if ($item->tipo_de_regularidad)
        <div class="item-detail-element regularidad">
            <span class="icon fas fa-clock"></span>
            <span class="label">{{ __('item.labels.'.$modo.'.regularidad') }}</span>
            <span class="value">{{$item->tipo_de_regularidad->nombre}}</span>
        </div>
    @endif

    @if ($item->tipo_de_nivel)
        <div class="item-detail-element nivel">
            <span class="icon fas fa-book-reader"></span>
            <span class="label">{{ __('item.labels.'.$modo.'.nivel') }}</span>
            <span class="value">{{$item->tipo_de_nivel->nombre}}</span>
        </div>
    @endif

    @if ($item->tipo_de_curricularidad)
        <div class="item-detail-element curricularidad">
            <span class="icon fas fa-calendar-alt"></span>
            <span class="label">{{  __('item.labels.'.$modo.'.curricularidad') }}</span>
            <span class="value">{{$item->tipo_de_curricularidad->nombre}}</span>
        </div>
    @endif
</div>

<div class="item-detail-element mas-informacion">
    <span class="icon fas fa-plus"></span>
    <span class="label">{{ __('item.labels.'.$modo.'.mas_informacion') }}</span>
</div>
<div class="mas-informacion-buttons">
    @isset ($item->estrategia_metodologica)
        <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#estrategia_metodologica" role="button" aria-expanded="false" aria-controls="estrategia_metodologica">
            {{ __('item.labels.'.$modo.'.estrategia_metodologica') }}
        </a>
    @endisset
    @isset ($item->condiciones)
        <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#condiciones" role="button" aria-expanded="false" aria-controls="condiciones">
            {{__('item.labels.'.$modo.'.condiciones') }}
        </a>
    @endisset
    @isset ($item->otros)
        <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#otros" role="button" aria-expanded="false" aria-controls="otros">
            {{ __('item.labels.'.$modo.'.otros') }}
        </a>
    @endisset
</div>

<div class="mas-informacion-boxes">
    @isset ($item->estrategia_metodologica)
        <div class="collapse item-detail-collapse estrategia_metodologica" id="estrategia_metodologica">
            <div class="card card-body">
                <h5 class="card-title">{{ __('item.labels.'.$modo.'.estrategia_metodologica_detalles') }}</h5>
                <p class="card-text">
                    {{$item->estrategia_metodologica}}
                </p>
            </div>
        </div>
    @endisset
    @isset ($item->condiciones)
        <div class="collapse item-detail-collapse condiciones" id="condiciones">
            <div class="card card-body">
                <h5 class="card-title">{{ __('item.labels.'.$modo.'.condiciones_detalles') }}</h5>
                <p class="card-text">
                    {{$item->condiciones}}
                </p>
            </div>
        </div>
    @endisset
    @isset ($item->otros)
        <div class="collapse item-detail-collapse otros" id="otros">
            <div class="card card-body">
                <h5 class="card-title">{{ __('item.labels.'.$modo.'.otros_detalles') }}</h5>
                <p class="card-text">
                    {{$item->otros}}
                </p>
            </div>
        </div>
    @endisset
</div>
