@isset($resultados)
    <div class="results-container">
        <div class="results">
            @if($resultados->isEmpty())
                <p class="no-results">
                    {!! __('busqueda.sin_resultados') !!}
                </p>
            @else
                @php ($inst = $resultados[0]->institucion)
                <div class="institucion-list-result-container">
                <h2>{{ $resultados[0]->institucion }}</h2>
                @foreach ($resultados as $item)
                    @if ($item->institucion != $inst)
                        </div>
                        <div class="institucion-list-result-container">
                        <h2>{{ $item->institucion }}</h2>
                    @endif

                    @include('resultados/lista_resultados_item')

                    @php ($inst = $item->institucion)
                @endforeach
                </div>
            @endif
        </div>
    </div>

    @include('resultados/paginacion')

@endisset
@empty($resultados)
    <div class="empty-result-list">
@endempty
