@extends('master')

@section('content')
    <div class="item-detail">

        {{-- Check if the user is coming from a search --}}
        @php ($from_search = parse_url(url()->previous())['path'] != '/'.request()->path())
        <div class="heading d-md-flex justify-content-end">
            <div class="titles d-grow">
                <h3 class="institucion">{{$item->institucion->nombre}}</h3>
                <h5 class="dependencia_institucional">{{$item->dependencia_institucional->nombre}}</h5>
            </div>
            @if ($from_search)
                <div class="go-back d-flex align-items-center">
                    <a class="btn btn-primary btn-lg" href="{{ url()->previous() }}">
                        <span class="fas fa-arrow-circle-left"></span>
                        {{ __('item.volver_al_listado') }}
                    </a>
                </div>
            @endif
        </div>

        @if($modo == 'curso')
            <h5 class="tipo">
            {{$item->tipo_de_curso->nombre}}
            @if (isset($item->tipo_de_nivel) && ($item->tipo_de_nivel->nombre !== 'Otros'))
                de {{$item->tipo_de_nivel->nombre}}
            @endif
            @isset($item->tipo_de_modalidad)
                - {{$item->tipo_de_modalidad->nombre}}
            @endisset
            </h5>
        @elseif ($modo == 'proyecto')
            <h5 class="tipo">{{$item->tipo_de_proyecto->nombre}}</h5>
        @elseif ($modo == 'publicacion')
            <h5 class="tipo">{{$item->tipo_de_publicacion->nombre}}</h5>
        @endif
        <h2 class="mt-0 nombre">{{$item->nombre}}</h2>
        <p>{{$item->descripcion}}</p>
        @include('resultados/item_detalles')
    </div>
@endsection
