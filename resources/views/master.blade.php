<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ setting('site.title') }}</title>

        <meta name="description" content="{{ setting('site.description') }}">
        <meta name="keywords" content="{{ setting('site.keywords') }}">
        <meta name="author" content="{{ setting('site.author') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <header class="logo-header">
            <div class="d-flex justify-content-around container">
                <div class="logo d-none d-md-block">
                    <a href="http://ddhh.unq.edu.ar" target="__blank"><img class="logo-ddhh" src="/img/logo_ddhh.png"></a>
                </div>

                <div class="title d-grow">
                    <h1 class="site-title">{{ setting('site.title') }}</h1>
                </div>

                <div class="logo d-none d-md-block">
                    <a href="http://unq.edu.ar" target="__blank"><img class="logo-unq" src="/img/logo_unqui.png"></a>
                </div>
            </div>
        </header>

        {!! menu('frontend', 'menues/bootstraped_menu') !!}

        <main class="content" id="app">
            <div class="container main-container">
                @yield('content')
            </div>
        </main>

        <footer class="navbar navbar-dark bg-dark">
            <span class="navbar-text">
                {!! setting('site.copyright') !!}
            </span>
        </footer>

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
