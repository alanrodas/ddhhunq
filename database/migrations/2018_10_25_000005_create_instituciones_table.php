<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstitucionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('instituciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
			$table->mediumText('direccion')->nullable();
            $table->mediumText('codigo_postal')->nullable();
            $table->mediumText('telefono')->nullable();
            $table->mediumText('email')->nullable();
            $table->string('logo')->nullable();
            $table->smallInteger('orden')->unsigned()->index('ORDEN');
			$table->integer('pais_id')->unsigned();
			$table->integer('region_id')->unsigned();
			$table->integer('provincia_id')->unsigned();
            $table->integer('tipo_de_institucion_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();

            $table->foreign('pais_id', 'instituciones_paises_FK')
                ->references('id')->on('paises')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('region_id', 'instituciones_regiones_FK')
                ->references('id')->on('regiones')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('provincia_id', 'instituciones_provincias_FK')
                ->references('id')->on('provincias')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tipo_de_institucion_id', 'instituciones_tipos_de_instituciones_FK')
                ->references('id')->on('tipos_de_instituciones')
                ->onDelete('cascade')
                ->onUpdate('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('instituciones');
	}

}
