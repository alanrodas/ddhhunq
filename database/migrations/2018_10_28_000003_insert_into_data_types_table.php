<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoDataTypesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('data_types')->insert([
			[
				"id" => 1,
				"name" => "users",
				"slug" => "users",
				"display_name_singular" => "Usuario",
				"display_name_plural" => "Usuarios",
                "description" => "Los usuarios registrados en la aplicación.",
				"icon" => "fa fa-user",
                "details" => "{\"order_column\":null,\"order_display_column\":null}",
                "server_side" => 0,
				"model_name" => "TCG\Voyager\Models\User",
				"policy_name" => "TCG\Voyager\Policies\UserPolicy",
				"controller" => NULL,
				"generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
            [
                "id" => 2,
                "name" => "roles",
                "slug" => "roles",
                "display_name_singular" => "Rol",
                "display_name_plural" => "Roles",
                "description" => "Los roles de usuario registrados en la aplicación.",
                "icon" => "fa fa-lock",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 0,
                "model_name" => "TCG\Voyager\Models\Role",
                "policy_name" => NULL,
                "controller" => NULL,
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
			[
				"id" => 3,
				"name" => "menus",
				"slug" => "menus",
				"display_name_singular" => "Menu",
				"display_name_plural" => "Menus",
                "description" => "Los menues registrados en la aplicación.",
				"icon" => "fa fa-list",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 0,
				"model_name" => "TCG\Voyager\Models\Menu",
				"policy_name" => NULL,
				"controller" => NULL,
				"generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
			[
				"id" => 4,
				"name" => "paises",
				"slug" => "paises",
				"display_name_singular" => "País",
				"display_name_plural" => "Paises",
                "description" => "Los paises registrados en la registrados.",
				"icon" => "fa fa-globe-americas",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
				"model_name" => "App\Models\Locaciones\Pais",
				"policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
				"generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
			[
				"id" => 5,
				"name" => "regiones",
				"slug" => "regiones",
				"display_name_singular" => "Región",
				"display_name_plural" => "Regiones",
                "description" => "Las regiones registradas en la aplicación.",
                "icon" => "fa fa-globe",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
				"model_name" => "App\Models\Locaciones\Region",
				"policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
				"generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
			[
				"id" => 6,
				"name" => "provincias",
				"slug" => "provincias",
				"display_name_singular" => "Provincia",
				"display_name_plural" => "Provincias",
                "description" => "Las provincias registradas en la aplicación.",
                "icon" => "fa fa-map-marker-alt",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
				"model_name" => "App\Models\Locaciones\Provincia",
				"policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
				"generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
            [
                "id" => 7,
                "name" => "tipos_de_instituciones",
                "slug" => "tipos-de-instituciones",
                "display_name_singular" => "Tipos De Institución",
                "display_name_plural" => "Tipos De Instituciones",
                "description" => "Los diferentes posibles tipos que tiene una institución.",
                "icon" => "fa fa-university",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Locaciones\TipoDeInstitucion",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 8,
                "name" => "tipos_de_dependencias_institucionales",
                "slug" => "tipos-de-dep-institucionales",
                "display_name_singular" => "Tipo De Dependencia Institucional",
                "display_name_plural" => "Tipos De Dependencias Institucionales",
                "description" => "Los diferentes posibles tipos que tiene una dependencia institucional.",
                "icon" => "fa fa-building",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Locaciones\TipoDeDependenciaInstitucional",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 9,
                "name" => "instituciones",
                "slug" => "instituciones",
                "display_name_singular" => "Institución",
                "display_name_plural" => "Instituciones",
                "description" => "Las instituciones registradas en la aplicación.",
                "icon" => "fa fa-university",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Locaciones\Institucion",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
			[
				"id" => 10,
				"name" => "dependencias_institucionales",
				"slug" => "dependencias-institucionales",
				"display_name_singular" => "Dependencia Institucional",
				"display_name_plural" => "Dependencias Institucionales",
                "description" => "Las dependencias institucionales registradas en la aplicación.",
                "icon" => "fa fa-building",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Locaciones\DependenciaInstitucional",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
            [
                "id" => 11,
                "name" => "tipos_de_publicaciones",
                "slug" => "tipos-de-publicaciones",
                "display_name_singular" => "Tipo De Publicación",
                "display_name_plural" => "Tipos De Publicaciones",
                "description" => "Los distintos posibles tipos de publicación.",
                "icon" => "fa fa-page-alt",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Publicaciones\TipoDePublicacion",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 12,
                "name" => "publicaciones",
                "slug" => "publicaciones",
                "display_name_singular" => "Publicación",
                "display_name_plural" => "Publicaciones",
                "description" => "Las publicaciones registradas en la aplicación.",
                "icon" => "fa fa-page-alt",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Publicaciones\Publicacion",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerVerifiableController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 13,
                "name" => "tipos_de_proyectos",
                "slug" => "tipos-de-proyectos",
                "display_name_singular" => "Tipo De Proyecto",
                "display_name_plural" => "Tipos De Proyectos",
                "description" => "Los diferentes posibles tipos que tiene un proyecto.",
                "icon" => "fa fa-briefcase",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Proyectos\TipoDeProyecto",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
			[
				"id" => 14,
				"name" => "tipos_de_articulaciones",
				"slug" => "tipos-de-articulaciones",
				"display_name_singular" => "Tipo De Articulación",
				"display_name_plural" => "Tipos De Articulaciones",
                "description" => "Los diferentes posibles tipos que tiene una articulación.",
                "icon" => "fa fa-puzzle",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Articulaciones\TipoDeArticulacion",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
            [
                "id" => 15,
                "name" => "proyectos",
                "slug" => "proyectos",
                "display_name_singular" => "Proyecto",
                "display_name_plural" => "Proyectos",
                "description" => "Los proyectos registrados en la aplicación.",
                "icon" => "fa fa-briefcase",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Proyectos\Proyecto",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerVerifiableController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 16,
                "name" => "tipos_de_cursos",
                "slug" => "tipos-de-cursos",
                "display_name_singular" => "Tipo De Curso",
                "display_name_plural" => "Tipos De Cursos",
                "description" => "Los diferentes posibles tipos que tiene un curso.",
                "icon" => "fa fa-graduation-cap",
                "server_side" => 1,
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "model_name" => "App\Models\Cursos\TipoDeCurso",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
			[
				"id" => 17,
				"name" => "tipos_de_curricularidades",
				"slug" => "tipos-de-curricularidades",
				"display_name_singular" => "Tipo De Curricularidad",
				"display_name_plural" => "Tipos De Curricularidades",
                "description" => "Los diferentes posibles tipos de curricularidad que tiene un curso.",
                "icon" => "fa fa-calendar-alt",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Cursos\TipoDeCurricularidad",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
			[
				"id" => 18,
				"name" => "tipos_de_intensidades",
				"slug" => "tipos-de-intensidades",
				"display_name_singular" => "Tipo De Intensidad",
				"display_name_plural" => "Tipos De Intensidades",
                "description" => "Los diferentes posibles tipos de intensidad que tiene un curso.",
                "icon" => "fa fa-podometer",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Cursos\TipoDeIntensidad",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
			[
				"id" => 19,
				"name" => "tipos_de_modalidades",
				"slug" => "tipos-de-modalidades",
				"display_name_singular" => "Tipo De Modalidad",
				"display_name_plural" => "Tipos De Modalidades",
                "description" => "Los diferentes posibles tipos de modalidad que tiene un curso.",
                "icon" => "fa fa-cog",
                "server_side" => 1,
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "model_name" => "App\Models\Cursos\TipoDeModalidad",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
            [
                "id" => 20,
                "name" => "tipos_de_regularidades",
                "slug" => "tipos-de-regularidades",
                "display_name_singular" => "Tipo De Regularidad",
                "display_name_plural" => "Tipos De Regularidades",
                "description" => "Los diferentes posibles tipos de regularidad que tiene un curso.",
                "icon" => "fa fa-cog",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Cursos\TipoDeRegularidad",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
			[
				"id" => 21,
				"name" => "tipos_de_niveles",
				"slug" => "tipos-de-niveles",
				"display_name_singular" => "Tipo De Nivel",
				"display_name_plural" => "Tipos De Niveles",
                "description" => "Los diferentes posibles tipos de nivel que tiene un curso.",
                "icon" => "fa fa-cog",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Cursos\TipoDeNivel",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
			[
				"id" => 22,
				"name" => "cursos",
				"slug" => "cursos",
				"display_name_singular" => "Curso",
				"display_name_plural" => "Cursos",
                "description" => "Los cursos registrados en la aplicación",
                "icon" => "fa fa-graduation-cap",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Cursos\Curso",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerVerifiableController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
            [
                "id" => 23,
                "name" => "paginas",
                "slug" => "paginas",
                "display_name_singular" => "Página",
                "display_name_plural" => "Páginas",
                "description" => "Las páginas estáticas registradas en la aplicación",
                "icon" => "fa fa-papers",
                "details" => json_encode([
                    "order_column" => null,
                    "order_display_column" => null
                ]),
                "server_side" => 1,
                "model_name" => "App\Models\Paginas\Pagina",
                "policy_name" => NULL,
                "controller" => '\App\Http\Controllers\Voyager\VoyagerBaseController',
                "generate_permissions" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ]
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('data_types')
            ->whereIn('id', [
                     1,  // Usuarios
                     2,  // Roles
                     3,  // Menus
                     4,  // Paises
                     5,  // Regiones
                     6,  // Provincias
                     7,  // Tipo de Institución
                     8,  // Tipo de Dependencias Institucionesles
                     9,  // Instituciones
                    10,  // Dispositivos Institucionales
                    11,  // Tipos de Publicaciones
                    12,  // Publicaciones
                    13,  // Tipos de Proyectos
                    14,  // Tipos de Articulaciones
                    15,  // Proyectos
                    16,  // Tipos de Cursos
                    17,  // Tipos de Curricularidades
                    18,  // Tipos de Intensidades
                    19,  // Tipos de Modalidades
                    20,  // Tipos de Regularidades
                    21,  // Tipos de Niveles
                    22,  // Cursos
                    23   // Páginas
            ])
		    ->delete();
	}

}
