<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoProvinciasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('provincias')->insert([
			[
				"id" => 1,
                "nombre" => "Buenos Aires",
                "pais_id" => 1,
				"region_id" => 1,
                "orden" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 2,
                "nombre" => "Entre Ríos",
                "pais_id" => 1,
				"region_id" => 1,
                "orden" => 2,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 3,
                "nombre" => "Santa Fé",
                "pais_id" => 1,
				"region_id" => 1,
                "orden" => 3,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 4,
                "nombre" => "Tucumán",
                "pais_id" => 1,
				"region_id" => 2,
                "orden" => 4,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 5,
                "nombre" => "Santiago del Estero",
                "pais_id" => 1,
				"region_id" => 2,
                "orden" => 5,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 6,
                "nombre" => "Catamarca",
                "pais_id" => 1,
				"region_id" => 2,
                "orden" => 6,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 7,
                "nombre" => "Jujuy",
                "pais_id" => 1,
				"region_id" => 2,
                "orden" => 7,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 8,
                "nombre" => "Salta",
                "pais_id" => 1,
				"region_id" => 2,
                "orden" => 8,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 9,
                "nombre" => "La Rioja",
                "pais_id" => 1,
				"region_id" => 2,
                "orden" => 9,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 10,
                "nombre" => "Misiones",
                "pais_id" => 1,
				"region_id" => 3,
                "orden" => 10,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 11,
                "nombre" => "Chaco",
                "pais_id" => 1,
				"region_id" => 3,
                "orden" => 11,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 12,
                "nombre" => "Corrientes",
                "pais_id" => 1,
				"region_id" => 3,
                "orden" => 12,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 13,
                "nombre" => "Córdoba",
                "pais_id" => 1,
				"region_id" => 5,
                "orden" => 13,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 14,
                "nombre" => "Mendoza",
                "pais_id" => 1,
				"region_id" => 5,
                "orden" => 14,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 15,
                "nombre" => "San Luis",
                "pais_id" => 1,
				"region_id" => 5,
                "orden" => 15,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 16,
                "nombre" => "San Juan",
                "pais_id" => 1,
				"region_id" => 5,
                "orden" => 16,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 17,
                "nombre" => "La Pampa",
                "pais_id" => 1,
				"region_id" => 5,
                "orden" => 17,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 18,
                "nombre" => "Ciudad Autónoma de Buenos Aires",
                "pais_id" => 1,
				"region_id" => 7,
                "orden" => 18,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 19,
                "nombre" => "Buenos Aires",
                "pais_id" => 1,
				"region_id" => 7,
                "orden" => 19,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 20,
                "nombre" => "Buenos Aires",
                "pais_id" => 1,
				"region_id" => 6,
                "orden" => 20,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 21,
                "nombre" => "Chubut",
                "pais_id" => 1,
				"region_id" => 4,
                "orden" => 21,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 22,
                "nombre" => "Santa Cruz",
                "pais_id" => 1,
				"region_id" => 4,
                "orden" => 22,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 23,
                "nombre" => "Río Negro",
                "pais_id" => 1,
				"region_id" => 4,
                "orden" => 23,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 24,
                "nombre" => "Tierra del Fuego",
                "pais_id" => 1,
				"region_id" => 4,
                "orden" => 24,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 25,
                "nombre" => "Neuquen",
                "pais_id" => 1,
				"region_id" => 4,
                "orden" => 25,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 26,
                "nombre" => "Formosa",
                "pais_id" => 1,
				"region_id" => 3,
                "orden" => 26,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			]
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('provincias')
            ->whereIn('id', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26])
		    ->delete();
	}

}
