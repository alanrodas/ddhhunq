<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDependenciasInstitucionalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dependencias_institucionales', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->text('articulacion')->nullable();
			$table->smallInteger('orden')->unsigned()->index('ORDEN');
			$table->integer('institucion_id')->unsigned();
            $table->integer('tipo_de_dependencia_institucional_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();

            $table->foreign('institucion_id', 'institucion_id_FK')
                ->references('id')->on('instituciones')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tipo_de_dependencia_institucional_id', 'dep_institucionales_tipos_de_dep_institucionales_FK')
                ->references('id')->on('tipos_de_dependencias_institucionales')
                ->onDelete('cascade')
                ->onUpdate('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dependencias_institucionales');
	}

}
