<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoPaisesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('paises')->insert([
			[
				"id" => 1,
				"nombre" => "Argentina",
                "bandera" => "paises/argentina.png",
                "continente" => "América del Sur",
                "orden" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			]

        ]);
    }
    
    

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('paises')
            ->whereIn("id", [1])
		    ->delete();
	}

}
