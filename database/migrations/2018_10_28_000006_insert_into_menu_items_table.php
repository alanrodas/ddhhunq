<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoMenuItemsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('menu_items')->insert([
			[
				"id" => 1100, "menu_id" => 1,
				"title" => "Inicio",
                "icon_class" => "fa fa-bell", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => "voyager.dashboard", "parameters" => NULL,
                "order" => 1, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			],
            [
                "id" => 1200, "menu_id" => 1,
                "title" => "Cursos",
                "icon_class" => "fa fa-graduation-cap", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 2, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
                    [
                        "id" => 1201, "menu_id" => 1,
                        "title" => "Cursos",
                        "icon_class" => "fa fa-graduation-cap", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.cursos.index", "parameters" => NULL,
                        "order" => 1, "parent_id" => 1200,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1202, "menu_id" => 1,
                        "title" => "Tipos De Cursos",
                        "icon_class" => "fa fa-graduation-cap", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-cursos.index", "parameters" => NULL,
                        "order" => 2, "parent_id" => 1200,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1203, "menu_id" => 1,
                        "title" => "Tipos De Curricularidades",
                        "icon_class" => "fa fa-calendar-alt", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-curricularidades.index", "parameters" => NULL,
                        "order" => 3, "parent_id" => 1200,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1204, "menu_id" => 1,
                        "title" => "Tipos De Intensidades",
                        "icon_class" => "fa fa-tachometer-alt", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-intensidades.index", "parameters" => NULL,
                        "order" => 4, "parent_id" => 1200,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1205, "menu_id" => 1,
                        "title" => "Tipos De Modalidades",
                        "icon_class" => "fa fa-th", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-modalidades.index", "parameters" => NULL,
                        "order" => 5, "parent_id" => 1200,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1206, "menu_id" => 1,
                        "title" => "Tipos De Regularidades",
                        "icon_class" => "fa fa-clock", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-regularidades.index", "parameters" => NULL,
                        "order" => 6, "parent_id" => 1200,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1207, "menu_id" => 1,
                        "title" => "Tipos De Niveles",
                        "icon_class" => "fa fa-book-reader", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-niveles.index", "parameters" => NULL,
                        "order" => 7, "parent_id" => 1200,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
            [
                "id" => 1300, "menu_id" => 1,
                "title" => "Proyectos",
                "icon_class" => "fa fa-briefcase", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 3, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
                    [
                        "id" => 1301, "menu_id" => 1,
                        "title" => "Proyectos",
                        "icon_class" => "fa fa-briefcase", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.proyectos.index", "parameters" => NULL,
                        "order" => 1, "parent_id" => 1300,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1302, "menu_id" => 1,
                        "title" => "Tipos De Proyectos",
                        "icon_class" => "fa fa-briefcase", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-proyectos.index", "parameters" => NULL,
                        "order" => 2, "parent_id" => 1300,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1303, "menu_id" => 1,
                        "title" => "Tipos De Articulaciones",
                        "icon_class" => "fa fa-puzzle-piece", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-articulaciones.index", "parameters" => NULL,
                        "order" => 3, "parent_id" => 1300,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
            [
                "id" => 1400, "menu_id" => 1,
                "title" => "Publicaciones",
                "icon_class" => "fa fa-file-alt", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 4, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
                    [
                        "id" => 1401, "menu_id" => 1,
                        "title" => "Publicaciones",
                        "icon_class" => "fa fa-file-alt", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.publicaciones.index", "parameters" => NULL,
                        "order" => 1, "parent_id" => 1400,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1402, "menu_id" => 1,
                        "title" => "Tipos De Publicaciones",
                        "icon_class" => "fa fa-file-alt", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-publicaciones.index", "parameters" => NULL,
                        "order" => 2, "parent_id" => 1400,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
            [
                "id" => 1500, "menu_id" => 1,
                "title" => "Locaciones",
                "icon_class" => "fa fa-map-marked-alt", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 5, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
                    [
                        "id" => 1501, "menu_id" => 1,
                        "title" => "Instituciones",
                        "icon_class" => "fa fa-university", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.instituciones.index", "parameters" => NULL,
                        "order" => 1, "parent_id" => 1500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1502, "menu_id" => 1,
                        "title" => "Dep. Institucionales",
                        "icon_class" => "fa fa-building", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.dependencias-institucionales.index", "parameters" => NULL,
                        "order" => 2, "parent_id" => 1500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1503, "menu_id" => 1,
                        "title" => "Paises",
                        "icon_class" => "fas fa-globe-americas", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.paises.index", "parameters" => NULL,
                        "order" => 3, "parent_id" => 1500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1504, "menu_id" => 1,
                        "title" => "Regiones",
                        "icon_class" => "fa fa-globe", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.regiones.index", "parameters" => NULL,
                        "order" => 4, "parent_id" => 1500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1505, "menu_id" => 1,
                        "title" => "Provincias",
                        "icon_class" => "fa fa-map-marker-alt", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.provincias.index", "parameters" => NULL,
                        "order" => 5, "parent_id" => 1500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1506, "menu_id" => 1,
                        "title" => "Tipos De Instituciones",
                        "icon_class" => "fa fa-university", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-instituciones.index", "parameters" => NULL,
                        "order" => 6, "parent_id" => 1500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],

                    [
                        "id" => 1507, "menu_id" => 1,
                        "title" => "Tipos De Dep. Institucional",
                        "icon_class" => "fa fa-building", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-dep-institucionales.index", "parameters" => NULL,
                        "order" => 7, "parent_id" => 1500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
            [
                "id" => 1600, "menu_id" => 1,
                "title" => "Páginas",
                "icon_class" => "fa fa-file-signature", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => "voyager.paginas.index", "parameters" => NULL,
                "order" => 6, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 1700, "menu_id" => 1,
                "title" => "Administración",
                "icon_class" => "fa fa-users-cog", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 7, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
                    [
                        "id" => 1701, "menu_id" => 1,
                        "title" => "Usuarios",
                        "icon_class" => "fa fa-user", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.users.index", "parameters" => NULL,
                        "order" => 1, "parent_id" => 1700,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1702, "menu_id" => 1,
                        "title" => "Roles",
                        "icon_class" => "fa fa-lock", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.roles.index", "parameters" => NULL,
                        "order" => 2, "parent_id" => 1700,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1703, "menu_id" => 1,
                        "title" => "Archivos",
                        "icon_class" => "fa fa-image", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.media.index", "parameters" => NULL,
                        "order" => 3, "parent_id" => 1700,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
            [
                "id" => 1800, "menu_id" => 1,
                "title" => "Herramientas",
                "icon_class" => "fa fa-hammer", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 8, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
                    [
                        "id" => 1801, "menu_id" => 1,
                        "title" => "Editor de Menúes",
                        "icon_class" => "fa fa-list", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.menus.index", "parameters" => NULL,
                        "order" => 1, "parent_id" => 1800,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1802, "menu_id" => 1,
                        "title" => "Base de Datos",
                        "icon_class" => "fa fa-database", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.database.index", "parameters" => NULL,
                        "order" => 2, "parent_id" => 1800,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp

                    ],
                    [
                        "id" => 1803, "menu_id" => 1,
                        "title" => "Compass",
                        "icon_class" => "fa fa-compass", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.compass.index", "parameters" => NULL,
                        "order" => 3, "parent_id" => 1800,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1804, "menu_id" => 1,
                        "title" => "BREAD",
                        "icon_class" => "voyager-bread", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.bread.index", "parameters" => NULL,
                        "order" => 4, "parent_id" => 1800,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1805, "menu_id" => 1,
                        "title" => "Hooks",
                        "icon_class" => "voyager-hook", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.hooks", "parameters" => NULL,
                        "order" => 5, "parent_id" => 1800,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 1806, "menu_id" => 1,
                        "title" => "Configuración",
                        "icon_class" => "fa fa-cog", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.settings.index", "parameters" => NULL,
                        "order" => 6, "parent_id" => 1800,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],




            [
                "id" => 2000, "menu_id" => 2,
                "title" => "Inicio",
                "icon_class" => "fa fa-bell", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => "voyager.dashboard", "parameters" => NULL,
                "order" => 1, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 2100, "menu_id" => 2,
                "title" => "Cursos",
                "icon_class" => "fa fa-graduation-cap", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => "voyager.cursos.index", "parameters" => NULL,
                "order" => 2, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 2200, "menu_id" => 2,
                "title" => "Proyectos",
                "icon_class" => "fa fa-briefcase", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => "voyager.proyectos.index", "parameters" => NULL,
                "order" => 3, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 2300, "menu_id" => 2,
                "title" => "Publicaciones",
                "icon_class" => "fa fa-file-alt", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => "voyager.publicaciones.index", "parameters" => NULL,
                "order" => 4, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 2400, "menu_id" => 2,
                "title" => "Páginas",
                "icon_class" => "fa fa-file-signature", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => "voyager.paginas.index", "parameters" => NULL,
                "order" => 5, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 2500, "menu_id" => 2,
                "title" => "Locaciones",
                "icon_class" => "fa fa-map-marked-alt", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 6, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
                    [
                        "id" => 2501, "menu_id" => 2,
                        "title" => "Instituciones",
                        "icon_class" => "fa fa-university", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.instituciones.index", "parameters" => NULL,
                        "order" => 1, "parent_id" => 2500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2502, "menu_id" => 2,
                        "title" => "Dep. Institucionales",
                        "icon_class" => "fa fa-building", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.dependencias-institucionales.index", "parameters" => NULL,
                        "order" => 2, "parent_id" => 2500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2503, "menu_id" => 2,
                        "title" => "Paises",
                        "icon_class" => "fas fa-globe-americas", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.paises.index", "parameters" => NULL,
                        "order" => 3, "parent_id" => 2500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2504, "menu_id" => 2,
                        "title" => "Regiones",
                        "icon_class" => "fa fa-globe", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.regiones.index", "parameters" => NULL,
                        "order" => 4, "parent_id" => 2500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2505, "menu_id" => 2,
                        "title" => "Provincias",
                        "icon_class" => "fa fa-map-marker-alt", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.provincias.index", "parameters" => NULL,
                        "order" => 5, "parent_id" => 2500,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
            [
                "id" => 2600, "menu_id" => 2,
                "title" => "Configuración",
                "icon_class" => "fa fa-cog", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 7, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
                    [
                        "id" => 1601, "menu_id" => 2,
                        "title" => "Tipos De Cursos",
                        "icon_class" => "fa fa-graduation-cap", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-cursos.index", "parameters" => NULL,
                        "order" => 1, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2602, "menu_id" => 2,
                        "title" => "Tipos De Proyectos",
                        "icon_class" => "fa fa-briefcase", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-proyectos.index", "parameters" => NULL,
                        "order" => 2, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2603, "menu_id" => 2,
                        "title" => "Tipos De Publicaciones",
                        "icon_class" => "fa fa-file-alt", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-publicaciones.index", "parameters" => NULL,
                        "order" => 3, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2604, "menu_id" => 2,
                        "title" => "Tipos De Curricularidades",
                        "icon_class" => "fa fa-calendar-alt", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-curricularidades.index", "parameters" => NULL,
                        "order" => 4, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2605, "menu_id" => 2,
                        "title" => "Tipos De Intensidades",
                        "icon_class" => "fa fa-tachometer-alt", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-intensidades.index", "parameters" => NULL,
                        "order" => 5, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2606, "menu_id" => 2,
                        "title" => "Tipos De Modalidades",
                        "icon_class" => "fa fa-th", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-modalidades.index", "parameters" => NULL,
                        "order" => 6, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2607, "menu_id" => 2,
                        "title" => "Tipos De Regularidades",
                        "icon_class" => "fa fa-clock", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-regularidades.index", "parameters" => NULL,
                        "order" => 7, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2608, "menu_id" => 2,
                        "title" => "Tipos De Niveles",
                        "icon_class" => "fa fa-book-reader", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-niveles.index", "parameters" => NULL,
                        "order" => 8, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2609, "menu_id" => 2,
                        "title" => "Tipos De Articulaciones",
                        "icon_class" => "fa fa-puzzle-piece", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-articulaciones.index", "parameters" => NULL,
                        "order" => 9, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2610, "menu_id" => 2,
                        "title" => "Tipos De Instituciones",
                        "icon_class" => "fa fa-university", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-instituciones.index", "parameters" => NULL,
                        "order" => 10, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],

                    [
                        "id" => 2611, "menu_id" => 2,
                        "title" => "Tipos De Dep. Institucional",
                        "icon_class" => "fa fa-building", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.tipos-de-dep-institucionales.index", "parameters" => NULL,
                        "order" => 11, "parent_id" => 2600,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
            [
                "id" => 2700, "menu_id" => 2,
                "title" => "Administración",
                "icon_class" => "fa fa-users-cog", "color" => NULL,
                "url" => "", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 8, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
                    [
                        "id" => 2701, "menu_id" => 2,
                        "title" => "Usuarios",
                        "icon_class" => "fa fa-user", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.users.index", "parameters" => NULL,
                        "order" => 1, "parent_id" => 2700,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],
                    [
                        "id" => 2702, "menu_id" => 2,
                        "title" => "Archivos",
                        "icon_class" => "fa fa-image", "color" => NULL,
                        "url" => "", "target" => "_self",
                        "route" => "voyager.media.index", "parameters" => NULL,
                        "order" => 2, "parent_id" => 2700,
                        "created_at" => $timestamp,
                        "updated_at" => $timestamp
                    ],



            [
                "id" => 3100, "menu_id" => 3,
                "title" => "Inicio",
                "icon_class" => NULL, "color" => NULL,
                "url" => "/", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 1, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 3200, "menu_id" => 3,
                "title" => "Búsqueda de Información",
                "icon_class" => NULL, "color" => NULL,
                "url" => "/busqueda", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 2, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ],
            [
                "id" => 3300, "menu_id" => 3,
                "title" => "Carga de Información",
                "icon_class" => NULL, "color" => NULL,
                "url" => "/carga", "target" => "_self",
                "route" => NULL, "parameters" => NULL,
                "order" => 3, "parent_id" => NULL,
                "created_at" => $timestamp,
                "updated_at" => $timestamp
            ]
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('menu_items')
            ->whereIn('id', [
                // Menu Admin
                     1,  2,  3,  4,  5,  6,  7,  8, 9,
                10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                30, 31, 32, 33, 34, 35
            ])
		    ->delete();
	}

}
