<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTiposDeCurricularidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipos_de_curricularidades', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
			$table->mediumText('descripcion')->nullable();
			$table->smallInteger('orden')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipos_de_curricularidades');
	}

}
