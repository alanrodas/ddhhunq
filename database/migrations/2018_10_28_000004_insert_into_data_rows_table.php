<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoDataRowsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('data_rows')->insert([

            //                       Users
            // ==================================================
			[
				"id" => 101, "data_type_id" => 1,
				"field" => "id",
				"type" => "number",
				"display_name" => "ID",
				"required" => 1,
				"browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
				"details" => NULL,
				"order" => 1
			],
			[
				"id" => 102, "data_type_id" => 1,
				"field" => "name",
				"type" => "text",
				"display_name" => "Nombre",
				"required" => 1,
				"browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
				"details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
				"order" => 2
			],
			[
				"id" => 103, "data_type_id" => 1,
				"field" => "email",
				"type" => "text",
				"display_name" => "Email",
				"required" => 1,
				"browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
				"details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255|email",
                        "messages" => [
                            "required" => "El email no puede estar vacío",
                            "max" => "El email no puede exceder los 255 caracteres",
                            "email" => "Debe proporcionar un correo electrónico válido"
                        ]
                    ]
                ]),
				"order" => 3
			],
            [
                "id" => 104, "data_type_id" => 1,
                "field" => "avatar",
				"type" => "image",
				"display_name" => "Imágen de Perfil",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 4
            ],
			[
				"id" => 105, "data_type_id" => 1,
				"field" => "password",
				"type" => "password",
				"display_name" => "Contraseña",
				"required" => 1,
                "browse" => 0, "read" => 0, "edit" => 1, "add" => 0, "delete" => 0,
				"details" => json_encode([
                    "validation" => [
                        "rule" => "required|min:6|max:30",
                        "messages" => [
                            "required" => "La contraseña no puede quedar vacía",
                            "min" => "La contraseña debe contener al menos 6 caracteres",
                            "max" => "La contraseña no puede contener más de 30 caracteres"
                        ]
                    ]
                ]),
				"order" => 5
			],
            [
                "id" => 106, "data_type_id" => 1,
                "field" => "role_id",
				"type" => "text",
				"display_name" => "Rol",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 107, "data_type_id" => 1,
                "field" => "user_belongsto_role_relationship",
				"type" => "relationship",
				"display_name" => "Rol",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "TCG\\Voyager\\Models\\Role",
                    "table"       => "roles",
                    "type"        => "belongsTo",
                    "column"      => "role_id",
                    "key"         => "id",
                    "label"       => "display_name",
                    "pivot_table" => "roles",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 7
            ],
            [
                "id" => 108, "data_type_id" => 1,
                "field" => "user_belongstomany_role_relationship",
				"type" => "relationship",
				"display_name" => "Roles",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "TCG\\Voyager\\Models\\Role",
                    "table"       => "roles",
                    "type"        => "belongsToMany",
                    "column"      => "id",
                    "key"         => "id",
                    "label"       => "display_name",
                    "pivot_table" => "user_roles",
                    "pivot"       => "1",
                    "taggable"    => "0"
                ]),
                "order" => 8
            ],
            [
                "id" => 109, "data_type_id" => 1,
                "field" => "settings",
				"type" => "textarea",
				"display_name" => "Configuración",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 9
            ],
			[
				"id" => 110, "data_type_id" => 1,
				"field" => "remember_token",
				"type" => "text",
				"display_name" => "Remember Token",
				"required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
				"details" => NULL,
				"order" => 10
			],
            [
                "id" => 111, "data_type_id" => 1,
                "field" => "email_verified_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Verificación de Email",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 11
            ],
			[
				"id" => 112, "data_type_id" => 1,
				"field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
				"required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
				"details" => NULL,
				"order" => 12
			],
			[
				"id" => 113, "data_type_id" => 1,
				"field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
				"required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
				"details" => NULL,
				"order" => 13
			],




            //                       Roles
            // ==================================================
            [
                "id" => 201, "data_type_id" => 2,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 202, "data_type_id" => 2,
                "field" => "name",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 203, "data_type_id" => 2,
                "field" => "display_name",
				"type" => "text",
				"display_name" => "Nombre para Mostrar",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 204, "data_type_id" => 2,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 205, "data_type_id" => 2,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],




            //                        Menus
            // ==================================================
			[
				"id" => 301, "data_type_id" => 3,
				"field" => "id",
				"type" => "number",
				"display_name" => "ID",
				"required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
				"details" => NULL,
				"order" => 1
			],
			[
				"id" => 302, "data_type_id" => 3,
				"field" => "name",
				"type" => "text",
				"display_name" => "Nombre",
				"required" => 1,
				"browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
				"details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
				"order" => 2
			],
			[
				"id" => 303, "data_type_id" => 3,
				"field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
				"required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
				"details" => NULL,
				"order" => 3
			],
			[
				"id" => 304, "data_type_id" => 3,
				"field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
				"required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
				"details" => NULL,
				"order" => 4
			],





            //                        País
            // ==================================================
            [
                "id" => 401, "data_type_id" => 4,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 402, "data_type_id" => 4,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 403, "data_type_id" => 4,
                "field" => "continente",
				"type" => "text",
				"display_name" => "Continente",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "El continente no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 3
            ],
            [
                "id" => 404, "data_type_id" => 8,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 405, "data_type_id" => 4,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 406, "data_type_id" => 4,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 407, "data_type_id" => 4,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],





            //                        Región
            // ==================================================
            [
                "id" => 501, "data_type_id" => 5,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 502, "data_type_id" => 5,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 503, "data_type_id" => 5,
                "field" => "pais_id",
				"type" => "text",
				"display_name" => "País ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar un país de la lista."
                        ]
                    ]
                ]),
                "order" => 3
            ],
            [
                "id" => 504, "data_type_id" => 5,
                "field" => "region_belongsto_pais_relationship",
				"type" => "relationship",
				"display_name" => "País",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\Pais",
                    "table"       => "paises",
                    "type"        => "belongsTo",
                    "column"      => "pais_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "paises",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 4
            ],
            [
                "id" => 505, "data_type_id" => 8,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 506, "data_type_id" => 5,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 507, "data_type_id" => 5,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],
            [
                "id" => 508, "data_type_id" => 5,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 8
            ],





            //                        Provincia
            // ==================================================
            [
                "id" => 601, "data_type_id" => 6,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 602, "data_type_id" => 6,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 603, "data_type_id" => 6,
                "field" => "pais_id",
				"type" => "text",
				"display_name" => "País ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar un pais de la lista."
                        ]
                    ]
                ]),
                "order" => 3
            ],
            [
                "id" => 604, "data_type_id" => 6,
                "field" => "provincia_belongsto_pais_relationship",
				"type" => "relationship",
				"display_name" => "País",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\Pais",
                    "table"       => "paises",
                    "type"        => "belongsTo",
                    "column"      => "pais_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "paises",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 4
            ],
            [
                "id" => 605, "data_type_id" => 6,
                "field" => "region_id",
				"type" => "text",
				"display_name" => "Región ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar una región de la lista."
                        ]
                    ]
                ]),
                "order" => 5
            ],
            [
                "id" => 606, "data_type_id" => 6,
                "field" => "provincia_belongsto_region_relationship",
				"type" => "relationship",
				"display_name" => "Región",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\Region",
                    "table"       => "regiones",
                    "type"        => "belongsTo",
                    "column"      => "region_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "regiones",
                    "pivot"       => "0",
                    "taggable"    => "0",
                    "filterColumn"  => "pais_id",
                    "dataAttribute" =>"pais_id"
                ]),
                "order" => 6
            ],
            [
                "id" => 607, "data_type_id" => 6,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],
            [
                "id" => 608, "data_type_id" => 6,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 8
            ],
            [
                "id" => 609, "data_type_id" => 6,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 9
            ],
            [
                "id" => 610, "data_type_id" => 6,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 10
            ],




            //                Tipos de Instituciones
            // ==================================================
            [
                "id" => 701, "data_type_id" => 7,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 702, "data_type_id" => 7,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 703, "data_type_id" => 7,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 704, "data_type_id" => 7,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 705, "data_type_id" => 7,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 706, "data_type_id" => 7,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 707, "data_type_id" => 7,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //       Tipos de Dependencias Institucionales
            // ==================================================
            [
                "id" => 801, "data_type_id" => 8,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 802, "data_type_id" => 8,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 803, "data_type_id" => 8,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 804, "data_type_id" => 8,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 805, "data_type_id" => 8,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 806, "data_type_id" => 8,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 807, "data_type_id" => 8,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //                      Instituciones
            // ==================================================
            [
                "id" => 901, "data_type_id" => 9,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 902, "data_type_id" => 9,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 903, "data_type_id" => 9,
                "field" => "tipo_de_institucion_id",
				"type" => "text",
				"display_name" => "Tipo de Institución ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar un tipo de institucion de la lista."
                        ]
                    ]
                ]),
                "order" => 3
            ],
            [
                "id" => 904, "data_type_id" => 9,
                "field" => "institucion_belongsto_tipo_de_institucion_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Institución",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\TipoDeInstitucion",
                    "table"       => "tipos_de_instituciones",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_institucion_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_instituciones",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 4
            ],
            [
                "id" => 905, "data_type_id" => 9,
                "field" => "direccion",
				"type" => "text",
				"display_name" => "Dirección",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "La dirección no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 5
            ],
            [
                "id" => 906, "data_type_id" => 9,
                "field" => "codigo_postal",
				"type" => "text",
				"display_name" => "Código Postal",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "El código postal no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 6
            ],
            [
                "id" => 907, "data_type_id" => 9,
                "field" => "telefono",
				"type" => "text",
				"display_name" => "Teléfono",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "El teléfono no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 7
            ],
            [
                "id" => 908, "data_type_id" => 9,
                "field" => "email",
				"type" => "text",
				"display_name" => "Email",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "El email no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 8
            ],
            [
                "id" => 909, "data_type_id" => 9,
                "field" => "pais_id",
				"type" => "text",
				"display_name" => "País ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar un país de la lista."
                        ]
                    ]
                ]),
                "order" => 9
            ],
            [
                "id" => 910, "data_type_id" => 9,
                "field" => "institucion_belongsto_pais_relationship",
				"type" => "relationship",
				"display_name" => "País",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\Pais",
                    "table"       => "paises",
                    "type"        => "belongsTo",
                    "column"      => "pais_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "paises",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 10
            ],
            [
                "id" => 911, "data_type_id" => 9,
                "field" => "region_id",
				"type" => "text",
				"display_name" => "Región ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar una región de la lista."
                        ]
                    ]
                ]),
                "order" => 11
            ],
            [
                "id" => 912, "data_type_id" => 9,
                "field" => "institucion_belongsto_region_relationship",
				"type" => "relationship",
				"display_name" => "Región",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\Region",
                    "table"       => "regiones",
                    "type"        => "belongsTo",
                    "column"      => "region_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "regiones",
                    "pivot"       => "0",
                    "taggable"    => "0",
                    "filterColumn"  => "pais_id",
                    "dataAttribute" => "pais_id"
                ]),
                "order" => 12
            ],
            [
                "id" => 913, "data_type_id" => 9,
                "field" => "provincia_id",
				"type" => "text",
				"display_name" => "Provincia ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar una provincia de la lista."
                        ]
                    ]
                ]),
                "order" => 13
            ],
            [
                "id" => 914, "data_type_id" => 9,
                "field" => "institucion_belongsto_provincia_relationship",
				"type" => "relationship",
				"display_name" => "Provincia",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\Provincia",
                    "table"       => "provincias",
                    "type"        => "belongsTo",
                    "column"      => "provincia_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "provincias",
                    "pivot"       => "0",
                    "taggable"    => "0",
                    "filterColumn"  => "region_id",
                    "dataAttribute" => "region_id"
                ]),
                "order" => 14
            ],
            [
                "id" => 915, "data_type_id" => 9,
                "field" => "logo",
				"type" => "image",
				"display_name" => "Logo",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 15
            ],
            [
                "id" => 916, "data_type_id" => 9,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 16
            ],
            [
                "id" => 917, "data_type_id" => 9,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 17
            ],
            [
                "id" => 918, "data_type_id" => 9,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 18
            ],
            [
                "id" => 919, "data_type_id" => 9,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 19
            ],




            //           Dependencias Institucionales
            // ==================================================
            [
                "id" => 1001, "data_type_id" => 10,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 1002, "data_type_id" => 10,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 1003, "data_type_id" => 10,
                "field" => "tipo_de_dependencia_institucional_id",
				"type" => "text",
				"display_name" => "Tipo de Dependencia Institucional ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar un tipo de dependencia institucional de la lista."
                        ]
                    ]
                ]),
                "order" => 3
            ],
            [
                "id" => 1004, "data_type_id" => 10,
                "field" => "dependencia_institucional_belongsto_tipo_de_dependencia_institucional_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Dependencia Institucional",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\TipoDeDependenciaInstitucional",
                    "table"       => "tipos_de_dependencias_institucionales",
                    "type"        => "belongsTo",
                    "column"      => "tipos_de_dependencia_institucional_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_dependencias_institucionales",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 4
            ],
            [
                "id" => 1005, "data_type_id" => 10,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 1006, "data_type_id" => 10,
                "field" => "institucion_id",
				"type" => "text",
				"display_name" => "Institución ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar una institución de la lista."
                        ]
                    ]
                ]),
                "order" => 6
            ],
            [
                "id" => 1007, "data_type_id" => 10,
                "field" => "dependencia_institucional_belongsto_institucion_relationship",
				"type" => "relationship",
				"display_name" => "Institución",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\Institucion",
                    "table"       => "instituciones",
                    "type"        => "belongsTo",
                    "column"      => "institucion_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "instituciones",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 7
            ],
            [
                "id" => 1008, "data_type_id" => 10,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 8
            ],
            [
                "id" => 1009, "data_type_id" => 10,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 9
            ],
            [
                "id" => 1010, "data_type_id" => 10,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 10
            ],
            [
                "id" => 1011, "data_type_id" => 10,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 11
            ],




            //               Tipo de Publicación
            // ==================================================
            [
                "id" => 1101, "data_type_id" => 11,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 1102, "data_type_id" => 11,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 1103, "data_type_id" => 11,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 1104, "data_type_id" => 11,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 1105, "data_type_id" => 11,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 1106, "data_type_id" => 11,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 1107, "data_type_id" => 11,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //               Publicación
            // ==================================================
            [
                "id" => 1201, "data_type_id" => 12,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 1202, "data_type_id" => 12,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 1203, "data_type_id" => 12,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 1204, "data_type_id" => 12,
                "field" => "responsables_nombres",
				"type" => "text",
				"display_name" => "Autores",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar uno o más autores para esta publicación",
                            "max" => "Los nombres de los autores no pueden exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 4
            ],
            [
                "id" => 1205, "data_type_id" => 12,
                "field" => "responsables_detalles",
				"type" => "text_area",
				"display_name" => "Autores (Información Adicional)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 1206, "data_type_id" => 12,
                "field" => "institucion_id",
				"type" => "text",
				"display_name" => "Institución ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar una institución de la lista."
                        ]
                    ]
                ]),
                "order" => 6
            ],
            [
                "id" => 1207, "data_type_id" => 12,
                "field" => "publicacion_belongsto_institucion_relationship",
				"type" => "relationship",
				"display_name" => "Institución",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\Institucion",
                    "table"       => "instituciones",
                    "type"        => "belongsTo",
                    "column"      => "institucion_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "instituciones",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 7
            ],
            [
                "id" => 1208, "data_type_id" => 12,
                "field" => "dependencia_institucional_id",
				"type" => "text",
				"display_name" => "Dependencia Institucional ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar una dependencia institucional de la lista."
                        ]
                    ]
                ]),
                "order" => 8
            ],
            [
                "id" => 1209, "data_type_id" => 12,
                "field" => "publicacion_belongsto_dependencia_institucional_relationship",
				"type" => "relationship",
				"display_name" => "Dependencia Institucional",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\DependenciaInstitucional",
                    "table"       => "dependencias_institucionales",
                    "type"        => "belongsTo",
                    "column"      => "dependencia_institucional_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "dependencias_institucionales",
                    "pivot"       => "0",
                    "taggable"    => "0",
                    "filterColumn"  => "institucion_id",
                    "dataAttribute" => "institucion_id"
                ]),
                "order" => 9
            ],
            [
                "id" => 1210, "data_type_id" => 12,
                "field" => "tipo_de_publicacion_id",
				"type" => "text",
				"display_name" => "Tipo de Publicación ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar un tipo de publicación de la lista."
                        ]
                    ]
                ]),
                "order" => 10
            ],
            [
                "id" => 1211, "data_type_id" => 12,
                "field" => "publicacion_belongsto_tipo_de_publicacion_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Publicación",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Publicaciones\\TipoDePublicacion",
                    "table"       => "tipos_de_publicaciones",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_publicacion_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_publicaciones",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 11
            ],
            [
                "id" => 1212, "data_type_id" => 12,
                "field" => "editorial",
				"type" => "text",
				"display_name" => "Editorial",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "La editorial no puede superar los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 12
            ],
            [
                "id" => 1213, "data_type_id" => 12,
                "field" => "anho",
				"type" => "text",
				"display_name" => "Año",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "El año no puede superar los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 13
            ],
            [
                "id" => 1214, "data_type_id" => 12,
                "field" => "ciudad",
				"type" => "text",
				"display_name" => "Ciudad",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "La ciudad no puede superar los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 14
            ],
            [
                "id" => 1215, "data_type_id" => 12,
                "field" => "serie",
				"type" => "text",
				"display_name" => "Serie",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "La serie no puede superar los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 15
            ],
            [
                "id" => 1216, "data_type_id" => 12,
                "field" => "edicion",
				"type" => "text",
				"display_name" => "Edición",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "La edición no puede superar los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 16
            ],
            [
                "id" => 1217, "data_type_id" => 12,
                "field" => "isbn",
				"type" => "text",
				"display_name" => "ISBN",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "El ISBN no puede superar los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 17
            ],
            [
                "id" => 1218, "data_type_id" => 12,
                "field" => "archivos_adjuntos",
				"type" => "file",
				"display_name" => "Archivos Adjuntos",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 18
            ],
            [
                "id" => 1219, "data_type_id" => 12,
                "field" => "contacto_nombre",
				"type" => "text",
				"display_name" => "Contacto (Nombre)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar un contacto para la publicación",
                            "max" => "El nombre del contacto no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 19
            ],
            [
                "id" => 1220, "data_type_id" => 12,
                "field" => "contacto_telefono",
				"type" => "text",
				"display_name" => "Contacto (Teléfono)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar un teléfono de contacto para la publicación",
                            "max" => "El teléfono de contacto no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 20
            ],
            [
                "id" => 1221, "data_type_id" => 12,
                "field" => "contacto_email",
				"type" => "text",
				"display_name" => "Contacto (Email)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar un correo electrónico de contacto para la publicación",
                            "max" => "El correo electrónico del contacto no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 21
            ],
            [
                "id" => 1222, "data_type_id" => 12,
                "field" => "verified_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Verificación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 22
            ],
            [
                "id" => 1223, "data_type_id" => 12,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 23
            ],
            [
                "id" => 1224, "data_type_id" => 12,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 24
            ],
            [
                "id" => 1225, "data_type_id" => 12,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 25
            ],




            //               Tipo de Proyecto
            // ==================================================
            [
                "id" => 1301, "data_type_id" => 13,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 1302, "data_type_id" => 13,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 1303, "data_type_id" => 13,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 1304, "data_type_id" => 13,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 1305, "data_type_id" => 13,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 1306, "data_type_id" => 13,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 1307, "data_type_id" => 13,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //               Tipo de Articulaciones
            // ==================================================
            [
                "id" => 1401, "data_type_id" => 14,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 1402, "data_type_id" => 14,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 1403, "data_type_id" => 14,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 1404, "data_type_id" => 14,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 1405, "data_type_id" => 14,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 1406, "data_type_id" => 14,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 1407, "data_type_id" => 14,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //               Proyectos
            // ==================================================
            [
                "id" => 1501, "data_type_id" => 15,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 1502, "data_type_id" => 15,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 1503, "data_type_id" => 15,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 1504, "data_type_id" => 15,
                "field" => "responsables_nombres",
				"type" => "text",
				"display_name" => "Directores",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar uno o más directores para este proyecto",
                            "max" => "Los nombres de los directores no pueden exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 4
            ],
            [
                "id" => 1505, "data_type_id" => 15,
                "field" => "responsables_detalles",
				"type" => "text_area",
				"display_name" => "Directores (Información Adicional)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 1506, "data_type_id" => 15,
                "field" => "institucion_id",
				"type" => "text",
				"display_name" => "Institución ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar una institución de la lista."
                        ]
                    ]
                ]),
                "order" => 6
            ],
            [
                "id" => 1507, "data_type_id" => 15,
                "field" => "proyecto_belongsto_institucion_relationship",
				"type" => "relationship",
				"display_name" => "Institución",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\Institucion",
                    "table"       => "instituciones",
                    "type"        => "belongsTo",
                    "column"      => "institucion_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "instituciones",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 7
            ],
            [
                "id" => 1508, "data_type_id" => 15,
                "field" => "dependencia_institucional_id",
				"type" => "text",
				"display_name" => "Dependencia Institucional ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar una dependencia institucional de la lista."
                        ]
                    ]
                ]),
                "order" => 8
            ],
            [
                "id" => 1509, "data_type_id" => 15,
                "field" => "proyecto_belongsto_dependencia_institucional_relationship",
				"type" => "relationship",
				"display_name" => "Dependencia Institucional",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\DependenciaInstitucional",
                    "table"       => "dependencias_institucionales",
                    "type"        => "belongsTo",
                    "column"      => "dependencia_institucional_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "dependencias_institucionales",
                    "pivot"       => "0",
                    "taggable"    => "0",
                    "filterColumn"  => "institucion_id",
                    "dataAttribute" => "institucion_id"
                ]),
                "order" => 9
            ],
            [
                "id" => 1510, "data_type_id" => 15,
                "field" => "tipo_de_proyecto_id",
				"type" => "text",
				"display_name" => "Tipo de Proyecto ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar un tipo de proyecto de la lista."
                        ]
                    ]
                ]),
                "order" => 10
            ],
            [
                "id" => 1511, "data_type_id" => 15,
                "field" => "proyecto_belongsto_tipo_de_proyecto_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Proyecto",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Proyectos\\TipoDeProyecto",
                    "table"       => "tipos_de_proyectos",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_proyecto_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_proyectos",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 11
            ],
            [
                "id" => 1512, "data_type_id" => 15,
                "field" => "tipo_de_articulacion_id",
				"type" => "text",
				"display_name" => "Tipo de Articulación ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 12
            ],
            [
                "id" => 1513, "data_type_id" => 15,
                "field" => "proyecto_belongsto_tipo_de_articulacion_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Articulación",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Articulaciones\\TipoDeArticulacion",
                    "table"       => "tipos_de_articulaciones",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_articulacion_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_articulaciones",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 13
            ],
            [
                "id" => 1514, "data_type_id" => 15,
                "field" => "articulacion_detalles",
				"type" => "text_area",
				"display_name" => "Articulación (Detalles Adicionales)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 14
            ],
            [
                "id" => 1515, "data_type_id" => 15,
                "field" => "destinatarios",
				"type" => "text_area",
				"display_name" => "Destinatarios",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 15
            ],
            [
                "id" => 1516, "data_type_id" => 15,
                "field" => "archivos_adjuntos",
				"type" => "file",
				"display_name" => "Archivos Adjuntos",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 16
            ],
            [
                "id" => 1517, "data_type_id" => 15,
                "field" => "contacto_nombre",
				"type" => "text",
				"display_name" => "Contacto (Nombre)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar un contacto para el proyecto",
                            "max" => "El nombre del contacto no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 17
            ],
            [
                "id" => 1518, "data_type_id" => 15,
                "field" => "contacto_telefono",
				"type" => "text",
				"display_name" => "Contacto (Teléfono)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar un teléfono de contacto para el proyecto",
                            "max" => "El teléfono de contacto no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 18
            ],
            [
                "id" => 1519, "data_type_id" => 15,
                "field" => "contacto_email",
				"type" => "text",
				"display_name" => "Contacto (Email)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar un correo electrónico de contacto para el proyecto",
                            "max" => "El correo electrónico del contacto no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 19
            ],
            [
                "id" => 1520, "data_type_id" => 15,
                "field" => "verified_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Verificación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 20
            ],
            [
                "id" => 1521, "data_type_id" => 15,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 21
            ],
            [
                "id" => 1522, "data_type_id" => 15,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 22
            ],
            [
                "id" => 1523, "data_type_id" => 15,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 23
            ],




            //               Tipo de Curso
            // ==================================================
            [
                "id" => 1601, "data_type_id" => 16,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 1602, "data_type_id" => 16,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 1603, "data_type_id" => 16,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 1604, "data_type_id" => 16,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 1605, "data_type_id" => 16,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 1606, "data_type_id" => 16,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 1607, "data_type_id" => 16,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //               Tipo de Curricularidades
            // ==================================================
            [
                "id" => 1701, "data_type_id" => 17,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 1702, "data_type_id" => 17,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 1703, "data_type_id" => 17,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 1704, "data_type_id" => 17,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 1705, "data_type_id" => 17,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 1706, "data_type_id" => 17,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 1707, "data_type_id" => 17,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //               Tipo de Intensidades
            // ==================================================
            [
                "id" => 1801, "data_type_id" => 18,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 1802, "data_type_id" => 18,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 1803, "data_type_id" => 18,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 1804, "data_type_id" => 18,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 1805, "data_type_id" => 18,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 1806, "data_type_id" => 18,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 1807, "data_type_id" => 18,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //               Tipo de Modalidades
            // ==================================================
            [
                "id" => 1901, "data_type_id" => 19,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 1902, "data_type_id" => 19,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 1903, "data_type_id" => 19,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 1904, "data_type_id" => 19,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 1905, "data_type_id" => 19,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 1906, "data_type_id" => 19,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 1907, "data_type_id" => 19,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //               Tipo de Regularidades
            // ==================================================
            [
                "id" => 2001, "data_type_id" => 20,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 2002, "data_type_id" => 20,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 2003, "data_type_id" => 20,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 2004, "data_type_id" => 20,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 2005, "data_type_id" => 20,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 2006, "data_type_id" => 20,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 2007, "data_type_id" => 20,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //               Tipo de Niveles
            // ==================================================
            [
                "id" => 2101, "data_type_id" => 21,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 2102, "data_type_id" => 21,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 2103, "data_type_id" => 21,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 2104, "data_type_id" => 21,
                "field" => "orden",
				"type" => "text",
				"display_name" => "Orden",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 4
            ],
            [
                "id" => 2105, "data_type_id" => 21,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 2106, "data_type_id" => 21,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
            [
                "id" => 2107, "data_type_id" => 21,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 7
            ],




            //               Cursos
            // ==================================================
            [
                "id" => 2201, "data_type_id" => 22,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 2202, "data_type_id" => 22,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 2203, "data_type_id" => 22,
                "field" => "descripcion",
				"type" => "text_area",
				"display_name" => "Descripción",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 2204, "data_type_id" => 22,
                "field" => "institucion_id",
				"type" => "text",
				"display_name" => "Institución ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar una institución de la lista."
                        ]
                    ]
                ]),
                "order" => 4
            ],
            [
                "id" => 2205, "data_type_id" => 22,
                "field" => "curso_belongsto_institucion_relationship",
				"type" => "relationship",
				"display_name" => "Institución",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\Institucion",
                    "table"       => "instituciones",
                    "type"        => "belongsTo",
                    "column"      => "institucion_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "instituciones",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 5
            ],
            [
                "id" => 2206, "data_type_id" => 22,
                "field" => "dependencia_institucional_id",
				"type" => "text",
				"display_name" => "Dependencia Institucional ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar una dependencia institucional de la lista."
                        ]
                    ]
                ]),
                "order" => 6
            ],
            [
                "id" => 2207, "data_type_id" => 22,
                "field" => "curso_belongsto_dependencia_institucional_relationship",
				"type" => "relationship",
				"display_name" => "Dependencia Institucional",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Locaciones\\DependenciaInstitucional",
                    "table"       => "dependencias_institucionales",
                    "type"        => "belongsTo",
                    "column"      => "dependencia_institucional_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "dependencias_institucionales",
                    "pivot"       => "0",
                    "taggable"    => "0",
                    "filterColumn"  => "institucion_id",
                    "dataAttribute" => "institucion_id"
                ]),
                "order" => 7
            ],
            [
                "id" => 2208, "data_type_id" => 22,
                "field" => "tipo_de_curso_id",
				"type" => "text",
				"display_name" => "Tipo de Curso ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required",
                        "messages" => [
                            "required" => "Debe seleccionar un tipo de curso de la lista."
                        ]
                    ]
                ]),
                "order" => 8
            ],
            [
                "id" => 2209, "data_type_id" => 22,
                "field" => "curso_belongsto_tipo_de_curso_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Curso",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Cursos\\TipoDeCurso",
                    "table"       => "tipos_de_cursos",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_curso_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_cursos",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 9
            ],
            [
                "id" => 2210, "data_type_id" => 22,
                "field" => "tipo_de_intensidad_id",
				"type" => "text",
				"display_name" => "Tipo de Intensidad ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 10
            ],
            [
                "id" => 2211, "data_type_id" => 22,
                "field" => "curso_belongsto_tipo_de_intensidad_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Intensidad",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Cursos\\TipoDeIntensidad",
                    "table"       => "tipos_de_intensidades",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_intensidad_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_intensidades",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 11
            ],
            [
                "id" => 2212, "data_type_id" => 22,
                "field" => "tipo_de_modalidad_id",
				"type" => "text",
				"display_name" => "Tipo de Modalidad ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 12
            ],
            [
                "id" => 2213, "data_type_id" => 22,
                "field" => "curso_belongsto_tipo_de_modalidad_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Modalidad",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Cursos\\TipoDeModalidad",
                    "table"       => "tipos_de_modalidades",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_modalidad_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_modalidades",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 13
            ],
            [
                "id" => 2214, "data_type_id" => 22,
                "field" => "tipo_de_regularidad_id",
				"type" => "text",
				"display_name" => "Tipo de Regularidad ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 14
            ],
            [
                "id" => 2215, "data_type_id" => 22,
                "field" => "curso_belongsto_tipo_de_regularidad_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Regularidad",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Cursos\\TipoDeRegularidad",
                    "table"       => "tipos_de_regularidades",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_regularidad_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_regularidades",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 15
            ],
            [
                "id" => 2216, "data_type_id" => 22,
                "field" => "tipo_de_nivel_id",
				"type" => "text",
				"display_name" => "Tipo de Nivel ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 16
            ],
            [
                "id" => 2217, "data_type_id" => 22,
                "field" => "curso_belongsto_tipo_de_nivel_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Nivel",
                "required" => 0,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Cursos\\TipoDeNivel",
                    "table"       => "tipos_de_niveles",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_nivel_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_niveles",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 17
            ],
            [
                "id" => 2218, "data_type_id" => 22,
                "field" => "tipo_de_curricularidad_id",
				"type" => "text",
				"display_name" => "Tipo de Curricularidad ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 18
            ],
            [
                "id" => 2219, "data_type_id" => 22,
                "field" => "curso_belongsto_tipo_de_curricularidad_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Curricularidad",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Cursos\\TipoDeCurricularidad",
                    "table"       => "tipos_de_curricularidades",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_curricularidad_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_curricularidades",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 19
            ],
            [
                "id" => 2220, "data_type_id" => 22,
                "field" => "arancelado",
				"type" => "checkbox",
				"display_name" => "Arancelado",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "on" => "Si",
                    "off" => "No"
                ]),
                "order" => 20
            ],
            [
                "id" => 2221, "data_type_id" => 22,
                "field" => "fecha_de_inicio",
				"type" => "date",
				"display_name" => "Fecha de Inicio",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 21
            ],
            [
                "id" => 2222, "data_type_id" => 22,
                "field" => "fecha_de_fin",
				"type" => "date",
				"display_name" => "Fecha de Fin",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 22
            ],
            [
                "id" => 2223, "data_type_id" => 22,
                "field" => "tipo_de_articulacion_id",
				"type" => "text",
				"display_name" => "Tipo de Articulación ID",
                "required" => 1,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 23
            ],
            [
                "id" => 2224, "data_type_id" => 22,
                "field" => "curso_belongsto_tipo_de_articulacion_relationship",
				"type" => "relationship",
				"display_name" => "Tipo de Articulación",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "model"       => "App\\Models\\Articulaciones\\TipoDeArticulacion",
                    "table"       => "tipos_de_articulaciones",
                    "type"        => "belongsTo",
                    "column"      => "tipo_de_articulacion_id",
                    "key"         => "id",
                    "label"       => "nombre",
                    "pivot_table" => "tipos_de_articulaciones",
                    "pivot"       => "0",
                    "taggable"    => "0"
                ]),
                "order" => 24
            ],
            [
                "id" => 2225, "data_type_id" => 22,
                "field" => "articulacion_detalles",
				"type" => "text_area",
				"display_name" => "Articulación",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 25
            ],
            [
                "id" => 2226, "data_type_id" => 22,
                "field" => "estrategia_metodologica",
				"type" => "text_area",
				"display_name" => "Estrategia Metodológica",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 26
            ],
            [
                "id" => 2227, "data_type_id" => 22,
                "field" => "condiciones",
				"type" => "text_area",
				"display_name" => "Condiciones",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 27
            ],
            [
                "id" => 2228, "data_type_id" => 22,
                "field" => "otros",
				"type" => "text_area",
				"display_name" => "Otros",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 28
            ],
            [
                "id" => 2229, "data_type_id" => 22,
                "field" => "responsables_nombres",
				"type" => "text",
				"display_name" => "Docentes",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "max:255",
                        "messages" => [
                            "max" => "Los nombres de los docentes no pueden exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 29
            ],
            [
                "id" => 2230, "data_type_id" => 22,
                "field" => "responsables_detalles",
				"type" => "text_area",
				"display_name" => "Docentes (Detalles Adicionales)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 30
            ],
            [
                "id" => 2231, "data_type_id" => 22,
                "field" => "contacto_nombre",
				"type" => "text",
				"display_name" => "Contacto (Nombre)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar un contacto para el curso",
                            "max" => "El nombre del contacto no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 31
            ],
            [
                "id" => 2232, "data_type_id" => 22,
                "field" => "contacto_telefono",
				"type" => "text",
				"display_name" => "Contacto (Teléfono)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar un teléfono de contacto para el curso",
                            "max" => "El teléfono de contacto no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 32
            ],
            [
                "id" => 2233, "data_type_id" => 22,
                "field" => "contacto_email",
				"type" => "text",
				"display_name" => "Contacto (Email)",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "Debe declarar un correo electrónico de contacto para el curso",
                            "max" => "El correo electrónico del contacto no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 33
            ],
            [
                "id" => 2234, "data_type_id" => 22,
                "field" => "archivos_adjuntos",
				"type" => "file",
				"display_name" => "Archivos Adjuntos",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 34
            ],
            [
                "id" => 2235, "data_type_id" => 22,
                "field" => "verified_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Verificación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 35
            ],
            [
                "id" => 2236, "data_type_id" => 22,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 36
            ],
            [
                "id" => 2237, "data_type_id" => 22,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 37
            ],
            [
                "id" => 2238, "data_type_id" => 22,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 38
            ],




            //               Páginas
            // ==================================================
            [
                "id" => 2301, "data_type_id" => 23,
                "field" => "id",
				"type" => "number",
				"display_name" => "ID",
                "required" => 1,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 1
            ],
            [
                "id" => 2302, "data_type_id" => 23,
                "field" => "nombre",
				"type" => "text",
				"display_name" => "Nombre",
                "required" => 1,
                "browse" => 1, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => json_encode([
                    "validation" => [
                        "rule" => "required|max:255",
                        "messages" => [
                            "required" => "El nombre no puede estar vacío",
                            "max" => "El nombre no puede exceder los 255 caracteres"
                        ]
                    ]
                ]),
                "order" => 2
            ],
            [
                "id" => 2303, "data_type_id" => 23,
                "field" => "contenido",
				"type" => "rich_text_box",
				"display_name" => "Contenidos",
                "required" => 0,
                "browse" => 0, "read" => 1, "edit" => 1, "add" => 1, "delete" => 1,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 2304, "data_type_id" => 23,
                "field" => "created_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Creación",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 3
            ],
            [
                "id" => 2305, "data_type_id" => 23,
                "field" => "updated_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Actualización",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 5
            ],
            [
                "id" => 2306, "data_type_id" => 23,
                "field" => "deleted_at",
				"type" => "timestamp",
				"display_name" => "Fecha de Borrado",
                "required" => 0,
                "browse" => 0, "read" => 0, "edit" => 0, "add" => 0, "delete" => 0,
                "details" => NULL,
                "order" => 6
            ],
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('data_rows')
            ->whereIn('id', [
                 101,  102,  103,  104,  105,  106,  107,  108,  109,   // Uusarios
                    110,  111,  112,
                 201,  202,  203,  204,  205,                           // Roles
                 301,  302,  303,  304,                                 // Menus
                 401,  402,  403,  404,  405,  406,  407,                // Paises
                 501,  502,  503,  504,  505,  506,  507,  508,         // Regiones
                 601,  602,  603,  604,  605,  606,  607,  608,  609,   // Provincias
                    610,
                 701,  702,  703,  704,  705,  706,  707,               // Tipos de Instituciones
                 801,  802,  803,  804,  805,  806,  807,               // Tipos de Dependencia Institucional
                 901,  902,  903,  904,  905,  906,  907,  908,  909,   // Instituciones
                    910,  911,  912,  913,  914,  915,  916,  917,  918,  919,
                1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009,   // Dependencias Institucionales
                    1010, 1011,
                1101, 1102, 1103, 1104, 1105, 1106, 1107,               // Tipo de Publicacion
                1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209,   // Publicacion
                    1210, 1211, 1212, 1213, 1214, 1215,
                1301, 1302, 1303, 1304, 1305, 1306, 1307,               // Tipo de Proyecto
                1401, 1402, 1403, 1404, 1405, 1406, 1407,               // Tipo de Articulación
                1501, 1502, 1503, 1504, 1505, 1506, 1507, 1508, 1509,   // Proyecto
                    1510, 1511, 1512, 1513, 1514, 1515, 1516, 1517,
                1601, 1602, 1603, 1604, 1605, 1606, 1607,               // Tipo de Curso
                1701, 1702, 1703, 1704, 1705, 1706, 1707,               // Tipo de Curricularidad
                1801, 1802, 1803, 1804, 1805, 1806, 1807,               // Tipo de Intensidad
                1901, 1902, 1903, 1904, 1905, 1906, 1907,               // Tipo de Modalidades
                2001, 2002, 2003, 2004, 2005, 2006, 2007,               // Tipo de Regularidades
                2101, 2102, 2103, 2104, 2105, 2106, 2107,               // Tipo de Niveles
                2201, 2202, 2203, 2204, 2205, 2206, 2207, 2208, 2209,   // Curso
                    2210, 2212, 2213, 2214, 2215, 2216, 2217, 2218, 2219,
                    2220, 2222, 2223, 2224, 2225, 2226, 2237, 2238, 2229,
                    2230, 2232, 2233, 2234, 2235, 2236, 2237, 2238, 2239,
                2301, 2302, 2303, 2304, 2305, 2306                      // Paginas
            ])
		    ->delete();
	}

}
