<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class UpdateUserBelongsToManyRolesInDataRowsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('data_rows')
        ->where('field', 'user_belongstomany_role_relationship')
        ->update(['add' => 1, 'delete' => 1]);

        DB::table('data_rows')
        ->where('field', 'user_belongsto_role_relationship')
        ->update(['add' => 1, 'delete' => 1]);
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('data_rows')
            ->where('field', 'user_belongstomany_role_relationship')
            ->update(['add' => 0, 'delete' => 0]);
	}

}
