<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoPermissionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('permissions')->insert([
            // General
			[
				"id" => 1,
                "key" => "browse_admin", "table_name" => NULL,
                "created_at" => $timestamp, "updated_at" => $timestamp
			],
            [
                "id" => 2,
                "key" => "browse_bread", "table_name" => NULL,
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 3,
                "key" => "browse_database", "table_name" => NULL,
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 4,
                "key" => "browse_media", "table_name" => NULL,
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 5,
                "key" => "browse_compass", "table_name" => NULL,
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 6,
                "key" => "browse_hooks", "table_name" => NULL,
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Users
            [
                "id" => 7,
                "key" => "browse_users", "table_name" => "users",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 8,
                "key" => "read_users", "table_name" => "users",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 9,
                "key" => "edit_users", "table_name" => "users",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 10,
                "key" => "add_users", "table_name" => "users",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 11,
                "key" => "delete_users", "table_name" => "users",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Roles
            [
                "id" => 12,
                "key" => "browse_roles", "table_name" => "roles",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 13,
                "key" => "read_roles", "table_name" => "roles",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 14,
                "key" => "edit_roles", "table_name" => "roles",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 15,
                "key" => "add_roles", "table_name" => "roles",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 16,
                "key" => "delete_roles", "table_name" => "roles",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Menus
            [
                "id" => 17,
                "key" => "browse_menus", "table_name" => "menus",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 18,
                "key" => "read_menus", "table_name" => "menus",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 19,
                "key" => "edit_menus", "table_name" => "menus",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 20,
                "key" => "add_menus", "table_name" => "menus",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 21,
                "key" => "delete_menus", "table_name" => "menus",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Paises
            [
                "id" => 22,
                "key" => "browse_paises", "table_name" => "paises",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 23,
                "key" => "read_paises", "table_name" => "paises",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 24,
                "key" => "edit_paises", "table_name" => "paises",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 25,
                "key" => "add_paises", "table_name" => "paises",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 26,
                "key" => "delete_paises", "table_name" => "paises",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Regiones
            [
                "id" => 27,
                "key" => "browse_regiones", "table_name" => "regiones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 28,
                "key" => "read_regiones", "table_name" => "regiones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 29,
                "key" => "edit_regiones", "table_name" => "regiones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 30,
                "key" => "add_regiones", "table_name" => "regiones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 31,
                "key" => "delete_regiones", "table_name" => "regiones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Provincias
            [
                "id" => 32,
                "key" => "browse_provincias", "table_name" => "provincias",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 33,
                "key" => "read_provincias", "table_name" => "provincias",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 34,
                "key" => "edit_provincias", "table_name" => "provincias",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 35,
                "key" => "add_provincias", "table_name" => "provincias",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 36,
                "key" => "delete_provincias", "table_name" => "provincias",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipo de Institución
            [
                "id" => 37,
                "key" => "browse_tipos_de_instituciones", "table_name" => "tipos_de_instituciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 38,
                "key" => "read_tipos_de_instituciones", "table_name" => "tipos_de_instituciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 39,
                "key" => "edit_tipos_de_instituciones", "table_name" => "tipos_de_instituciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 40,
                "key" => "add_tipos_de_instituciones", "table_name" => "tipos_de_instituciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 41,
                "key" => "delete_tipos_de_instituciones", "table_name" => "tipos_de_instituciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipo de Dependencias Institucionales
            [
                "id" => 42,
                "key" => "browse_tipos_de_dependencias_institucionales", "table_name" => "tipos_de_dependencias_institucionales",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 43,
                "key" => "read_tipos_de_dependencias_institucionales", "table_name" => "tipos_de_dependencias_institucionales",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 44,
                "key" => "edit_tipos_de_dependencias_institucionales", "table_name" => "tipos_de_dependencias_institucionales",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 45,
                "key" => "add_tipos_de_dependencias_institucionales", "table_name" => "tipos_de_dependencias_institucionales",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 46,
                "key" => "delete_tipos_de_dependencias_institucionales", "table_name" => "tipos_de_dependencias_institucionales",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Instituciones
            [
                "id" => 47,
                "key" => "browse_instituciones", "table_name" => "instituciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 48,
                "key" => "read_instituciones", "table_name" => "instituciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 49,
                "key" => "edit_instituciones", "table_name" => "instituciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 50,
                "key" => "add_instituciones", "table_name" => "instituciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 51,
                "key" => "delete_instituciones", "table_name" => "instituciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Dependencias Institucionales
            [
                "id" => 52,
                "key" => "browse_dependencias_institucionales", "table_name" => "dependencias_institucionales",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 53,
                "key" => "read_dependencias_institucionales", "table_name" => "dependencias_institucionales",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 54,
                "key" => "edit_dependencias_institucionales", "table_name" => "dependencias_institucionales",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 55,
                "key" => "add_dependencias_institucionales", "table_name" => "dependencias_institucionales",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 56,
                "key" => "delete_dependencias_institucionales", "table_name" => "dependencias_institucionales",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipos de publicaciones
            [
                "id" => 57,
                "key" => "browse_tipos_de_publicaciones", "table_name" => "tipos_de_publicaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 58,
                "key" => "read_tipos_de_publicaciones", "table_name" => "tipos_de_publicaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 59,
                "key" => "edit_tipos_de_publicaciones", "table_name" => "tipos_de_publicaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 60,
                "key" => "add_tipos_de_publicaciones", "table_name" => "tipos_de_publicaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 61,
                "key" => "delete_tipos_de_publicaciones", "table_name" => "tipos_de_publicaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Publicaciones
            [
                "id" => 62,
                "key" => "browse_publicaciones", "table_name" => "publicaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 63,
                "key" => "read_publicaciones", "table_name" => "publicaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 64,
                "key" => "edit_publicaciones", "table_name" => "publicaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 65,
                "key" => "add_publicaciones", "table_name" => "publicaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 66,
                "key" => "delete_publicaciones", "table_name" => "publicaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipos de proyecto
            [
                "id" => 67,
                "key" => "browse_tipos_de_proyectos", "table_name" => "tipos_de_proyectos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 68,
                "key" => "read_tipos_de_proyectos", "table_name" => "tipos_de_proyectos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 69,
                "key" => "edit_tipos_de_proyectos", "table_name" => "tipos_de_proyectos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 70,
                "key" => "add_tipos_de_proyectos", "table_name" => "tipos_de_proyectos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 71,
                "key" => "delete_tipos_de_proyectos", "table_name" => "tipos_de_proyectos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipos de articulación
            [
                "id" => 72,
                "key" => "browse_tipos_de_articulaciones", "table_name" => "tipos_de_articulaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 73,
                "key" => "read_tipos_de_articulaciones", "table_name" => "tipos_de_articulaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 74,
                "key" => "edit_tipos_de_articulaciones", "table_name" => "tipos_de_articulaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 75,
                "key" => "add_tipos_de_articulaciones", "table_name" => "tipos_de_articulaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 76,
                "key" => "delete_tipos_de_articulaciones", "table_name" => "tipos_de_articulaciones",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Proyectos
            [
                "id" => 77,
                "key" => "browse_proyectos", "table_name" => "proyectos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 78,
                "key" => "read_proyectos", "table_name" => "proyectos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 79,
                "key" => "edit_proyectos", "table_name" => "proyectos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 80,
                "key" => "add_proyectos", "table_name" => "proyectos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 81,
                "key" => "delete_proyectos", "table_name" => "proyectos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipos de cursos
            [
                "id" => 82,
                "key" => "browse_tipos_de_cursos", "table_name" => "tipos_de_cursos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 83,
                "key" => "read_tipos_de_cursos", "table_name" => "tipos_de_cursos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 84,
                "key" => "edit_tipos_de_cursos", "table_name" => "tipos_de_cursos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 85,
                "key" => "add_tipos_de_cursos", "table_name" => "tipos_de_cursos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 86,
                "key" => "delete_tipos_de_cursos", "table_name" => "tipos_de_cursos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipos de curricularidades
            [
                "id" => 87,
                "key" => "browse_tipos_de_curricularidades", "table_name" => "tipos_de_curricularidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 88,
                "key" => "read_tipos_de_curricularidades", "table_name" => "tipos_de_curricularidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 89,
                "key" => "edit_tipos_de_curricularidades", "table_name" => "tipos_de_curricularidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 90,
                "key" => "add_tipos_de_curricularidades", "table_name" => "tipos_de_curricularidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 91,
                "key" => "delete_tipos_de_curricularidades", "table_name" => "tipos_de_curricularidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipos de intensidades
            [
                "id" => 92,
                "key" => "browse_tipos_de_intensidades", "table_name" => "tipos_de_intensidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 93,
                "key" => "read_tipos_de_intensidades", "table_name" => "tipos_de_intensidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 94,
                "key" => "edit_tipos_de_intensidades", "table_name" => "tipos_de_intensidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 95,
                "key" => "add_tipos_de_intensidades", "table_name" => "tipos_de_intensidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 96,
                "key" => "delete_tipos_de_intensidades", "table_name" => "tipos_de_intensidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipos de modalidades
            [
                "id" => 97,
                "key" => "browse_tipos_de_modalidades", "table_name" => "tipos_de_modalidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 98,
                "key" => "read_tipos_de_modalidades", "table_name" => "tipos_de_modalidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 99,
                "key" => "edit_tipos_de_modalidades", "table_name" => "tipos_de_modalidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 100,
                "key" => "add_tipos_de_modalidades", "table_name" => "tipos_de_modalidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 101,
                "key" => "delete_tipos_de_modalidades", "table_name" => "tipos_de_modalidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipos de regularidades
            [
                "id" => 102,
                "key" => "browse_tipos_de_regularidades", "table_name" => "tipos_de_regularidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 103,
                "key" => "read_tipos_de_regularidades", "table_name" => "tipos_de_regularidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 104,
                "key" => "edit_tipos_de_regularidades", "table_name" => "tipos_de_regularidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 105,
                "key" => "add_tipos_de_regularidades", "table_name" => "tipos_de_regularidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 106,
                "key" => "delete_tipos_de_regularidades", "table_name" => "tipos_de_regularidades",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Tipos de niveles
            [
                "id" => 107,
                "key" => "browse_tipos_de_niveles", "table_name" => "tipos_de_niveles",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 108,
                "key" => "read_tipos_de_niveles", "table_name" => "tipos_de_niveles",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 109,
                "key" => "edit_tipos_de_niveles", "table_name" => "tipos_de_niveles",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 110,
                "key" => "add_tipos_de_niveles", "table_name" => "tipos_de_niveles",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 111,
                "key" => "delete_tipos_de_niveles", "table_name" => "tipos_de_niveles",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Cursos
            [
                "id" => 112,
                "key" => "browse_cursos", "table_name" => "cursos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 113,
                "key" => "read_cursos", "table_name" => "cursos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 114,
                "key" => "edit_cursos", "table_name" => "cursos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 115,
                "key" => "add_cursos", "table_name" => "cursos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 116,
                "key" => "delete_cursos", "table_name" => "cursos",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Páginas
            [
                "id" => 117,
                "key" => "browse_paginas", "table_name" => "paginas",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 118,
                "key" => "read_paginas", "table_name" => "paginas",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 119,
                "key" => "edit_paginas", "table_name" => "paginas",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 120,
                "key" => "add_paginas", "table_name" => "paginas",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 121,
                "key" => "delete_paginas", "table_name" => "paginas",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Settings
            [
                "id" => 122,
                "key" => "browse_settings", "table_name" => "settings",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 123,
                "key" => "read_settings", "table_name" => "settings",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 124,
                "key" => "add_settings", "table_name" => "settings",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 125,
                "key" => "edit_settings", "table_name" => "settings",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 126,
                "key" => "delete_settings", "table_name" => "settings",
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            // Soft deleted
            [
                "id" => 127,
                "key" => "browse_soft_deletes", "table_name" => NULL,
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 128,
                "key" => "delete_soft_deletes", "table_name" => NULL,
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
            [
                "id" => 129,
                "key" => "restore_soft_deletes", "table_name" => NULL,
                "created_at" => $timestamp, "updated_at" => $timestamp
            ],
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('permissions')
            ->whereIn('id', [
                      1,  2,    3,  4,    5,   6,   7,   8,   9,
                 10,  11,  12,  13,  14,  15,  16,  17,  18,  19,
                 20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
                 30,  31,  32,  33,  34,  35,  36,  37,  38,  39,
                 40,  41,  42,  43,  44,  45,  46,  47,  48,  49,
                 50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
                 60,  61,  62,  63,  64,  65,  66,  67,  68,  69,
                 70,  71,  72,  73,  74,  75,  76,  77,  78,  79,
                 80,  81,  82,  83,  84,  85,  86,  87,  88,  89,
                 90,  91,  92,  93,  94,  95,  96,  97,  98,  99,
                100, 101, 102, 103, 104, 105, 106, 107, 108, 109,
                110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
                120, 121
            ])
		    ->delete();
	}

}
