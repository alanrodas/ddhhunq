<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoRegionesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('regiones')->insert([
			[
				"id" => 1,
				"nombre" => "Centro Este",
				"pais_id" => 1,
                "orden" => 3,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 2,
				"nombre" => "Noroeste",
				"pais_id" => 1,
                "orden" => 6,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 3,
				"nombre" => "Noreste",
				"pais_id" => 1,
                "orden" => 5,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 4,
				"nombre" => "Sur",
				"pais_id" => 1,
                "orden" => 7,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 5,
				"nombre" => "Centro Oeste",
				"pais_id" => 1,
                "orden" => 4,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 6,
				"nombre" => "Bonaerense",
				"pais_id" => 1,
                "orden" => 2,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 7,
				"nombre" => "Metropolitana",
				"pais_id" => 1,
                "orden" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			]
		]);
    }
    
    

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('regiones')
            ->whereIn("id", [1, 2, 3, 4, 5, 6, 7])
		    ->delete();
	}

}
