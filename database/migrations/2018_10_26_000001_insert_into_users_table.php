<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('users')->insert([
			[
				"id" => 1,
				"role_id" => 1,
				"name" => "Alan Rodas Bonjour",
				"email" => "alanrodas@gmail.com",
				"avatar" => "defaults/users/alanrodas.jpg",
				"email_verified_at" => $timestamp,
				"password" => "\$2y\$10\$8/ygD3FB.WXG7kQ6/xTQhuZRhsGmZocoUzQCFwLjq9GJR55dnu5tm",
				"remember_token" => "7kwZmcqEP4Poz3dGIbUxDS5ZFwRHjVuwJereUYiAnKaUDZ4zHUHWB8yuR5CQ",
				"settings" => "{\"locale\":\"es\"}",
				"created_at" => $timestamp,
				"updated_at" => $timestamp
			],
			[
				"id" => 2,
				"role_id" => 2,
				"name" => "Omar Suarez",
				"email" => "suarezomar@hotmail.com",
				"avatar" => "defaults/users/omarsuarez.jpg",
                "email_verified_at" => $timestamp,
                // use password: 123456
				"password" => "\$2y\$10\$/4UliQILqGk4fufvbNf6ren34ynns9Kvx2uOzuq5erPmyrvIj.4OK",
				"remember_token" => "",
				"settings" => "{\"locale\":\"es\"}",
                "created_at" => $timestamp,
                "updated_at" => $timestamp
			]
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('users')
            ->whereIn('id', [1, 2])
		    ->delete();
	}

}
