<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCursosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cursos', function(Blueprint $table)
		{
            // datos básicos
			$table->increments('id');
			$table->string('nombre');
            $table->integer('tipo_de_curso_id')->unsigned();
            $table->integer('institucion_id')->unsigned();
            $table->integer('dependencia_institucional_id')->unsigned();
            $table->text('descripcion')->nullable();
            // contacto
			$table->string('contacto_nombre')->nullable();
			$table->string('contacto_telefono')->nullable();
			$table->string('contacto_email')->nullable();
            // responsables
            $table->string('responsables_nombres', 255)->nullable();
            $table->text('responsables_detalles')->nullable();
            // articulacion
            $table->integer('tipo_de_articulacion_id')->unsigned()->nullable();
            $table->text('articulacion_detalles')->nullable();
            // archivos adjunto
            $table->longText('archivos_adjuntos')->nullable();
            // otras categorías
            $table->integer('tipo_de_intensidad_id')->unsigned()->nullable();
            $table->integer('tipo_de_modalidad_id')->unsigned()->nullable();
            $table->integer('tipo_de_regularidad_id')->unsigned()->nullable();
            $table->integer('tipo_de_nivel_id')->unsigned()->nullable();
            $table->integer('tipo_de_curricularidad_id')->unsigned()->nullable();
            $table->boolean('arancelado')->default(0);
            $table->date('fecha_de_inicio')->nullable();
            $table->date('fecha_de_fin')->nullable();
            // otros datos
            $table->text('estrategia_metodologica')->nullable();
			$table->text('condiciones')->nullable();
			$table->text('otros')->nullable();
			// timestamps
			$table->timestamps();
			$table->softDeletes();
            $table->timestamp('verified_at')->nullable();


            $table->foreign('tipo_de_curso_id', 'cursos_tipos_de_cursos_FK')
                ->references('id')->on('tipos_de_cursos')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tipo_de_intensidad_id', 'cursos_tipos_de_intensidades_FK')
                ->references('id')->on('tipos_de_intensidades')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tipo_de_modalidad_id', 'cursos_tipos_de_modalidades_FK')
                ->references('id')->on('tipos_de_modalidades')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tipo_de_regularidad_id', 'cursos_tipos_de_regularidades_FK')
                ->references('id')->on('tipos_de_regularidades')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tipo_de_nivel_id', 'cursos_tipos_de_niveles_FK')
                ->references('id')->on('tipos_de_niveles')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tipo_de_curricularidad_id', 'cursos_tipos_de_curricularidades_FK')
                ->references('id')->on('tipos_de_curricularidades')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tipo_de_articulacion_id', 'cursos_tipos_de_articulaciones_FK')
                ->references('id')->on('tipos_de_articulaciones')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('institucion_id', 'cursos_instituciones_FK')
                ->references('id')->on('instituciones')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('dependencia_institucional_id', 'cursos_dependencias_institucionales_FK')
                ->references('id')->on('dependencias_institucionales')
                ->onDelete('cascade')
                ->onUpdate('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cursos');
	}

}
