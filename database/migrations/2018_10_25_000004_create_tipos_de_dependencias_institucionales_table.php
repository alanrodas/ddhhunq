<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTiposDeDependenciasInstitucionalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tipos_de_dependencias_institucionales', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
            $table->mediumText('descripcion')->nullable();
			$table->smallInteger('orden')->unsigned()->index('ORDEN');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipos_de_dependencias_institucionales');
	}

}
