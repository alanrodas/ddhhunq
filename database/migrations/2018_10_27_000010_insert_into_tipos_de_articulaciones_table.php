<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoTiposDeArticulacionesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('tipos_de_articulaciones')->insert([
			[
				"id" => 1,
				"nombre" => "Otros",
                "descripcion" => NULL,
                "orden" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 2,
				"nombre" => "Ninguna",
                "descripcion" => NULL,
                "orden" => 2,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 3,
				"nombre" => "Local",
                "descripcion" => NULL,
                "orden" => 3,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 4,
				"nombre" => "Nacional",
                "descripcion" => NULL,
                "orden" => 4,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 5,
				"nombre" => "Internacional",
                "descripcion" => NULL,
                "orden" => 5,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			]
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('tipos_de_articulaciones')
            ->whereIn("id", [1])
		    ->delete();
	}

}
