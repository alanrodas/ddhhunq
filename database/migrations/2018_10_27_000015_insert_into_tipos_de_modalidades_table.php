<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoTiposDeModalidadesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('tipos_de_modalidades')->insert([
			[
				"id" => 1,
				"nombre" => "Otros",
                "descripcion" => NULL,
                "orden" => 100,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 2,
				"nombre" => "Presencial",
                "descripcion" => NULL,
                "orden" => 2,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 3,
				"nombre" => "Semipresencial",
                "descripcion" => NULL,
                "orden" => 3,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 4,
				"nombre" => "Virtual",
                "descripcion" => NULL,
                "orden" => 4,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			]
		]);
    }
    
    

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('tipos_de_modalidades')
            ->whereIn("id", [1, 2, 3, 4])
		    ->delete();
	}

}
