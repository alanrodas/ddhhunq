<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoInstitucionesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('instituciones')->insert([
			[
				"id" => 1,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Tucumán (UNT)",
				"direccion" => "Ayacucho 491, San Miguel de Tucumán",
                "codigo_postal" => NULL,
                "telefono" => "(54) (381) 424-7752",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 2,
				"provincia_id" => 4,
                "orden" => 1,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 2,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Santiago del Estero (UNSE)",
				"direccion" => "Avenida Belgrano Sur 1912, Santiago del Estero",
                "codigo_postal" => "4200",
				"telefono" => "(54) (385) 450-9500",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 2,
				"provincia_id" => 5,
                "orden" => 2,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 3,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Catamarca (UNCa)",
				"direccion" => "Maximio Victoria 55, Piso 2, San Fernando del Valle de Catamarca, Catamarca",
                "codigo_postal" => "4700",
                "telefono" => "(54) (383) 443-0657",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 2,
				"provincia_id" => 6,
                "orden" => 3,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 4,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Jujuy (UNJu)",
				"direccion" => "Av. Bolivia 1239, San Salvador de Jujuy, Jujuy",
                "codigo_postal" => "Y4600GNA",
                "telefono" => "(54) (388) 422-1515",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 2,
				"provincia_id" => 7,
                "orden" => 4,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 5,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Salta (UNSa)",
				"direccion" => "Avenida Bolivia 5150, Salta",
                "codigo_postal" => "4400",
                "telefono" => "(54) (387) 425-8671",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 2,
				"provincia_id" => 8,
                "orden" => 5,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 6,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de La Rioja (UNLAR)",
				"direccion" => "Av. Luis M. de la Fuente S/N, Ciudad Universitaria de la Ciencia y de la Técnica, La Rioja",
                "codigo_postal" => "5300",
                "telefono" => "(54) (380) 445-7004",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 2,
				"provincia_id" => 9,
                "orden" => 6,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 7,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Chilecito (UNDEC)",
				"direccion" => "9 de Julio 22, Chilecito, La Rioja",
                "codigo_postal" => "5360",
                "telefono" => "(54) (3825) 42-7200",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 2,
				"provincia_id" => 9,
                "orden" => 7,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 8,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Misiones (UNAM)",
				"direccion" => "Campus Universitario Ruta 12 Km. 7,5, Miguel Lanús, Posadas, Misiones",
                "codigo_postal" => NULL,
                "telefono" => "(54) (376) 448-0200",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 3,
				"provincia_id" => 10,
                "orden" => 8,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 9,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional del Nordeste (UNNE)",
				"direccion" => " 25 de Mayo 868, Corrientes, Corrientes ",
                "codigo_postal" => "3400",
                "telefono" => NULL,
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 3,
				"provincia_id" => 12,
                "orden" => 9,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 10,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Formosa (UNaF)",
				"direccion" => "Campus Universitario, Av. Gutnisky 3200, Formosa",
                "codigo_postal" => "3600",
                "telefono" => "(54) (370) 442-3926",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 3,
				"provincia_id" => 26,
                "orden" => 10,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 11,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Río Cuarto (UNRC)",
				"direccion" => "Ruta Nacional 36 KM 601, Río Cuarto, Córdoba",
                "codigo_postal" => "5800",
                "telefono" => "(54) (358) 467-6200",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 5,
				"provincia_id" => 13,
                "orden" => 11,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 12,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Villa María (UNVM)",
				"direccion" => " Lisandro de la Torre 252, Villa María, Córdoba",
                "codigo_postal" => NULL,
                "telefono" => "(54) (353) 453-9100",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 5,
				"provincia_id" => 13,
                "orden" => 12,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 13,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Cuyo (UNCUYO)",
				"direccion" => "San Martín, Mendoza",
                "codigo_postal" => NULL,
                "telefono" => "(54) (261) 449-4009",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 5,
				"provincia_id" => 14,
                "orden" => 13,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 14,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Córdoba (UNC)",
				"direccion" => "Av. Valparaiso S/N, Córdoba",
                "codigo_postal" => "X5000HRV",
                "telefono" => "(54) (351) 443-7300",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 5,
				"provincia_id" => 13,
                "orden" => 14,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 15,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de San Luis (UNSL)",
				"direccion" => "Av. 25 de Mayo 384, Villa Mercedes, San Luis",
                "codigo_postal" => "5730",
                "telefono" => "(54) (2657) 43-0980",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 5,
				"provincia_id" => 15,
                "orden" => 15,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 16,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de San Juan (UNSJ)",
				"direccion" => "Mitre Este 396, San Juan",
                "codigo_postal" => "J5402CWH",
                "telefono" => "(54) (264) 429-5000",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 5,
				"provincia_id" => 16,
                "orden" => 16,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 17,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de La Pampa (UNLPAM)",
				"direccion" => "Cnel. Gil 353, Santa Rosa, La Pampa",
                "codigo_postal" => NULL,
                "telefono" => "(54) (2295) 45-1600",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 5,
				"provincia_id" => 17,
                "orden" => 17,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 18,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Rosario (UNR)",
				"direccion" => "Maipú 1065, Rosario, Santa Fe",
                "codigo_postal" => "2000",
                "telefono" => "(54) (341) 420-1200",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 1,
				"provincia_id" => 3,
                "orden" => 18,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 19,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional del Litoral (UNL)",
				"direccion" => " Bv. Pellegrini 2750, Santa Fe",
                "codigo_postal" => "3000",
                "telefono" => "(54) (342) 457-1110",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 1,
				"provincia_id" => 3,
                "orden" => 19,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 20,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Entre Ríos (UNER)",
				"direccion" => "Eva Perón 24, Concepción del Uruguay, Entre Ríos",
                "codigo_postal" => "3260",
                "telefono" => "(54) (3442) 42-1500",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 1,
				"provincia_id" => 2,
                "orden" => 20,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 21,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de General San Martín (UNSam)",
				"direccion" => " Av. 25 de Mayo y Francia, San Martín, Buenos Aires",
                "codigo_postal" => "1650",
                "telefono" => "(54) (11) 4006-1500",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 21,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 22,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad de Buenos Aires (UBA)",
				"direccion" => "Viamonte 430, Buenos Aires",
                "codigo_postal" => "1053",
                "telefono" => "(54) (11) 4510-1100",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 22,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 23,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de General Sarmiento (UNGS)",
				"direccion" => "Juan María Gutiérrez 1150, Los Polvorines, Buenos Aires",
                "codigo_postal" => "1613",
                "telefono" => "(54) (11) 4469-7795",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 23,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 24,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Lomas de Zamora (UNLZ)",
				"direccion" => " Camino de Cintura y Juan XXIII, Lomas de Zamora, Buenos Aires",
                "codigo_postal" => "1836",
                "telefono" => "(54) (11) 4282-8045",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 24,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 25,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Quilmes (UNQ)",
				"direccion" => "Roque Sáenz Peña 352, Bernal, Buenos Aires",
                "codigo_postal" => "B1876BXD",
                "telefono" => "(54) (11) 4365-7100",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 25,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 26,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Lanús (UNLa)",
				"direccion" => "29 de Septiembre 3901, Lanús, Buenos Aires",
                "codigo_postal" => "B1826GLC",
                "telefono" => "(54) (11) 5533-5600",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 26,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 27,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Avellaneda (UNDAV)",
				"direccion" => "España 350 esq. Colón, Avellaneda",
                "codigo_postal" => "B1826GLC",
                "telefono" => "(54) (11) 4229-2400",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 27,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 28,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional Arturo Jauretche (UNAJ)",
				"direccion" => "Avenida Calchaquí 6200, Florencio Varela, Buenos Aires",
                "codigo_postal" => "1888",
                "telefono" => "(54) (11) 4275-6100",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 28,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 29,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de La Matanza (UNLaM)",
				"direccion" => "Florencio Varela 1903, La Matanza, Buenos Aires",
                "codigo_postal" => "1755",
                "telefono" => "(54) (11) 4480-8900",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 29,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 30,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Tres de Febrero (UNTREF)",
				"direccion" => "Sede Centro Cultural Borges - Viamonte 525 - CABA",
                "codigo_postal" => NULL,
                "telefono" => NULL,
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 30,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 31,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de las Artes (UNA)",
				"direccion" => "Bolivar 1674, Capital Federal, Buenos Aires",
                "codigo_postal" => "C1140ABI",
                "telefono" => "(54) (11) 4362-1699",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 31,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 32,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Tecnológica Nacional (UTN)",
				"direccion" => "Avenida Medrano 951, Buenos Aires",
                "codigo_postal" => "C1179AAQ",
                "telefono" => "(54) (11) 4867-7511",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 32,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 33,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional del Sur (UNS)",
				"direccion" => "Av. Alem 1253, Bahía Blanca, Buenos Aires",
                "codigo_postal" => "8000",
                "telefono" => "(54) (291) 459-5100",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 6,
				"provincia_id" => 20,
                "orden" => 33,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 34,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Mar del Plata (UNMdP)",
				"direccion" => "Diagonal J. B. Alberdi 2695, Mar del Plata, Buenos Aires",
                "codigo_postal" => "7600",
                "telefono" => "(54) (223) 492-1705",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 6,
				"provincia_id" => 20,
                "orden" => 34,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 35,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional del Centro de la Provincia de Buenos Aires (UNICEN)",
				"direccion" => "Gral. Pinto 399, Tandil, Buenos Aires",
                "codigo_postal" => "B7000GHG",
                "telefono" => "(54) (249) 442 2000",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 6,
				"provincia_id" => 20,
                "orden" => 35,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 36,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional del Noroeste de la Provincia de Buenos Aires (UNNOBA)",
				"direccion" => "Sede Junín: Roque Saenz Peña 456, Junín, Buenos Aires | Sede Pergamino: Monteagudo 2772, Pergamino, Buenos Aires",
                "codigo_postal" => "B7000GHG",
                "telefono" => "(54) (236) 440-7750 / (54) (2477) 40-9500",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 6,
				"provincia_id" => 20,
                "orden" => 36,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 37,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de La Plata (UNLP)",
				"direccion" => "Avenida 7 776, La Plata, Buenos Aires",
                "codigo_postal" => "1900",
                "telefono" => "(54) (221) 425-6967",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 6,
				"provincia_id" => 20,
                "orden" => 37,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 38,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Luján (UNLu)",
				"direccion" => "Ruta 5 y Avenida Constitución, Luján, Buenos Aires",
                "codigo_postal" => "6700",
                "telefono" => "(54) (2323) 42-3979",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 6,
				"provincia_id" => 20,
                "orden" => 38,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 39,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de la Patagonia San Juan Bosco (UNPSJB)",
				"direccion" => " Ciudad Universitaria, Ruta Provincial No. 1 Km. 4, Comodoro Rivadavia, Chubut",
                "codigo_postal" => "9000",
                "telefono" => "(54) (297) 455-7856",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 4,
				"provincia_id" => 21,
                "orden" => 39,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 40,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de la Patagonia Austral (UNPA)",
				"direccion" => "Acceso Norte, Ruta N° 3, Caleta Olivia, Santa Cruz",
                "codigo_postal" => "9011",
                "telefono" => "(54) (297) 485-4888",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 4,
				"provincia_id" => 22,
                "orden" => 40,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 41,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Río Negro (UNRN)",
				"direccion" => "Av. San Martín 2650, El Bolsón, Río Negro",
                "codigo_postal" => "8430",
                "telefono" => "(54) (294) 449-8939",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 4,
				"provincia_id" => 23,
                "orden" => 41,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 42,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional del Comahue (UNCOMA)",
				"direccion" => "Buenos Aires 1400, Neuquén",
                "codigo_postal" => "8300",
                "telefono" => "(54) (299) 449-0363",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 4,
				"provincia_id" => 25,
                "orden" => 42,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 43,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de Tierra del Fuego, Antártida e Islas del Atlántico Sur (UNTDF)",
				"direccion" => "Darwin S/N, Ushuaia, Tierra del Fuego",
                "codigo_postal" => "9410",
                "telefono" => "(54) (2901) 43-4163",
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 4,
				"provincia_id" => 24,
                "orden" => 43,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 44,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Nacional de José C. Paz (UNPaz)",
				"direccion" => "Leandro N. Alem 4731, José C. Paz",
                "codigo_postal" => "1665",
                "telefono" => NULL,
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 44,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 45,
				"tipo_de_institucion_id" => 2,
				"nombre" => "Universidad Autónoma de Entre Rios (UADER)",
				"direccion" => "Av. Francisco Ramírez 1143, Paraná",
                "codigo_postal" => "3100",
                "telefono" => NULL,
                "email" => NULL,
				"pais_id" => 1,
				"region_id" => 7,
				"provincia_id" => 19,
                "orden" => 44,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			]
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('instituciones')
            ->whereIn('id', [
                     1,  2,  3,  4,  5,  6,  7,  8,  9,
                10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
                20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
                40, 41, 42, 43, 44, 45
            ])
		    ->delete();
	}

}
