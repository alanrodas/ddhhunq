<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoPublicacionesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());
        $createdTimestamp = $timestamp;
        $updatedTimestamp = $timestamp;
        $verifiedTimestamp = $timestamp;

        $nextCreatedTimestamp = function() {
            global $createdTimestamp;
            $datetime = new DateTime($createdTimestamp);
            $datetime->modify('+1 second');
            $createdTimestamp = $datetime->format('Y-m-d H:i:s');
            return $createdTimestamp;
        };

        $nextUpdatedTimestamp = function() {
            global $updatedTimestamp;
            $datetime = new DateTime($updatedTimestamp);
            $datetime->modify('+1 second');
            $updatedTimestamp = $datetime->format('Y-m-d H:i:s');
            return $updatedTimestamp;
        };

        $nextVerifiedTimestamp = function() {
            global $verifiedTimestamp;
            $datetime = new DateTime($verifiedTimestamp);
            $datetime->modify('+1 second');
            $verifiedTimestamp = $datetime->format('Y-m-d H:i:s');
            return $verifiedTimestamp;
        };

        DB::table('publicaciones')->insert([
			[
				"id" => 1,
                "nombre" => "Problemáticas Docentes Universitarios, Una mirada desde la Facultad de Agronomia y Zootecnica",
                "tipo_de_publicacion_id" => 3,
				"institucion_id" => 1,
				"dependencia_institucional_id" => 2,
                "descripcion" => "Capitulo I - EL PARADIGMA DE LA SOSTENIBILIDAD. SU INCORPORACION COMO OBJETO DE ENSEÑANZA EN LA CARRERA DE AGRONOMIA DE LA UNIVERSIDAD NACIONAL DE TUCUMAN",
                "contacto_nombre" => NULL,
                "contacto_telefono" => NULL,
                "contacto_email" => NULL,
                "responsables_nombres" => "Manuela R. Toranzos de Pérez",
                "responsables_detalles" => NULL,
                "archivos_adjuntos" => NULL,
                "editorial" => NULL,
                "anho" => NULL,
                "ciudad" => NULL,
                "serie" => NULL,
                "isbn" => NULL,
                "created_at" => $nextCreatedTimestamp(),
                "updated_at" => $nextUpdatedTimestamp(),
                "deleted_at" => NULL,
                "verified_at" => $nextVerifiedTimestamp()
			],
			[
				"id" => 2,
                "nombre" => "David y el general",
                "tipo_de_publicacion_id" => 1,
                "institucion_id" => 32,
                "dependencia_institucional_id" => 57,
                "descripcion" => "Obra teatral. de la artista bahiense Coral Aguirre, de destacada trayectoria en la Argentina y en México, país donde se halla radicada. La obra trata el tema de la identidad, vulnerada por la dictadura de los años setenta en la Argentina.",
                "contacto_nombre" => NULL,
                "contacto_telefono" => NULL,
                "contacto_email" => NULL,
                "responsables_nombres" => "Coral Aguirre",
                "responsables_detalles" => NULL,
                "archivos_adjuntos" => NULL,
                "editorial" => NULL,
                "anho" => NULL,
                "ciudad" => NULL,
                "serie" => NULL,
                "isbn" => NULL,
                "created_at" => $nextCreatedTimestamp(),
                "updated_at" => $nextUpdatedTimestamp(),
                "deleted_at" => NULL,
                "verified_at" => $nextVerifiedTimestamp()
			],
			[
				"id" => 3,
                "nombre" => "Revista Digital de Derechos Humanos de Infojus (Sistema Argentino de Información Jurídica)",
                "tipo_de_publicacion_id" => 4,
                "institucion_id" => 35,
                "dependencia_institucional_id" => 63,
                "descripcion" => "La Revista se descarga gratuitamente en http://www.infojus.gov.ar/ index.php?kk_seccion=revistas o en http://www1.infojus.gov.ar/ revistas?0. ",
                "contacto_nombre" => NULL,
                "contacto_telefono" => NULL,
                "contacto_email" => NULL,
                "responsables_nombres" => NULL,
                "responsables_detalles" => NULL,
                "archivos_adjuntos" => NULL,
                "editorial" => NULL,
                "anho" => NULL,
                "ciudad" => NULL,
                "serie" => NULL,
                "isbn" => NULL,
                "created_at" => $nextCreatedTimestamp(),
                "updated_at" => $nextUpdatedTimestamp(),
                "deleted_at" => NULL,
                "verified_at" => $nextVerifiedTimestamp()
			]
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('publicaciones')
            ->whereIn('id', [1, 2, 3])
		    ->delete();
	}

}
