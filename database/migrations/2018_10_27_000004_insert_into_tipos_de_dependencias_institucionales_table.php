<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoTiposDeDependenciasInstitucionalesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('tipos_de_dependencias_institucionales')->insert([
			[
				"id" => 1,
				"nombre" => "Otros",
                "descripcion" => NULL,
                "orden" => 100,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL

			],
			[
				"id" => 2,
				"nombre" => "Facultad",
                "descripcion" => NULL,
                "orden" => 2,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 3,
				"nombre" => "Carrera",
                "descripcion" => NULL,
                "orden" => 3,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 4,
				"nombre" => "Espacio",
                "descripcion" => NULL,
                "orden" => 4,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 5,
				"nombre" => "Proyecto de Investigación",
                "descripcion" => NULL,
                "orden" => 5,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 6,
				"nombre" => "Proyecto de Extensión",
                "descripcion" => NULL,
                "orden" => 6,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			]
		]);
    }
    
    

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('tipos_de_dependencias_institucionales')
            ->whereIn("id", [1, 2, 3, 4, 5, 6])
		    ->delete();
	}

}
