<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoMenusTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('menus')->insert([
			[
				"id" => 1,
				"name" => "admin",
				"created_at" => $timestamp,
				"updated_at" => $timestamp
            ],
            [
				"id" => 2,
				"name" => "user",
				"created_at" => $timestamp,
				"updated_at" => $timestamp
			],
            [
				"id" => 3,
				"name" => "frontend",
				"created_at" => $timestamp,
				"updated_at" => $timestamp
			]
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('menus')
            ->whereIn('id', [1])
		    ->delete();
	}

}
