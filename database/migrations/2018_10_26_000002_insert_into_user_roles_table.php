<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoUserRolesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('user_roles')->insert([
			[
				"user_id" => 1,
				"role_id" => 1
			],
			[
				"user_id" => 2,
				"role_id" => 2
			]
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('user_roles')
            ->whereIn('user_id', [1, 2])
		    ->delete();
	}

}
