<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoTiposDeNivelesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('tipos_de_niveles')->insert([
			[
				"id" => 1,
				"nombre" => "Otros",
                "descripcion" => NULL,
                "orden" => 100,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 2,
				"nombre" => "Pregrado",
                "descripcion" => NULL,
                "orden" => 100,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 3,
				"nombre" => "Grado",
                "descripcion" => NULL,
                "orden" => 100,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			],
			[
				"id" => 4,
				"nombre" => "Posgrado",
                "descripcion" => NULL,
                "orden" => 100,
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
			]
		]);
    }
    
    

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('tipos_de_niveles')
            ->whereIn("id", [1, 2, 3, 4])
		    ->delete();
	}

}
