<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePublicacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('publicaciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
			$table->integer('tipo_de_publicacion_id')->unsigned();
			$table->integer('institucion_id')->unsigned();
            $table->integer('dependencia_institucional_id')->unsigned();
            $table->text('descripcion')->nullable();
            // contacto
			$table->string('contacto_nombre')->nullable();
			$table->string('contacto_telefono')->nullable();
			$table->string('contacto_email')->nullable();
            // responsables
            $table->string('responsables_nombres', 255)->nullable();
            $table->text('responsables_detalles')->nullable();
			// articulacion
            $table->integer('tipo_de_articulacion_id')->unsigned()->nullable();
            $table->text('articulacion_detalles')->nullable();
            // archivos adjunto
            $table->longText('archivos_adjuntos')->nullable();
            // otros datos
            $table->string('editorial')->nullable();
            $table->string('anho')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('serie')->nullable();
            $table->string('edicion')->nullable();
            $table->string('isbn')->nullable();
            // timestamps
			$table->timestamps();
			$table->softDeletes();
            $table->timestamp('verified_at')->nullable();

            $table->foreign('tipo_de_publicacion_id', 'publicaciones_tipos_de_publicaciones_FK')
                ->references('id')->on('tipos_de_publicaciones')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('institucion_id', 'publicaciones_instituciones_FK')
                ->references('id')->on('instituciones')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('dependencia_institucional_id', 'publicaciones_dependencias_institucionales_FK')
                ->references('id')->on('dependencias_institucionales')
                ->onDelete('cascade')
                ->onUpdate('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('publicaciones');
	}

}
