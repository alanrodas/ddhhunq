<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProvinciasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('provincias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombre');
            $table->smallInteger('orden')->unsigned()->index('ORDEN');
            $table->integer('pais_id')->unsigned();
			$table->integer('region_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();

            $table->foreign('pais_id', 'provincias_paises_FK')
                ->references('id')->on('paises')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('region_id', 'provincias_regiones_FK')
                ->references('id')->on('regiones')
                ->onDelete('cascade')
                ->onUpdate('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('provincias');
	}

}
