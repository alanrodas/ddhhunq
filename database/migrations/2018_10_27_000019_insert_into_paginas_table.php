<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoPaginasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('paginas')->insert([
            [
                "id" => 1,
                "nombre" => "home",
                "contenido" =>
"
<h2 class=\"home_title\">Plataforma Informática de Acceso, Intercambio y Divulgación</h2>
<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-8\">
            <h4 class=\"home_subtitle\">Contenidos, Prácticas y Gestión</h4>
            <p>
                Los contenidos necesarios para que toda persona y en cualquier parte del
                mundo pueda acceder de forma rápida y confiable a informaciones académicas
                específicas disponibles pero no siempre visibles, es el desafío cotidiano,
                por ejemplo, de las instituciones educativas de nivel superior.
                En el universo de todos los contenidos disponibles, en esta ocasión,
                prestamos especial atención a la educación en derechos humanos.
            </p>
            <p>
                Para lograr este objetivo, afrontamos el desafío de condensar todo lo
                vinculado con derechos humanos en este sitio que se suma al contexto de
                época, esto es, el avance de las nuevas tecnologías informáticas
                (computadoras, tablets, celulares...) que forman parte de nuestras vidas,
                se constituyen en herramientas cada vez más utilizadas para la búsqueda
                de información y la comunicación.
            </p>
        </div>
        <div class=\"col-md-4\">
            <img
                src=\"/defaults/paginas/cover_1.jpg\"
                alt=\"Portada 1\"
            />
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-4\">
            <img
                src=\"/defaults/paginas/cover_2.jpg\"
                alt=\"Portada 2\"
            />
        </div>
        <div class=\"col-md-8\">
            <p>
                Por esto, la Universidad Nacional de Quilmes (UNQ) ofrece esta plataforma
                (base de datos) que condensa información esencial sobre lo que acontece
                en relación a los derechos humanos en las universidades de Argentina y
                otros países. Esta es el resultado de investigaciones y acciones llevadas
                a cabo por el Centro de DDHH Emilio Mignone, el Programa de Extensión
                “Derechos de Todas y Todos” (y los proyectos que lo componen), el
                “Programa Universidad y Derechos Humanos (PUDeH)” como así también los
                proyectos de investigación “La Educación en Derechos Humanos en la
                Universidad Argentina. Políticas públicas” y “Derechos humanos aquí y
                ahora: un compromiso con el derecho a la educación superior y su carácter
                interdependiente”.
            </p>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-8\">
            <p>
                Esta base de datos, iniciada como un proyecto ambicioso hace varios años,
                tiene como propósito compilar y sistematizar toda información relevante
                que, muchas veces, no está visible o hallable. En este sentido, la
                plataforma informática permite la disponibilidad y actualización permanentes
                sobre contenidos de DDHH que cada institución ofrece.
            </p>
            <p>
                Invitamos a participar de esta propuesta colaborativa, tanto en la búsqueda
                como en el aporte de información sobre Educación en Derechos Humanos que
                desarrollan y poseen en vuestras instituciones.
            </p>
        </div>
        <div class=\"col-md-4\">
            <img
                src=\"/defaults/paginas/cover_3.jpg\"
                alt=\"Portada 3\"
            />
        </div>
    </div>
</div>
",
                "created_at" => $timestamp,
                "updated_at" => $timestamp,
                "deleted_at" => NULL
            ]
		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('paginas')
            ->whereIn('id', [1])
		    ->delete();
	}

}
