<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoSettingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('settings')->insert([
            // Site
			[
				"id" => 101,
				"key" => "site.title",
				"display_name" => "Título del Sitio",
				"value" => "La Educación en Derechos Humanos en el Nivel Superior",
				"details" => NULL,
				"type" => "text",
				"order" => 1,
				"group" => "Site"
			],
			[
				"id" => 102,
				"key" => "site.description",
				"display_name" => "Descripción del Sitio",
				"value" => "Plataforma informática de acceso, intercambio y divulgación sobre la educación en derechos humanos en el nivel superior.",
				"details" => NULL,
				"type" => "text",
				"order" => 2,
				"group" => "Site"
            ],
            [
				"id" => 103,
				"key" => "site.keywords",
				"display_name" => "Palabras Claves del Sitio",
				"value" => "nivel, superior, derechos, humanos, DDHH, intercambio, divulgación.",
				"details" => NULL,
				"type" => "text",
				"order" => 3,
				"group" => "Site"
			],
			[
				"id" => 104,
				"key" => "site.logo",
				"display_name" => "Logo del Sitio",
				"value" => "defaults/settings/logo.png",
				"details" => NULL,
				"type" => "image",
				"order" => 4,
				"group" => "Site"
			],
            [
				"id" => 105,
				"key" => "site.copyright",
				"display_name" => "Nota de Copyright",
				"value" => "Copyright© 2019 CeDHEM UNQ - Desarrollado por <a href=\"https://alanrodas.com\" target=\"__blank\">Alan Rodas Bonjour</a>",
				"details" => NULL,
				"type" => "text",
				"order" => 5,
				"group" => "Site"
            ],
            [
				"id" => 106,
				"key" => "site.author",
				"display_name" => "Autor",
				"value" => "Alan Rodas Bonjour",
				"details" => NULL,
				"type" => "text",
				"order" =>65,
				"group" => "Site"
			],
            [
				"id" => 107,
				"key" => "site.author_website",
				"display_name" => "Sitio web del Autor",
				"value" => "https://alanrodas.com",
				"details" => NULL,
				"type" => "text",
				"order" => 7,
				"group" => "Site"
            ],
            [
				"id" => 108,
				"key" => "site.author_email",
				"display_name" => "Email del Autor",
				"value" => "alanrodas@gmail.com",
				"details" => NULL,
				"type" => "text",
				"order" => 8,
				"group" => "Site"
            ],
            [
				"id" => 109,
				"key" => "site.google_analytics_tracking_id",
				"display_name" => "Google Analytics Tracking ID",
				"value" => NULL,
				"details" => NULL,
				"type" => "text",
				"order" => 9,
				"group" => "Site"
            ],
            [
				"id" => 110,
				"key" => "site.max_chars_textarea",
				"display_name" => "Cantidad Máxima de Caracteres en Textarea",
				"value" => 1500,
				"details" => NULL,
				"type" => "number",
				"order" => 10,
				"group" => "Site"
            ],
            [
				"id" => 111,
				"key" => "site.results_per_page",
				"display_name" => "Cantidad de Resultados de Busqueda por página",
				"value" => 30,
				"details" => NULL,
				"type" => "number",
				"order" => 11,
				"group" => "Site"
            ],
            [
				"id" => 112,
				"key" => "site.max_upload_files",
				"display_name" => "Cantidad máxima de archivos a subir (1 a 10)",
				"value" => 3,
				"details" => NULL,
				"type" => "number",
				"order" => 12,
				"group" => "Site"
            ],
            [
				"id" => 113,
				"key" => "site.max_upload_file_size",
				"display_name" => "Tamaño máximo de archivo a subir (En MB)",
				"value" => 25,
				"details" => NULL,
				"type" => "number",
				"order" => 13,
				"group" => "Site"
            ],

            // Admin
			[
				"id" => 201,
				"key" => "admin.bg_image",
				"display_name" => "Imagen de Fondo de Administración",
				"value" => "defaults/settings/unq.jpg",
				"details" => NULL,
				"type" => "image",
				"order" => 1,
				"group" => "Admin"
			],
			[
				"id" => 202,
				"key" => "admin.title",
				"display_name" => "Título de Administración",
				"value" => "Panel de administración",
				"details" => NULL,
				"type" => "text",
				"order" => 2,
				"group" => "Admin"
			],
			[
				"id" => 203,
				"key" => "admin.description",
				"display_name" => "Descripción de Administración",
				"value" => "Plataforma Informática de Acceso, Intercambio y Divulgación: Panel de Administración.",
				"details" => NULL,
				"type" => "text",
				"order" => 3,
				"group" => "Admin"
			],
			[
				"id" => 204,
				"key" => "admin.loader",
				"display_name" => "Cargador de Administración",
				"value" => "defaults/settings/logo.png",
				"details" => NULL,
				"type" => "image",
				"order" => 4,
				"group" => "Admin"
			],
			[
				"id" => 205,
				"key" => "admin.icon_image",
				"display_name" => "Imágen del Icono de Administración",
				"value" => "defaults/settings/logo.png",
				"details" => NULL,
				"type" => "image",
				"order" => 5,
				"group" => "Admin"
			],
			[
				"id" => 206,
				"key" => "admin.google_analytics_client_id",
				"display_name" => "Google Analytics Client ID (usado para panel de administración)",
				"value" => NULL,
				"details" => NULL,
				"type" => "text",
				"order" => 6,
				"group" => "Admin"
			],
			[
				"id" => 207,
				"key" => "admin.inner_title",
				"display_name" => "Título de Administración en Barra Lateral",
				"value" => "CeDHEM",
				"details" => NULL,
				"type" => "text",
				"order" => 7,
				"group" => "Admin"
            ],
            // Soft Delete
            [
                "id" => 301,
                "key" => "soft_deletes.browse_soft_deletes",
                "display_name" => "Permitir navegar los elementos en la papelera",
                "value" => true,
                "details" => "{\"on\":\"Si\",\"off\":\"No\"}",
                "type" => "checkbox",
                "order" => 1,
                "group" => "Soft Deletes"
            ],
            [
                "id" => 302,
                "key" => "soft_deletes.delete_soft_deletes",
                "display_name" => "Permitir borrar los elementos en la papelera",
                "value" => true,
                "details" => "{\"on\":\"Si\",\"off\":\"No\"}",
                "type" => "checkbox",
                "order" => 2,
                "group" => "Soft Deletes"
            ],
            [
                "id" => 303,
                "key" => "soft_deletes.restore_soft_deletes",
                "display_name" => "Permitir restaurar los elementos en la papelera",
                "value" => true,
                "details" => "{\"on\":\"Si\",\"off\":\"No\"}",
                "type" => "checkbox",
                "order" => 3,
                "group" => "Soft Deletes"
            ],

		]);
    }



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('settings')
            ->whereIn('id', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])
		    ->delete();
	}

}
