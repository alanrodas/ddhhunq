<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InsertIntoRolesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $timestamp = date('Y-m-d H:i:s', time());

        DB::table('roles')->insert([
			[
				"id" => 1,
				"name" => "admin",
				"display_name" => "Administrador",
				"created_at" => $timestamp,
				"updated_at" => $timestamp
			],
			[
				"id" => 2,
				"name" => "user",
				"display_name" => "Usuario",
				"created_at" => $timestamp,
				"updated_at" => $timestamp
			]
		]);
    }
    
    

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::table('roles')
            ->whereIn('id', [1, 2])
		    ->delete();
	}

}
